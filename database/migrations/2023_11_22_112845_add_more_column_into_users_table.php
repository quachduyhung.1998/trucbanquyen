<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->after('content_type', function (Blueprint $table) {
                $table->string('nickname')->nullable();
                $table->tinyInteger('sex')->nullable()->comment('0: nữ, 1: nam');
                $table->date('birthday')->nullable();
                $table->string('job')->nullable();
                $table->string('permanent_address')->nullable()->comment('Địa chỉ thường trú');
                $table->string('contact_address')->nullable()->comment('Địa chỉ liên hệ');
                $table->string('cccd_number')->nullable()->comment('Số CCCD/CMND');
                $table->string('cccd_place')->nullable()->comment('Nơi cấp CCCD/CMND');
                $table->date('cccd_date')->nullable()->comment('Ngày cấp CCCD/CMND');
                $table->string('cccd_front_photo')->nullable()->comment('Ảnh mặt trước CCCD/CMND');
                $table->string('cccd_back_photo')->nullable()->comment('Ảnh mặt sau CCCD/CMND');
                $table->string('face_photo')->nullable()->comment('Ảnh gương mặt');
            });
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropColumn(['nickname', 'sex', 'birthday', 'job', 'permanent_address', 'contact_address', 'cccd_number', 'cccd_place', 'cccd_date', 'cccd_front_photo', 'cccd_back_photo', 'face_photo']);
        });
    }
};
