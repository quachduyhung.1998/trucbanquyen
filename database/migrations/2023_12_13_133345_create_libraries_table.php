<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('libraries', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->tinyInteger('type');
            $table->string('file_id');
            $table->text('url')->nullable();
            $table->string('thumb')->nullable();
            $table->bigInteger('size')->nullable();
            $table->string('file_extension')->nullable();
            $table->integer('video_duration')->nullable();
            $table->string('author')->nullable();
            $table->text('data_similar')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('libraries');
    }
};
