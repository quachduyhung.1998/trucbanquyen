<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notification_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('notification_id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('to_owner')->default(1)->comment('gửi tới chủ bản quyền. ENUM 1: đúng, 2: sai');
            $table->tinyInteger('is_new')->default(1)->comment('1: mới, 2: cũ');
            $table->tinyInteger('status')->default(1)->comment('1: chưa đọc, 2: đã đọc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notification_users');
    }
};
