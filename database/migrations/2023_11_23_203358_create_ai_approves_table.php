<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ai_approves', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url');
            $table->enum('type', ['IMAGE', 'VIDEO']);
            $table->tinyInteger('status_ai_detect')->default(1)->comment('1: chờ xử lý, 2: có khả năng vi phạm, 3: không tìm thấy vi phạm');
            $table->tinyInteger('status')->default(1)->comment('1: chờ xử lý, 2: duyệt, 3: không duyệt');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ai_approves');
    }
};
