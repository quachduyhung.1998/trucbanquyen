<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('object_copyrights', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('type');
            $table->unsignedBigInteger('library_id')->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('object_copyrights', function (Blueprint $table) {
            $table->string('url');
            $table->enum('type', ['IMAGE', 'VIDEO']);
            $table->dropColumn('library_id');
        });
    }
};
