<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('copyright_registrations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('name');
            $table->unsignedBigInteger('type_id')->nullable()->comment('Loại hình');
            $table->unsignedBigInteger('field_id')->nullable()->comment('Lĩnh vực');
            $table->text('description')->nullable();
            $table->string('url_content_protection')->comment('Nội dung bản quyền cần bảo vệ');
            $table->text('url_copyright_registrations')->nullable()->comment('Hồ sơ đăng ký bản quyền');
            $table->tinyInteger('status_ai_detect')->default(1)->comment('1: chờ xử lý, 2: có khả năng vi phạm, 3: không tìm thấy vi phạm');
            $table->tinyInteger('status')->default(1)->comment('1: chờ xử lý, 2: đang triển khai, 3: cần hoàn thiện hồ sơ, 4: từ chối, 5: hoàn thành');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('copyright_registrations');
    }
};
