<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('model_type');
            $table->unsignedBigInteger('model_id');
            $table->text('content')->nullable();
            $table->tinyInteger('status_content')->nullable()->comment('1: chờ xử lý, 2: đang triển khai, 3: cần hoàn thiện hồ sơ, 4: từ chối, 5: hoàn thành');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
