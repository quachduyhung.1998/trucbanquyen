<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->comment('Người giới thiệu');
            $table->string('username')->unique();
            $table->string('full_name');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->nullable();
            $table->string('avatar')->nullable();
            $table->tinyInteger('role')->default(4)->comment('1: super admin, 2: admin, 3: sale, 4: cskh, 5: pháp lý, 6: thành viên, 7: cộng tác viên');
            $table->unsignedBigInteger('package_id')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: active, 2: inactive');
            $table->enum('type_user', ['TO_CHUC', 'CA_NHAN'])->nullable()->comment('TO_CHUC, CA_NHAN');
            $table->boolean('paid')->default(false);
            $table->string('fax')->nullable();
            $table->string('unit_name')->nullable()->comment('Tên đơn vị');
            $table->text('unit_address')->nullable()->comment('Địa chỉ đơn vị');
            $table->string('operating_license')->nullable()->comment('Giấy phép hoạt động');
            $table->string('business_license')->nullable()->comment('Giấy phép kinh doanh');
            $table->string('business_registration_number')->nullable()->comment('Số đăng ký kinh doanh');
            $table->text('business_registration_address')->nullable()->comment('Nơi đăng ký kinh doanh');
            $table->date('business_registration_date_issuance')->nullable()->comment('Ngày cấp giấy phép kinh doanh');
            $table->string('legal_entity_representative')->nullable()->comment('Đại diện pháp nhân');
            $table->string('logo_company')->nullable()->comment('Logo công ty');
            $table->text('content_type')->nullable()->comment('Các loại hình nội dung cần bảo vệ (mảng)');
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
