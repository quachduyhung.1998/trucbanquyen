<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('copyright_reports', function (Blueprint $table) {
            $table->longText('field_id')->nullable()->after('type_object');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('copyright_reports', function (Blueprint $table) {
            $table->dropColumn('field_id');
        });
    }
};
