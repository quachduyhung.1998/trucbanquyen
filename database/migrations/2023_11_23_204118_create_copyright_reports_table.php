<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('copyright_reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('object');
            $table->tinyInteger('type_object')->default(1)->comment('1: trong danh sách bảo vệ bản quyền, 2: ngoài danh sách bảo vệ bản quyền');
            $table->text('description')->nullable();
            $table->text('url_contents')->comment('Nội dung, hình ảnh bằng chứng');
            $table->string('subject_name')->nullable();
            $table->string('subject_phone')->nullable();
            $table->string('subject_email')->nullable();
            $table->string('subject_website')->nullable();
            $table->text('subject_address')->nullable();
            $table->date('violation_from_date')->nullable();
            $table->date('violation_to_date')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: chờ xử lý, 2: đang triển khai, 3: cần hoàn thiện hồ sơ, 4: từ chối, 5: hoàn thành');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('copyright_reports');
    }
};
