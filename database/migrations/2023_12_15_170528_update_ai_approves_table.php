<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('ai_approves', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('url');
            $table->dropColumn('type');
            $table->unsignedBigInteger('model_id')->after('id');
            $table->string('model_type')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('ai_approves', function (Blueprint $table) {
            $table->string('name');
            $table->string('url');
            $table->enum('type', ['IMAGE', 'VIDEO']);
            $table->dropColumn('model_id');
            $table->dropColumn('model_type');
        });
    }
};
