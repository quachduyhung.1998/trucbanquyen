<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::create([
            'username' => 'superadmin',
            'full_name' => 'Super Admin',
            'email' => 'superadmin@trucbanquyen.com',
            'role' => 1,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'username' => 'admin',
            'full_name' => 'Admin',
            'email' => 'admin@trucbanquyen.com',
            'role' => 2,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'username' => 'sale',
            'full_name' => 'Sale',
            'email' => 'sale@trucbanquyen.com',
            'role' => 3,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'username' => 'cskh',
            'full_name' => 'CSKH',
            'email' => 'cskh@trucbanquyen.com',
            'role' => 4,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'username' => 'phaply',
            'full_name' => 'Pháp Lý',
            'email' => 'phaply@trucbanquyen.com',
            'role' => 5,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
        \App\Models\User::create([
            'username' => 'member',
            'full_name' => 'Thành Viên',
            'email' => 'member@trucbanquyen.com',
            'role' => 6,
            'status' => 1,
            'password' => Hash::make('123456')
        ]);
    }
}
