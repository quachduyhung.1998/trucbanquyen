<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $arrayOfPermissionNames = [
            'view_dashboard',

            'permission.*',
            'permission.create',
            'permission.edit',
            'permission.delete',

            'object_copyright.*',
            'object_copyright.view',
            'object_copyright.create',
            'object_copyright.edit',
            'object_copyright.delete',

            'object_face.*',
            'object_face.view',
            'object_face.create',
            'object_face.edit',
            'object_face.delete',

            'ai_approve.*',
            'ai_approve.view',
            'ai_approve.create',
            'ai_approve.edit',
            'ai_approve.delete',

            'category.*',
            'category.view',
            'category.create',
            'category.edit',
            'category.delete',

            'package.*',
            'package.view',
            'package.create',
            'package.edit',
            'package.delete',

            'user.*',
            'user.view',
            'user.create',
            'user.edit',
            'user.delete',

            'member.*',
            'member.view',
            'member.create',
            'member.edit',
            'member.delete',

            'copyright_registration.*',
            'copyright_registration.view',
            'copyright_registration.create',
            'copyright_registration.edit',
            'copyright_registration.delete',

            'copyright_protection.*',
            'copyright_protection.view',
            'copyright_protection.create',
            'copyright_protection.edit',
            'copyright_protection.delete',

            'copyright_report.*',
            'copyright_report.view',
            'copyright_report.create',
            'copyright_report.edit',
            'copyright_report.delete',

            'history.*',
            'history.view',
            'history.create',
            'history.edit',
            'history.delete',

            'setting.*',
            'setting.view',
            'setting.create',
            'setting.edit',
            'setting.delete',
        ];
        $permissions = collect($arrayOfPermissionNames)->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => 'web',
                'created_at' => now(),
                'updated_at' => now()
            ];
        });
        Permission::insert($permissions->toArray());

        $roleSuperAdmin = Role::create([
            'name' => 'super_admin',
            'guard_name' => 'web',
        ]);

        $roleAdmin = Role::create([
            'name' => 'admin',
            'guard_name' => 'web',
        ])->givePermissionTo(Permission::all());

        $roleSale = Role::create([
            'name' => 'sale',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'package.*',
            'member.*',
        ]);

        $roleCskh = Role::create([
            'name' => 'cskh',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'member.view',
            'copyright_report.view',
        ]);

        $rolePhapLy = Role::create([
            'name' => 'phap_ly',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'copyright_registration.*',
            'copyright_protection.*',
            'copyright_report.*',
        ]);

        $roleMember = Role::create([
            'name' => 'member',
            'guard_name' => 'web',
        ])->givePermissionTo([
            'view_dashboard',
            'package.view',
            'copyright_registration.view',
            'copyright_registration.create',
            'copyright_registration.edit',
            'copyright_protection.view',
            'copyright_protection.create',
            'copyright_protection.edit',
            'copyright_report.view',
            'copyright_report.create',
            'copyright_report.edit',
        ]);

        $roleCtv = Role::create([
            'name' => 'cong_tac_vien',
            'guard_name' => 'web',
        ]);

        $superAdmin = User::query()->where('role', 1)->first();
        if ($superAdmin) $superAdmin->assignRole('super_admin');

        $admin = User::query()->where('role', 2)->first();
        if ($admin) $admin->assignRole('admin');

        $sale = User::query()->where('role', 3)->first();
        if ($sale) $sale->assignRole('sale');

        $cskh = User::query()->where('role', 4)->first();
        if ($cskh) $cskh->assignRole('cskh');

        $phapLy = User::query()->where('role', 5)->first();
        if ($phapLy) $phapLy->assignRole('phap_ly');

        $member = User::query()->where('role', 6)->first();
        if ($member) $member->assignRole('member');
    }
}
