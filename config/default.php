<?php
return [
    'password_default' => '123456',
    'color' => [
        1 => 'primary',
        2 => 'info',
        3 => 'success',
        4 => 'warning',
        5 => 'danger',
        6 => 'dark',
    ],
    'base_url_server_upload' => env('BASE_URL_SERVER_UPLOAD', 'http://139.99.93.129:5001'),
    'url_server_file' => env('URL_SERVER_FILE', 'http://139.99.93.129:9000'),
    'violation_threshold' => 0.85
];
