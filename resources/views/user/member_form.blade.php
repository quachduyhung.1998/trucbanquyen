<div class="row">
    <div class="col-12 mb-5"><h3>{{ __('Thông tin đăng nhập') }}</h3></div>

    @isset($member)
        <input type="hidden" name="id" value="{{ $member->id }}">
    @endisset

    <div class="col-md-6 col-12 mb-7">
        <label class="required fw-semibold fs-6 mb-2">{{ __('Tên đăng nhập') }}</label>
        <input type="text" name="username" class="form-control mb-3 mb-lg-0 @error('username') border-danger @enderror" value="{{ old('username', isset($member) ? $member->username : '') }}" />
        @error('username')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    @if(!isset($member))
        <div class="col-md-6 col-12 mb-7">
            <label class="required fw-semibold fs-6 mb-2">{{ __('Mật khẩu') }}</label>
            <input type="password" name="password" class="form-control mb-3 mb-lg-0 @error('password') border-danger @enderror" />
            @error('password')
                <span class="text-danger d-block mt-1">{{ $message }}</span>
            @enderror
        </div>
    @endif

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Gói thành viên') }}</label>
        <select name="package_id" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn thành viên') }}" data-allow-clear="true" data-hide-search="true">
            <option></option>
            @foreach($packages as $key => $value)
                <option value="{{ $key }}" {{ old('package_id') ? (old('package_id') == $key ? 'selected' : '') : ((isset($member) && $member->package_id == $key) ? 'selected' : '') }}>{{ $value }}</option>
            @endforeach
        </select>
    </div>

    @if(!isset($member))
        <div class="col-md-6 col-12 mb-7"></div>
    @endif

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Loại tài khoản') }}</label>
        <div class="d-flex align-items-center gap-6">
            @foreach(\App\Models\User::$typeUserLabel as $key => $value)
                <div class="form-check form-check-custom form-check-success">
                    <input class="form-check-input" type="radio" name="type_user" value="{{ $key }}" id="flexRadioDefault-type_user-{{ $key }}" {{ old('type_user') ? (old('type_user') == $key ? 'checked' : '') : ((isset($member) && $member->type_user == $key) ? 'checked' : '') }} />
                    <label class="form-check-label" for="flexRadioDefault-type_user-{{ $key }}">
                        {{ $value }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Trạng thái thanh toán') }}</label>
        <div class="d-flex align-items-center gap-6">
            @foreach(\App\Models\User::$paymentStatusLabel as $key => $value)
                <div class="form-check form-check-custom form-check-success">
                    <input class="form-check-input" type="radio" name="paid" value="{{ $key }}" id="flexRadioDefault-paid-{{ $key }}" {{ old('paid') !== null ? (old('paid') == $key ? 'checked' : '') : ((isset($member) && $member->paid == $key) ? 'checked' : '') }} />
                    <label class="form-check-label" for="flexRadioDefault-paid-{{ $key }}">
                        {{ $value }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Người giới thiệu') }}</label>
        <select name="user_id" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn người giới thiệu') }}" data-allow-clear="true" data-hide-search="true">
            <option></option>
            @foreach($refs as $ref)
                <option value="{{ $ref->id }}" {{ old('user_id') ? (old('user_id') == $ref->id ? 'selected' : '') : ((isset($member) && $member->user_id == $ref->id) ? 'selected' : '') }}>{{ $ref->full_name }}</option>
            @endforeach
        </select>
    </div>

    <div class="separator mb-7"></div>

    <div class="col-12 mb-5"><h3>{{ __('Thông tin khách hàng') }}</h3></div>

    <div class="col-md-6 col-12 mb-7">
        <label class="required fw-semibold fs-6 mb-2">{{ __('Họ tên') }}</label>
        <input type="text" name="full_name" class="form-control mb-3 mb-lg-0 @error('full_name') border-danger @enderror" value="{{ old('full_name', isset($member) ? $member->full_name : '') }}" />
        @error('full_name')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Biệt danh') }}</label>
        <input type="text" name="nickname" class="form-control mb-3 mb-lg-0 value="{{ old('nickname', isset($member) ? $member->nickname : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Điện thoại') }}</label>
        <input type="number" name="phone" class="form-control mb-3 mb-lg-0 @error('phone') border-danger @enderror" value="{{ old('phone', isset($member) ? $member->phone : '') }}" />
        @error('phone')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">Email</label>
        <input type="email" name="email" class="form-control mb-3 mb-lg-0 @error('email') border-danger @enderror" placeholder="example@domain.com" value="{{ old('email', isset($member) ? $member->email : '') }}" />
        @error('email')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Giới tính') }}</label>
        <div class="d-flex align-items-center gap-6">
            @foreach(\App\Models\User::$sexLabel as $key => $value)
                <div class="form-check form-check-custom form-check-success">
                    <input class="form-check-input" type="radio" name="sex" value="{{ $key }}" id="flexRadioDefault-sex-{{ $key }}" {{ old('sex') !== null ? (old('sex') == $key ? 'checked' : '') : ((isset($member) && $member->sex == $key) ? 'checked' : '') }} />
                    <label class="form-check-label" for="flexRadioDefault-sex-{{ $key }}">
                        {{ $value }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ngày sinh') }}</label>
        <input name="birthday" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('birthday', isset($member) ? $member->birthday : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Nghề nghiệp') }}</label>
        <input type="text" name="job" class="form-control mb-3 mb-lg-0" value="{{ old('job', isset($member) ? $member->job : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Địa chỉ thường chú (CCCD/CMND)') }}</label>
        <input type="text" name="permanent_address" class="form-control mb-3 mb-lg-0" value="{{ old('permanent_address', isset($member) ? $member->permanent_address : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Địa chỉ liên hệ') }}</label>
        <input type="text" name="contact_address" class="form-control mb-3 mb-lg-0" value="{{ old('contact_address', isset($member) ? $member->contact_address : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Số CCCD/CMND') }}</label>
        <input type="text" name="cccd_number" class="form-control mb-3 mb-lg-0" value="{{ old('cccd_number', isset($member) ? $member->cccd_number : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Nơi cấp CCCD/CMND') }}</label>
        <input type="text" name="cccd_place" class="form-control mb-3 mb-lg-0" value="{{ old('cccd_place', isset($member) ? $member->cccd_place : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ngày cấp CCCD/CMND') }}</label>
        <input name="cccd_date" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('cccd_date', isset($member) ? $member->cccd_date : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh mặt trước CCCD/CMND') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_cccd_front_photo">
            <input type="hidden" name="cccd_front_photo" class="form-control mb-3 mb-lg-0" value="{{ old('cccd_front_photo', isset($member) ? $member->cccd_front_photo : '') }}" />
            <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                    </div>
                </div>
                <a href="{{ old('cccd_front_photo', isset($member) ? asset($member->cccd_front_photo) : '') }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                    <img src="{{ old('cccd_front_photo', isset($member) ? asset($member->cccd_front_photo) : '') }}" alt="" class="cursor-pointer mw-250px mh-200px">
                </a>
            </div>
        </div>
        <!--end::Dropzone-->
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh mặt sau CCCD/CMND') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_cccd_back_photo">
            <input type="hidden" name="cccd_back_photo" class="form-control mb-3 mb-lg-0" value="{{ old('cccd_back_photo', isset($member) ? $member->cccd_back_photo : '') }}" />
            <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                    </div>
                </div>
                <a href="{{ old('cccd_back_photo', isset($member) ? asset($member->cccd_back_photo) : '') }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                    <img src="{{ old('cccd_back_photo', isset($member) ? asset($member->cccd_back_photo) : '') }}" alt="" class="cursor-pointer mw-250px mh-200px">
                </a>
            </div>
        </div>
        <!--end::Dropzone-->
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ảnh gương mặt') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_face_photo">
            <input type="hidden" name="face_photo" class="form-control mb-3 mb-lg-0" value="{{ old('face_photo', isset($member) ? $member->face_photo : '') }}" />
            <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                    </div>
                </div>
                <a href="{{ old('face_photo', isset($member) ? asset($member->face_photo) : '') }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                    <img src="{{ old('face_photo', isset($member) ? asset($member->face_photo) : '') }}" alt="" class="cursor-pointer mw-250px mh-200px">
                </a>
            </div>
        </div>
        <!--end::Dropzone-->
    </div>

    <div class="col-md-6 col-12 mb-7"></div>

    <div class="col-md-6 col-12 mb-7">
        <label class="required fw-semibold fs-6 mb-2">{{ __('Mã số thuế') }}</label>
        <input type="text" name="fax" class="form-control mb-3 mb-lg-0 @error('fax') border-danger @enderror" value="{{ old('fax', isset($member) ? $member->fax : '') }}" />
        @error('fax')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="required fw-semibold fs-6 mb-2">{{ __('Tên đơn vị') }}</label>
        <input type="text" name="unit_name" class="form-control mb-3 mb-lg-0 @error('unit_name') border-danger @enderror" value="{{ old('unit_name', isset($member) ? $member->unit_name : '') }}" />
        @error('unit_name')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="required fw-semibold fs-6 mb-2">{{ __('Địa chỉ đơn vị') }}</label>
        <input type="text" name="unit_address" class="form-control mb-3 mb-lg-0 @error('unit_address') border-danger @enderror" value="{{ old('unit_address', isset($member) ? $member->unit_address : '') }}" />
        @error('unit_address')
            <span class="text-danger d-block mt-1">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Giấy phép hoạt động') }}</label>
        <input type="text" name="operating_license" class="form-control mb-3 mb-lg-0" value="{{ old('operating_license', isset($member) ? $member->operating_license : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Số đăng ký kinh doanh') }}</label>
        <input type="text" name="business_registration_number" class="form-control mb-3 mb-lg-0" value="{{ old('business_registration_number', isset($member) ? $member->business_registration_number : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Nơi đăng ký kinh doanh') }}</label>
        <input type="text" name="business_registration_address" class="form-control mb-3 mb-lg-0" value="{{ old('business_registration_address', isset($member) ? $member->business_registration_address : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Ngày cấp giấy phép kinh doanh') }}</label>
        <input type="text" name="business_registration_date_issuance" class="form-control mb-3 mb-lg-0 datepicker" value="{{ old('business_registration_date_issuance', isset($member) ? $member->business_registration_date_issuance : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Đại diện pháp nhân') }}</label>
        <input type="text" name="legal_entity_representative" class="form-control mb-3 mb-lg-0" value="{{ old('legal_entity_representative', isset($member) ? $member->legal_entity_representative : '') }}" />
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Giấy phép kinh doanh') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_business_license">
            <input type="hidden" name="business_license" class="form-control mb-3 mb-lg-0" value="{{ old('business_license', isset($member) ? $member->business_license : '') }}" />
            <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                    </div>
                </div>
                <a href="{{ old('business_license', isset($member) ? asset($member->business_license) : '') }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                    <img src="{{ old('business_license', isset($member) ? asset($member->business_license) : '') }}" alt="" class="cursor-pointer mw-250px mh-200px">
                </a>
            </div>
        </div>
        <!--end::Dropzone-->
    </div>

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Logo công ty') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_logo_company">
            <input type="hidden" name="logo_company" class="form-control mb-3 mb-lg-0" value="{{ old('logo_company', isset($member) ? $member->logo_company : '') }}" />
            <div class="d-flex flex-xl-row flex-column-reverse align-center justify-content-between">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                    </div>
                </div>
                <a href="{{ old('logo_company', isset($member) ? asset($member->logo_company) : '') }}" class="text-center image_preview" target="_blank" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}">
                    <img src="{{ old('logo_company', isset($member) ? asset($member->logo_company) : '') }}" alt="" class="cursor-pointer mw-250px mh-200px">
                </a>
            </div>
        </div>
        <!--end::Dropzone-->
    </div>

    <div class="col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Các loại hình nội dung cần bảo vệ (chọn 1 hoặc nhiều)') }}</label>
        <input type="hidden" name="content_type[]">
        <div class="d-flex flex-wrap align-items-center gap-6">
            @foreach($types as $key => $value)
                <div class="form-check form-check-custom form-check-success">
                    <input class="form-check-input" type="checkbox" name="content_type[]" value="{{ $key }}" id="flexCheckDefault-content_type-{{ $key }}" {{ old('content_type') ? ((is_array(old('content_type')) && in_array($key, old('content_type'))) ? 'checked' : '') : ((isset($member) && is_array($member->content_type) && in_array($key, $member->content_type)) ? 'checked' : '') }}/>
                    <label class="form-check-label" for="flexCheckDefault-content_type-{{ $key }}">
                        {{ $value }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>
</div>

@canany(['member.create', 'member.edit'])
    @push('js')
        <script>
            initDropzoneUploadOneFile('#dropzonejs_cccd_front_photo')
            initDropzoneUploadOneFile('#dropzonejs_cccd_back_photo')
            initDropzoneUploadOneFile('#dropzonejs_face_photo')
            initDropzoneUploadOneFile('#dropzonejs_business_license')
            initDropzoneUploadOneFile('#dropzonejs_logo_company')
        </script>
    @endpush
@endcanany
