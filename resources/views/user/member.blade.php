@extends('layouts.app')
@section('title', __('Thành viên'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('member.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="Từ khóa" />
                                </div>
                                <select name="type_user" class="form-select fw-bold w-lg-150px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Loại tài khoản') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$typeUserLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('type_user') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="paid" class="form-select fw-bold w-lg-200px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Lệ phí') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$paymentStatusLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('paid') !== null && request('paid') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="status" class="form-select fw-bold w-lg-200px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Trạng thái') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$statusLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('member.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a href="{{ route('member.create') }}" class="btn btn-primary">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm thành viên') }}
                                    </a>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ __('Tài khoản') }}</th>
                                    <th class="min-w-125px">{{ __('Tên đăng nhập') }}</th>
                                    <th class="min-w-125px">{{ __('Số điện thoại') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Loại tài khoản') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Lệ phí') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Trạng thái') }}</th>
                                    <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($members->total() == 0)
                                    <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                                @else
                                    @php
                                        $prev_color = $current_color = '';
                                    @endphp
                                    @foreach($members as $index => $member)
                                        @php
                                            while ($current_color == $prev_color) {
                                                $current_color = config('default.color')[rand(1, 5)];
                                            }
                                            $prev_color = $current_color;
                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ ($members->currentPage() - 1) * $members->perPage() + $index+1 }}</td>
                                            <td class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                    <a href="{{ route('member.edit', $member->id) }}">
                                                        <div class="symbol-label fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $member->full_name)), 0, 1, 'UTF-8')) }}</div>
                                                    </a>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <a href="{{ route('member.edit', $member->id) }}" class="text-gray-800 text-hover-primary mb-1">{{ $member->full_name }}</a>
                                                    <span>{{ $member->email }}</span>
                                                </div>
                                            </td>
                                            <td>{{ $member->username }}</td>
                                            <td>{{ $member->phone }}</td>
                                            <td class="text-center">
                                                @if ($member->type_user)
                                                    <div class="badge badge-{{ $member->type_user === \App\Models\User::TYPE_USER_TO_CHUC ? 'danger' : 'success' }}">
                                                        {{ \App\Models\User::$typeUserLabel[$member->type_user] ?? '' }}
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <div class="badge badge-{{ $member->paid ? 'primary' : 'warning' }}">
                                                    {{ \App\Models\User::$paymentStatusLabel[$member->paid] ?? \App\Models\User::$paymentStatusLabel[\App\Models\User::PAID] }}
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="form-check form-switch d-inline-block">
                                                    <input class="form-check-input @can('member.edit') change-status-user @endcan" type="checkbox" role="switch" data-user_id="{{ $member->id }}" {{ $member->status === \App\Models\User::STATUS_ACTIVE ? 'checked' : '' }} @cannot('member.edit') disabled @endcannot>
                                                </div>
                                            </td>
                                            <td class="text-end">
                                                <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                    <i class="ki-outline ki-down fs-5 ms-1"></i>
                                                </a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                    @can('member.edit')
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3 btn-reset-password" data-user_id="{{ $member->id }}" data-username="{{ $member->username }}">{{ __('Lấy lại') }}</a>
                                                        </div>
                                                    @endcan
                                                    @canany(['member.view', 'member.edit'])
                                                        <div class="menu-item px-3">
                                                            <a href="{{ route('member.edit', $member->id) }}" class="menu-link px-3 btn-edit-user">
                                                                @can('member.edit')
                                                                    {{ __('Cập nhật') }}
                                                                @else
                                                                    {{ __('Xem') }}
                                                                @endcan
                                                            </a>
                                                        </div>
                                                    @endcanany
                                                    @can('member.delete')
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3 text-danger btn-delete-user" data-user_id="{{ $member->id }}" data-username="{{ $member->username }}">{{ __('Xóa') }}</a>
                                                        </div>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                               @endif
                            </tbody>
                        </table>
                        {{ $members->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.change-status-user').change(function() {
            const user_id = $(this).data('user_id')
            $.ajax({
                url: '{{ route('user.change_status') }}',
                type: 'PUT',
                data: {
                    _token: '{{ csrf_token() }}',
                    user_id
                },
                success: function (result) {
                    if (result.status) {
                        toastr.success("{{ __('Cập nhật trạng thái thành công') }}");
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    console.log(error)
                    toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                }
            }).always(function () {

            })
        })

        $('.btn-reset-password').click(function(e) {
            e.preventDefault()
            const user_id = $(this).data('user_id')
            const username = $(this).data('username')
            Swal.fire({
                title: "{{ __('Đặt lại mật khẩu') }}",
                html: "{{ __('Đặt lại mật khẩu tài khoản') }} <b>" + username + "</b> {{ __('về mật khẩu mặc định') . ': ' . config('default.password_default') }}?",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "Ok",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user.reset_password') }}',
                        type: 'PUT',
                        data: {
                            _token: '{{ csrf_token() }}',
                            user_id
                        },
                        success: function (response) {
                            if (response.status) {
                                Swal.fire({
                                    text: "{{ __('Đặt lại mật khẩu thành công') }}",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                })
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        },
                        error: function(error) {
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }).always(function () {

                    })
                }
            })
        })

        $('.btn-delete-user').click(function() {
            const user_id = $(this).data('user_id')
            const username = $(this).data('username')
            Swal.fire({
                title: "{{ __('Xóa tài khoản') }}",
                html: "{{ __('Bạn có chắc muốn xóa tài khoản') }} <b>" + username + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            user_id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa tài khoản thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
