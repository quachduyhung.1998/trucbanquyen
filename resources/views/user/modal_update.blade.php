<div class="modal-content">
    <div class="modal-header" id="kt_modal_update_user_header">
        <h2 class="fw-bold">{{ __('Cập nhật tài khoản') }}</h2>
        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
            <i class="ki-outline ki-cross fs-1"></i>
        </div>
    </div>
    <div class="modal-body px-5 my-7">
        <form id="kt_modal_update_user_form" class="form">
            @csrf
            <input type="hidden" name="id" value="{{ $user->id }}">
            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_update_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_user_header" data-kt-scroll-wrappers="#kt_modal_update_user_scroll" data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Họ tên') }}</label>
                    <input type="text" name="full_name" class="form-control mb-3 mb-lg-0" value="{{ $user->full_name }}" />
                    <span class="text-danger d-block mt-1 box-err-full_name"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Tên đăng nhập') }}</label>
                    <input type="text" name="username" class="form-control mb-3 mb-lg-0" value="{{ $user->username }}" />
                    <span class="text-danger d-block mt-1 box-err-username"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Vài trò') }}</label>
                    <select name="role" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn vai trò') }}" data-allow-clear="true" data-hide-search="true">
                        <option></option>
                        @foreach(\App\Models\User::$roleLabel as $key => $value)
                            <option value="{{ $key }}" {{ $user->role == $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    <span class="text-danger d-block mt-1 box-err-role"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="fw-semibold fs-6 mb-2">{{ __('Điện thoại') }}</label>
                    <input type="number" name="phone" class="form-control mb-3 mb-lg-0" value="{{ $user->phone }}" />
                    <span class="text-danger d-block mt-1 box-err-phone"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="fw-semibold fs-6 mb-2">Email</label>
                    <input type="email" name="email" class="form-control mb-3 mb-lg-0" placeholder="example@domain.com" value="{{ $user->email }}" />
                    <span class="text-danger d-block mt-1 box-err-email"></span>
                </div>
            </div>
            <div class="text-center pt-10">
                <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-users-modal-action="cancel">{{ __('Hủy') }}</button>
                @can('user.edit')
                    <button type="button" id="btn-submit-update-user" class="btn btn-primary" data-kt-users-modal-action="submit">
                        <span class="indicator-label">{{ __('Cập nhật') }}</span>
                        <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                        </span>
                    </button>
                @endcan
            </div>
        </form>
    </div>
</div>
