@extends('layouts.app')
@section('title', __('Tài khoản'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('user.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="Từ khóa" />
                                </div>
                                <select name="role_search" class="form-select fw-bold w-lg-200px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Vai trò') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$roleLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('role_search') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="status" class="form-select fw-bold w-lg-200px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Trạng thái') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$statusLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('user.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_user">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm tài khoản') }}
                                    </button>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5" id="kt_table_users">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ __('Tài khoản') }}</th>
                                    <th class="min-w-125px">{{ __('Tên đăng nhập') }}</th>
                                    <th class="min-w-125px">{{ __('Số điện thoại') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Vai trò') }}</th>
                                    <th class="min-w-125px">{{ __('Ngày tham gia') }}</th>
                                    <th class="min-w-125px text-center">{{ __('Trạng thái') }}</th>
                                    <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($users->total() == 0)
                                    <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                                @else
                                    @php
                                        $prev_color = $current_color = '';
                                    @endphp
                                    @foreach($users as $index => $user)
                                        @php
                                            while ($current_color == $prev_color) {
                                                $current_color = config('default.color')[rand(1, 5)];
                                            }
                                            $prev_color = $current_color;
                                        @endphp
                                        <tr>
                                            <td class="text-center">{{ ($users->currentPage() - 1) * $users->perPage() + $index+1 }}</td>
                                            <td class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                    <a href="javascript:void(0)" class="btn-edit-user" data-url="{{ route('user.edit', $user->id) }}">
                                                        <div class="symbol-label fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $user->full_name)), 0, 1, 'UTF-8')) }}</div>
                                                    </a>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <a href="javascript:void(0)" class="text-gray-800 text-hover-primary mb-1 btn-edit-user" data-url="{{ route('user.edit', $user->id) }}">{{ $user->full_name }}</a>
                                                    <span>{{ $user->email }}</span>
                                                </div>
                                            </td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td class="text-center">
                                                <div class="badge badge-{{ \App\Models\User::$roleColor[$user->role] ?? '' }}">
                                                    {{ \App\Models\User::$roleLabel[$user->role] ?? '' }}
                                                </div>
                                            </td>
                                            <td>{{ $user->created_at->format('H:i - d/m/Y') }}</td>
                                            <td class="text-center">
                                                <div class="form-check form-switch d-inline-block">
                                                    <input class="form-check-input @can('user.edit') change-status-user @endcan" type="checkbox" role="switch" data-user_id="{{ $user->id }}" {{ $user->status === \App\Models\User::STATUS_ACTIVE ? 'checked' : '' }} @cannot('user.edit') disabled @endcannot>
                                                </div>
                                            </td>
                                            <td class="text-end">
                                                @canany(['user.edit', 'user.delete'])
                                                    <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                        <i class="ki-outline ki-down fs-5 ms-1"></i>
                                                    </a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                        @can('user.edit')
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 btn-reset-password" data-user_id="{{ $user->id }}" data-username="{{ $user->username }}">{{ __('Lấy lại') }}</a>
                                                            </div>
                                                        @endcan
                                                        @can('user.edit')
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 btn-edit-user" data-url="{{ route('user.edit', $user->id) }}">{{ __('Cập nhật') }}</a>
                                                            </div>
                                                        @endcan
                                                        @can('user.delete')
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 text-danger btn-delete-user" data-user_id="{{ $user->id }}" data-username="{{ $user->username }}">{{ __('Xóa') }}</a>
                                                            </div>
                                                        @endcan
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                    @endforeach
                               @endif
                            </tbody>
                        </table>
                        {{ $users->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add user-->
    <div class="modal fade" id="kt_modal_add_user" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content">
                <div class="modal-header" id="kt_modal_add_user_header">
                    <h2 class="fw-bold">{{ __('Thêm tài khoản') }}</h2>
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                </div>
                <div class="modal-body px-5 my-7">
                    <form id="kt_modal_add_user_form" class="form">
                        @csrf
                        <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Họ tên') }}</label>
                                <input type="text" name="full_name" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-full_name"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên đăng nhập') }}</label>
                                <input type="text" name="username" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-username"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Mật khẩu') }}</label>
                                <input type="password" name="password" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-password"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Vài trò') }}</label>
                                <select name="role" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn vai trò') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\User::$roleLabel as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger d-block mt-1 box-err-role"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">{{ __('Điện thoại') }}</label>
                                <input type="number" name="phone" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-phone"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">Email</label>
                                <input type="email" name="email" class="form-control mb-3 mb-lg-0" placeholder="example@domain.com" />
                                <span class="text-danger d-block mt-1 box-err-email"></span>
                            </div>
                        </div>
                        <div class="text-center pt-10">
                            <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-users-modal-action="cancel">{{ __('Hủy') }}</button>
                            @can('user.create')
                                <button type="button" id="btn-submit-add-user" class="btn btn-primary" data-kt-users-modal-action="submit">
                                    <span class="indicator-label">{{ __('Lưu') }}</span>
                                    <span class="indicator-progress">{{ __('Đang lưu') }}...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                </button>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal - Add user-->

    <!--begin::Modal - Update user-->
    <div class="modal fade" id="kt_modal_update_user" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('.change-status-user').change(function() {
            const user_id = $(this).data('user_id')
            $.ajax({
                url: '{{ route('user.change_status') }}',
                type: 'PUT',
                data: {
                    _token: '{{ csrf_token() }}',
                    user_id
                },
                success: function (result) {
                    if (result.status) {
                        toastr.success("{{ __('Cập nhật trạng thái thành công') }}");
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    console.log(error)
                    toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                }
            }).always(function () {

            })
        })

        $('.btn-reset-password').click(function(e) {
            e.preventDefault()
            const user_id = $(this).data('user_id')
            const username = $(this).data('username')
            Swal.fire({
                title: "{{ __('Đặt lại mật khẩu') }}",
                html: "{{ __('Đặt lại mật khẩu tài khoản') }} <b>" + username + "</b> {{ __('về mật khẩu mặc định') . ': ' . config('default.password_default') }}?",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "Ok",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user.reset_password') }}',
                        type: 'PUT',
                        data: {
                            _token: '{{ csrf_token() }}',
                            user_id
                        },
                        success: function (response) {
                            if (response.status) {
                                Swal.fire({
                                    text: "{{ __('Đặt lại mật khẩu thành công') }}",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                })
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        },
                        error: function(error) {
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }).always(function () {

                    })
                }
            })
        })

        $('#btn-submit-add-user').click(function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let data = $('#kt_modal_add_user_form').serialize()
            $.ajax({
                url: '{{ route('user.store') }}',
                type: 'POST',
                data,
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Thêm tài khoản thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_add_user').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.full_name) {
                        $('#kt_modal_add_user_form .box-err-full_name').html(errors.full_name[0])
                        $('#kt_modal_add_user_form input[name="full_name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-full_name').html('')
                        $('#kt_modal_add_user_form input[name="full_name"]').removeClass('border-danger')
                    }
                    if (errors.username) {
                        $('#kt_modal_add_user_form .box-err-username').html(errors.username[0])
                        $('#kt_modal_add_user_form input[name="username"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-username').html('')
                        $('#kt_modal_add_user_form input[name="username"]').removeClass('border-danger')
                    }
                    if (errors.password) {
                        $('#kt_modal_add_user_form .box-err-password').html(errors.password[0])
                        $('#kt_modal_add_user_form input[name="password"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-password').html('')
                        $('#kt_modal_add_user_form input[name="password"]').removeClass('border-danger')
                    }
                    if (errors.role) {
                        $('#kt_modal_add_user_form .box-err-role').html(errors.role[0])
                        $('#kt_modal_add_user_form input[name="role"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-role').html('')
                        $('#kt_modal_add_user_form input[name="role"]').removeClass('border-danger')
                    }
                    if (errors.phone) {
                        $('#kt_modal_add_user_form .box-err-phone').html(errors.phone[0])
                        $('#kt_modal_add_user_form input[name="phone"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-phone').html('')
                        $('#kt_modal_add_user_form input[name="phone"]').removeClass('border-danger')
                    }
                    if (errors.email) {
                        $('#kt_modal_add_user_form .box-err-email').html(errors.email[0])
                        $('#kt_modal_add_user_form input[name="email"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_user_form .box-err-email').html('')
                        $('#kt_modal_add_user_form input[name="email"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-add-user .indicator-label').removeAttr('style')
                $('#btn-submit-add-user .indicator-progress').removeAttr('style')
                $('#btn-submit-add-user').attr('disabled', false)
            })
        })

        $('.btn-edit-user').click(function() {
            const url = $(this).data('url')
            $.ajax({
                url,
                beforeSend: KTApp.showPageLoading(),
                success: function(response) {
                    $('#kt_modal_update_user .modal-dialog').html(response)
                    $('#kt_modal_update_user').modal('show')
                }
            }).always(function() {
                KTApp.hidePageLoading()
            })
        })

        $(document).on('click', '#btn-submit-update-user', function() {
            const form = $(this).closest('form')
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let url = '{{ route('user.update') }}'
            let data = form.serialize()
            $.ajax({
                url,
                type: 'POST',
                data,
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật tài khoản thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_update_user').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.full_name) {
                        $('#kt_modal_update_user_form .box-err-full_name').html(errors.full_name[0])
                        $('#kt_modal_update_user_form input[name="full_name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-full_name').html('')
                        $('#kt_modal_update_user_form input[name="full_name"]').removeClass('border-danger')
                    }
                    if (errors.username) {
                        $('#kt_modal_update_user_form .box-err-username').html(errors.username[0])
                        $('#kt_modal_update_user_form input[name="username"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-username').html('')
                        $('#kt_modal_update_user_form input[name="username"]').removeClass('border-danger')
                    }
                    if (errors.password) {
                        $('#kt_modal_update_user_form .box-err-password').html(errors.password[0])
                        $('#kt_modal_update_user_form input[name="password"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-password').html('')
                        $('#kt_modal_update_user_form input[name="password"]').removeClass('border-danger')
                    }
                    if (errors.role) {
                        $('#kt_modal_update_user_form .box-err-role').html(errors.role[0])
                        $('#kt_modal_update_user_form input[name="role"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-role').html('')
                        $('#kt_modal_update_user_form input[name="role"]').removeClass('border-danger')
                    }
                    if (errors.phone) {
                        $('#kt_modal_update_user_form .box-err-phone').html(errors.phone[0])
                        $('#kt_modal_update_user_form input[name="phone"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-phone').html('')
                        $('#kt_modal_update_user_form input[name="phone"]').removeClass('border-danger')
                    }
                    if (errors.email) {
                        $('#kt_modal_update_user_form .box-err-email').html(errors.email[0])
                        $('#kt_modal_update_user_form input[name="email"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_user_form .box-err-email').html('')
                        $('#kt_modal_update_user_form input[name="email"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-update-user .indicator-label').removeAttr('style')
                $('#btn-submit-update-user .indicator-progress').removeAttr('style')
                $('#btn-submit-update-user').attr('disabled', false)
            })
        })

        $('.btn-delete-user').click(function() {
            const user_id = $(this).data('user_id')
            const username = $(this).data('username')
            Swal.fire({
                title: "{{ __('Xóa tài khoản') }}",
                html: "{{ __('Bạn có chắc muốn xóa tài khoản') }} <b>" + username + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('user.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            user_id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa tài khoản thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
