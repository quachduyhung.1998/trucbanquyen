<div class="row">
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

    @isset($copyrightReport)
        <div class="col-12 mb-5">
            <div class="d-flex flex-md-row flex-column align-items-md-center justify-content-between">
                <h3 class="mb-md-0 mb-3"></h3>
                <div class="d-flex flex-md-row flex-column align-items-md-center gap-md-5 gap-3">
                    <span class="fs-5">{{ __('Người tạo') }}: <b>{{ $copyrightReport->user?->full_name }}</b></span>
                    <span class="fs-5">{{ __('Trạng thái') }}: <span class="badge badge-lg badge-{{ \App\Models\CopyrightReport::$statusColor[$copyrightReport->status] ?? '' }}">{{ \App\Models\CopyrightReport::$statusLabel[$copyrightReport->status] ?? '' }}</span></span>
                </div>
            </div>
        </div>
    @endisset

    <div class="col-md-6 col-12 mb-7">
        <div class="row">
            <div class="col-12 mb-5"><h3 class="mb-md-0 mb-3">{{ __('Thông tin sản phẩm') }}</h3></div>

            <div class="col-12 mb-7">
                <div class="d-flex flex-sm-row flex-column align-items-sm-center justify-content-between">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Sản phẩm') }}</label>
                    <div class="form-check mb-2">
                        <input type="hidden" name="type_object" value="{{ \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED }}">
                        <input name="type_object" class="form-check-input input-type-object" type="checkbox" value="{{ \App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED }}" {{ old('type_object') ? (old('type_object') == \App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED ? 'checked' : '') : (isset($copyrightReport) && $copyrightReport->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED ? 'checked' : '') }} id="flexCheckTypeObject" />
                        <label class="form-check-label" for="flexCheckTypeObject">
                            {{ \App\Models\CopyrightReport::$typeObjectLabel[\App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED] ?? 'Ngoài danh sách bảo vệ bản quyền' }}
                        </label>
                    </div>
                </div>
                <div class="box-input-object" style="{{ old('type_object') ? (old('type_object') == \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED ? 'display: none' : '') : (isset($copyrightReport) ? ($copyrightReport->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED ? 'display: none' : '') : 'display: none') }}">
                    <input type="text" name="object_out" class="form-control mb-3 mb-lg-0 @error('object_out') border-danger @enderror" value="{{ old('object_out', isset($copyrightReport) ? $copyrightReport->object : '') }}" />
                </div>
                <div class="box-select-object" style="{{ old('type_object') ? (old('type_object') == \App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED ? 'display: none' : '') : (isset($copyrightReport) ? ($copyrightReport->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED ? 'display: none' : '') : '') }}">
                    <select name="object_in" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn sản phẩm') }}" data-allow-clear="true" data-hide-search="true">
                        <option></option>
                        @foreach($copyrightProtects as $key => $value)
                            <option value="{{ $key }}" {{ old('object_in') ? (old('object_in') == $key ? 'selected' : '') : ((isset($copyrightReport) && $copyrightReport->object == $key) ? 'selected' : '') }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                @error('object_in')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
                @error('object_out')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Lĩnh vực') }}</label>
                <select name="field_id" class="form-select field_select fw-bold @error('field_id') border-danger @enderror" data-kt-select2="true" data-placeholder="{{ __('Chọn lĩnh vực') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($fields as $key => $value)
                        <option value="{{ $key }}" {{ old('field_id') ? (old('field_id') == $key ? 'selected' : '') : ((isset($copyrightReport) && $copyrightReport->field_id == $key) ? 'selected' : '') }}>{{ $value }}</option>
                    @endforeach
                </select>
                @error('field_id')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                <textarea name="description" id="kt_docs_ckeditor" class="form-control mb-3 mb-lg-0">{{ old('description', isset($copyrightReport) ? $copyrightReport->description : '') }}</textarea>
            </div>

            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Hồ sơ đăng ký bản quyền') }}</label>
                <!--begin::Dropzone-->
                <div class="dropzone dropzone-with-field @if(!isset($copyrightReport) || !$copyrightReport->field_id) disable-dropzone @endif @error('url_contents') border-danger bg-light-danger @enderror {{ auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightReport) && $copyrightReport->status === \App\Models\CopyrightReport::STATUS_CANCEL ? 'cursor-not-allowed' : '' }}" id="dropzonejs_url_contents">
                    <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                        <i class="ki-duotone ki-file-up fs-3x text-primary @error('url_contents') text-danger @enderror"><span class="path1"></span><span class="path2"></span></i>
                        <div class="ms-4">
                            <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Chọn file') }}</h3>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Tải lên tối đa 10 tài liệu liên quan') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Nội dung, quá trình phát hiện vi phạm') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Hình ảnh bằng chứng vi phạm, ghi rõ thời điểm vi phạm') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Các tài liệu khác nhằm cung cấp thông tin báo cáo vi phạm') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Loại file cho phép: x-wav, wav, mpeg, mp3, pdf, jpeg, png, mp4, doc, docx') }}</div>
                            <div class="fs-7 fw-semibold text-gray-500">- {{ __('Dung lượng tối đa: 200MB/file') }}</div>
                        </div>
                    </div>
                </div>
                <!--end::Dropzone-->
                @error('url_contents')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror

                <div class="mt-5 list-image box-url_contents">
                    @if(old('url_contents'))
                        @foreach(old('url_contents') as $image)
                            <div class="image-item mb-2 p-3 bg-light-info rounded">
                                @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightReport) && $copyrightReport->status !== \App\Models\CopyrightReport::STATUS_CANCEL))
                                    <div class="overlay-layer"></div>
                                    <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                        <a href="{{ \App\Helpers\FileHelper::generateLink($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                        <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                            <span class="path1"></span>
                                            <span class="path2"></span>
                                            <span class="path3"></span>
                                            <span class="path4"></span>
                                        </i>
                                    </div>
                                    <input type="hidden" name="url_contents[]" value="{{ $image }}">
                                @endif
                                @php $urlCustom = explode('/', $image) @endphp
                                <span>{{ end($urlCustom) }}</span>
                            </div>
                        @endforeach
                    @elseif(isset($copyrightReport) && $copyrightReport->url_contents)
                        @foreach($copyrightReport->url_contents as $image)
                            <div class="image-item mb-2 p-3 bg-light-info rounded">
                                @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightReport) && $copyrightReport->status !== \App\Models\CopyrightReport::STATUS_CANCEL))
                                    <div class="overlay-layer"></div>
                                    <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                        <a href="{{ \App\Helpers\FileHelper::generateLink($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                        <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                            <span class="path1"></span>
                                            <span class="path2"></span>
                                            <span class="path3"></span>
                                            <span class="path4"></span>
                                        </i>
                                    </div>
                                    <input type="hidden" name="url_contents[]" value="{{ $image }}">
                                @endif
                                @php $urlCustom = explode('/', $image) @endphp
                                <span>{{ end($urlCustom) }}</span>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="row">
            <div class="col-12 mb-5"><h3 class="mb-md-0 mb-3">{{ __('Thông tin bên vi phạm') }}</h3></div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Tên chủ thể') }}</label>
                <input type="text" name="subject_name" class="form-control mb-3 mb-lg-0" value="{{ old('subject_name', isset($copyrightReport) ? $copyrightReport->subject_name : '') }}" />
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Số điện thoại') }}</label>
                <input type="number" name="subject_phone" class="form-control mb-3 mb-lg-0 @error('subject_phone') border-danger @enderror" value="{{ old('subject_phone', isset($copyrightReport) ? $copyrightReport->subject_phone : '') }}" />
                @error('subject_phone')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Email') }}</label>
                <input type="email" name="subject_email" class="form-control mb-3 mb-lg-0 @error('subject_email') border-danger @enderror" placeholder="example@domain.com" value="{{ old('subject_email', isset($copyrightReport) ? $copyrightReport->subject_email : '') }}" />
                @error('subject_email')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Website') }}</label>
                <input type="text" name="subject_website" class="form-control mb-3 mb-lg-0" value="{{ old('subject_website', isset($copyrightReport) ? $copyrightReport->subject_website : '') }}" />
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Địa chỉ') }}</label>
                <input type="text" name="subject_address" class="form-control mb-3 mb-lg-0" value="{{ old('subject_address', isset($copyrightReport) ? $copyrightReport->subject_address : '') }}" />
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Vi phạm từ ngày') }}</label>
                <input name="violation_from_date" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('violation_from_date', isset($member) ? $member->violation_from_date : '') }}" />
            </div>

            <div class="col-md-6 col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Vi phạm đến khi') }}</label>
                <input name="violation_to_date" class="form-control mb-3 mb-lg-0 datepicker" placeholder="{{ __('Chọn ngày') }}" value="{{ old('violation_to_date', isset($member) ? $member->violation_to_date : '') }}" />
            </div>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {

            })
            .catch(error => {
                console.error(error);
            });

        @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && (!isset($copyrightReport) || $copyrightReport->status !== \App\Models\CopyrightReport::STATUS_CANCEL)))
            function generateHtml(urlPreview, inputValue, inputHiddenName) {
                let linkDisplay = inputValue.split('/')
                return `<div class="image-item mb-2 p-3 bg-light-info rounded">
                    <div class="overlay-layer"></div>
                    <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                        <a href="${urlPreview}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                        <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                            <span class="path4"></span>
                        </i>
                    </div>
                    <input type="hidden" name="${inputHiddenName}" value="${inputValue}">
                    <span>${linkDisplay[linkDisplay.length - 1]}</span>
                </div>`
            }

            let canUploadFiles = 10 - parseInt('{{ (isset($copyrightReport) && $copyrightReport->url_contents) ? count($copyrightReport->url_contents) : 0 }}');
            initDropzoneUploadMultiFile('#dropzonejs_url_contents', function (response, file) {
                if (!file.type.includes('video')) {
                    $('.list-image.box-url_contents').append(generateHtml(response.url, response.url, 'url_contents[]'))
                } else {
                    saveToLibrary(response, file)
                    $('.list-image.box-url_contents').append(generateHtml('{{ config('default.url_server_file') }}'+response.data.link, response.data.link, 'url_contents[]'))
                }
            }, canUploadFiles, 200, '.x-wav, .wav, .mpeg, .mp3, .pdf, .jpeg, .png, .mp4, .doc, .docx')

            function saveToLibrary(response, file) {
                const file_id = response.data.video_id
                const title = file.name
                const author = response.data.author
                const url = response.data.link
                const thumb = response.data.thumbnail
                const duration = response.data.duration
                const type = file.type
                const size = file.size
                const field_id = $('.field_select').val()
                $.ajax({
                    url: '{{ route('library.store') }}',
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        file_id,
                        title,
                        author,
                        url,
                        thumb,
                        duration,
                        type,
                        size,
                        field_id
                    },
                    success: function (response) {
                        if (response.status) {
                            {{--toastr.success("{{ __('Thêm đối tượng thành công') }}");--}}
                        } else {
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }
                }).always(function () {
                    KTApp.hidePageLoading()
                })
            }

            $(document).on('click', '.btn-remove-image', function () {
                Swal.fire({
                    title: "{{ __('Bạn có chắc muốn xóa tệp này không?') }}",
                    icon: "question",
                    buttonsStyling: false,
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Xóa') }}",
                    cancelButtonText: "{{ __("Hủy") }}",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-light-secondary text-black-50"
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).closest('.image-item').remove()
                    }
                })

            })
        @endif

        $('#flexCheckTypeObject').change(function() {
            if ($(this).is(':checked')) {
                $('.box-input-object').removeAttr('style')
                $('.box-select-object').css('display', 'none')
            } else {
                $('.box-select-object').removeAttr('style')
                $('.box-input-object').css('display', 'none')
            }
        })
    </script>
@endpush
