@extends('layouts.app')
@section('title', __('Thông tin tài khoản'))

@section('content')
    @csrf
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--begin::Basic info-->
                <div class="card mb-5 mb-xl-10">
                    <!--begin::Card header-->
                    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">{{ __('Thông tin tài khoản') }}</h3>
                        </div>
                        <!--end::Card title-->
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Content-->
                    <div id="kt_account_settings_profile_details" class="collapse show">
                        <!--begin::Form-->
                        <form id="kt_account_profile_details_form" class="form">
                            <!--begin::Card body-->
                            <div class="card-body border-top p-9">
                                <!--begin::Input group-->
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-semibold fs-6">{{ __('Ảnh đại diện') }}</label>
                                    <div class="col-lg-8">
                                        <!--begin::Image input-->
                                        <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url('{{ asset('assets/media/svg/avatars/blank.png') }}')">
                                            <!--begin::Preview existing avatar-->
                                            <div class="image-input-wrapper w-125px h-125px" style="background-image: url('{{ asset('assets/media/avatars/blank.png') }}')"></div>
                                            <!--end::Preview existing avatar-->
            {{--                                <!--begin::Label-->--}}
            {{--                                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">--}}
            {{--                                    <i class="ki-outline ki-pencil fs-7"></i>--}}
            {{--                                    <!--begin::Inputs-->--}}
            {{--                                    <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />--}}
            {{--                                    <input type="hidden" name="avatar_remove" />--}}
            {{--                                    <!--end::Inputs-->--}}
            {{--                                </label>--}}
            {{--                                <!--end::Label-->--}}
            {{--                                <!--begin::Cancel-->--}}
            {{--                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">--}}
            {{--                                    <i class="ki-outline ki-cross fs-2"></i>--}}
            {{--                                </span>--}}
            {{--                                <!--end::Cancel-->--}}
            {{--                                <!--begin::Remove-->--}}
            {{--                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">--}}
            {{--                                    <i class="ki-outline ki-cross fs-2"></i>--}}
            {{--                                </span>--}}
            {{--                                <!--end::Remove-->--}}
                                        </div>
                                        <!--end::Image input-->
                                        <!--begin::Hint-->
            {{--                            <div class="form-text">{{ __('Các loại tệp được phép: png, jpg, jpeg.') }}</div>--}}
                                        <!--end::Hint-->
                                    </div>
                                </div>
                                <!--end::Input group-->
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label required fw-semibold fs-6">{{ __('Họ tên') }}</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="full_name" class="form-control form-control-lg" placeholder="{{ __('Họ tên') }}" value="{{ $user->full_name }}" />
                                        <span class="text-danger d-block mt-1 box-err-full_name"></span>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-semibold fs-6">{{ __('Email') }}</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="email" name="email" class="form-control form-control-lg" placeholder="Email" value="{{ $user->email }}" />
                                        <span class="text-danger d-block mt-1 box-err-email"></span>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-semibold fs-6">{{ __('Số điện thoại') }}</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="text" name="phone_number" class="form-control form-control-lg" placeholder="{{ __('Số điện thoại') }}" value="{{ $user->phone }}" />
                                        <span class="text-danger d-block mt-1 box-err-phone"></span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Card body-->
                            <!--begin::Actions-->
                            <div class="card-footer d-flex justify-content-end py-6 px-9">
{{--                                <button type="reset" class="btn btn-light btn-active-light-primary me-2">{{ __('Quay lại') }}</button>--}}
                                <button type="button" class="btn btn-primary" id="kt_account_profile_details_submit">
                                    <span class="indicator-label">{{ __('Lưu thay đổi') }}</span>
                                    <span class="indicator-progress">{{ __('Đang lưu') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </button>
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Basic info-->

                <!--begin::Sign-in Method-->
                <div class="card mb-5 mb-xl-10">
                    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_signin_method">
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">{{ __('Phương thức đăng nhập') }}</h3>
                        </div>
                    </div>
                    <div id="kt_account_settings_signin_method" class="collapse show">
                        <div class="card-body border-top p-9">
                            <div class="d-flex flex-wrap align-items-center">
                                <div id="kt_signin_email">
                                    <div class="fs-6 fw-bold mb-1">{{ __('Tên đăng nhập') }}</div>
                                    <div class="fw-semibold text-gray-600 div-show-username">{{ $user->username }}</div>
                                </div>
                                <div id="kt_signin_email_edit" class="flex-row-fluid d-none">
                                    <!--begin::Form-->
                                    <form id="kt_signin_change_email" class="form" novalidate="novalidate">
                                        <div class="row mb-6">
                                            <div class="col-lg-6 mb-4 mb-lg-0">
                                                <div class="fv-row mb-0">
                                                    <label for="username" class="form-label fs-6 fw-bold mb-3">{{ __('Nhập tên đăng nhập mới') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="username" placeholder="{{ __('Tên đăng nhập') }}" name="username" value="{{ $user->username }}" />
                                                    <span class="text-danger d-block mt-1 box-err-username"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="fv-row mb-0">
                                                    <label for="confirm_username_password" class="form-label fs-6 fw-bold mb-3">{{ __('Nhập mật khẩu để xác nhận') }}</label>
                                                    <input type="password" class="form-control form-control-lg" name="confirm_username_password" id="confirm_username_password" />
                                                    <span class="text-danger d-block mt-1 box-err-confirm_username_password"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex">
                                            <button id="kt_signin_submit_ajax" type="button" class="btn btn-primary me-2 px-6">
                                                <span class="indicator-label">{{ __('Cập nhật tên đăng nhập') }}</span>
                                                <span class="indicator-progress">{{ __('Đang cập nhật') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span></button>
                                            <button id="kt_signin_cancel" type="button" class="btn btn-color-gray-500 btn-active-light-primary px-6">{{ __('Hủy') }}</button>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <div id="kt_signin_email_button" class="ms-auto">
                                    <button class="btn btn-light btn-active-light-primary">{{ __('Đổi tên đăng nhập')  }}</button>
                                </div>
                            </div>

                            <div class="separator separator-dashed my-6"></div>

                            <div class="d-flex flex-wrap align-items-center mb-10">
                                <div id="kt_signin_password">
                                    <div class="fs-6 fw-bold mb-1">{{ __('Mật khẩu') }}</div>
                                    <div class="fw-semibold text-gray-600">************</div>
                                </div>
                                <div id="kt_signin_password_edit" class="flex-row-fluid d-none">
                                    <!--begin::Form-->
                                    <form id="kt_signin_change_password" class="form" novalidate="novalidate">
                                        <div class="row mb-1">
                                            <div class="col-lg-4">
                                                <div class="fv-row mb-0">
                                                    <label for="current_password" class="form-label fs-6 fw-bold mb-3">{{ __('Mật khẩu hiện tại') }}</label>
                                                    <input type="password" class="form-control form-control-lg" name="current_password" id="current_password" />
                                                    <span class="text-danger d-block mt-1 box-err-current_password"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row mb-0">
                                                    <label for="new_password" class="form-label fs-6 fw-bold mb-3">{{ __('Mật khẩu mới') }}</label>
                                                    <input type="password" class="form-control form-control-lg" name="new_password" id="new_password" min="8" />
                                                    <span class="text-danger d-block mt-1 box-err-new_password"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row mb-0">
                                                    <label for="new_password_confirm" class="form-label fs-6 fw-bold mb-3">{{ __('Nhập lại mật khẩu mới') }}</label>
                                                    <input type="password" class="form-control form-control-lg" name="new_password_confirm" id="new_password_confirm" />
                                                    <span class="text-danger d-block mt-1 box-err-new_password_confirm"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-text mb-5">{{ __('Mật khẩu mới phải có ít nhất 8 ký tự') }}</div>
                                        <div class="d-flex">
                                            <button id="kt_password_submit_ajax" type="button" class="btn btn-primary me-2 px-6">
                                                <span class="indicator-label">{{ __('Cập nhật mật khẩu') }}</span>
                                                <span class="indicator-progress">{{ __('Đang cập nhật') }}...<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                            </button>
                                            <button id="kt_password_cancel" type="button" class="btn btn-color-gray-500 btn-active-light-primary px-6">{{ __('Hủy') }}</button>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <div id="kt_signin_password_button" class="ms-auto">
                                    <button class="btn btn-light btn-active-light-primary">{{ __('Đổi mật khẩu')  }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Sign-in Method-->
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/js/custom/account/settings/signin-methods.js') }}"></script>
    <script src="{{ asset('assets/js/custom/account/settings/profile-details.js') }}"></script>

    <script>
        let _token = $('input[name="_token"]').val()

        // profile
        $('#kt_account_profile_details_submit').click(function() {
            let full_name = $('input[name="full_name"]').val()
            let email = $('input[name="email"]').val()
            let phone = $('input[name="phone_number"]').val()
            if (!full_name.length) {
                $('.box-err-full_name').html('{{ __('Bạn chưa nhập họ tên') }}')
                $('input[name="full_name"]').addClass('border-danger')
                return
            }
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            $.ajax({
                url: '{{ route('profile.change_profile') }}',
                type: 'PUT',
                data: {
                    _token,
                    full_name,
                    email,
                    phone
                },
                success: function (response) {
                    if (response.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })
                        $('.box-err-full_name').html('')
                        $('input[name="full_name"]').removeClass('border-danger')
                        $('.box-err-email').html('')
                        $('input[name="email"]').removeClass('border-danger')
                        $('.box-err-phone').html('')
                        $('input[name="phone_number"]').removeClass('border-danger')
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.full_name) {
                        $('.box-err-full_name').html(errors.full_name[0])
                        $('input[name="full_name"]').addClass('border-danger')
                    } else {
                        $('.box-err-full_name').html('')
                        $('input[name="full_name"]').removeClass('border-danger')
                    }
                    if (errors.email) {
                        $('.box-err-email').html(errors.email[0])
                        $('input[name="email"]').addClass('border-danger')
                    } else {
                        $('.box-err-email').html('')
                        $('input[name="email"]').removeClass('border-danger')
                    }
                    if (errors.phone) {
                        $('.box-err-phone').html(errors.phone[0])
                        $('input[name="phone_number"]').addClass('border-danger')
                    } else {
                        $('.box-err-phone').html('')
                        $('input[name="phone_number"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#kt_account_profile_details_submit .indicator-label').removeAttr('style')
                $('#kt_account_profile_details_submit .indicator-progress').removeAttr('style')
                $('#kt_account_profile_details_submit').attr('disabled', false)
            })
        })

        // username
        let currentUsername = '{{ $user->username }}'
        let signingButton = $('#kt_signin_submit_ajax')
        signingButton.attr('disabled', true)
        $('input[name="username"]').on('input', function() {
            if ($(this).val().length && $(this).val() !== currentUsername) {
                signingButton.removeAttr('disabled')
            } else {
                signingButton.attr('disabled', true)
            }
        })
        $('input[name="confirm_username_password"]').on('input', function() {
            if (!$(this).val().length) {
                $('.box-err-confirm_username_password').html('{{ __('Bạn chưa nhập mật khẩu xác nhận') }}')
                $('input[name="confirm_username_password"]').addClass('border-danger')
            } else {
                $('.box-err-confirm_username_password').html('')
                $('input[name="confirm_username_password"]').removeClass('border-danger')
            }
        })

        signingButton.click(function() {
            let username = $('input[name="username"]').val()
            let confirm_username_password = $('input[name="confirm_username_password"]').val()
            if (!confirm_username_password.length) {
                $('.box-err-confirm_username_password').html('{{ __('Bạn chưa nhập mật khẩu xác nhận') }}')
                $('input[name="confirm_username_password"]').addClass('border-danger')
                return
            }
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            $.ajax({
                url: '{{ route('profile.change_username') }}',
                type: 'PUT',
                data: {
                    _token,
                    username,
                    confirm_username_password
                },
                success: function (response) {
                    if (response.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật tên đăng nhập thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })
                        currentUsername = username
                        $('input[name="username"]').val(username)
                        $('.div-show-username').html(username)
                        $('#kt_signin_change_email').trigger('reset')
                        $('#kt_signin_email').toggleClass("d-none")
                        $('#kt_signin_email_button').toggleClass("d-none")
                        $('#kt_signin_email_edit').toggleClass("d-none")
                        $('.box-err-username').html('')
                        $('input[name="username"]').removeClass('border-danger')
                        $('.box-err-confirm_username_password').html('')
                        $('input[name="confirm_username_password"]').removeClass('border-danger')
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.username) {
                        $('.box-err-username').html(errors.username[0])
                        $('input[name="username"]').addClass('border-danger')
                    } else {
                        $('.box-err-username').html('')
                        $('input[name="username"]').removeClass('border-danger')
                    }
                    if (errors.confirm_username_password) {
                        $('.box-err-confirm_username_password').html(errors.confirm_username_password[0])
                        $('input[name="confirm_username_password"]').addClass('border-danger')
                    } else {
                        $('.box-err-confirm_username_password').html('')
                        $('input[name="confirm_username_password"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#kt_signin_submit_ajax .indicator-label').removeAttr('style')
                $('#kt_signin_submit_ajax .indicator-progress').removeAttr('style')
                signingButton.attr('disabled', false)
            })
        })

        // mật khẩu
        let changePasswordButton = $('#kt_password_submit_ajax')
        let currentPassword = $('input[name="current_password"]')
        let newPassword = $('input[name="new_password"]')
        let newPasswordConfirm = $('input[name="new_password_confirm"]')
        changePasswordButton.attr('disabled', true)
        currentPassword.on('input', function() {
            if (!$(this).val().length) {
                $('.box-err-current_password').html('{{ __('Bạn chưa nhập mật khẩu cũ') }}')
                $('input[name="current_password"]').addClass('border-danger')
                changePasswordButton.attr('disabled', true)
            } else {
                $('.box-err-current_password').html('')
                $('input[name="current_password"]').removeClass('border-danger')
                changePasswordButton.removeAttr('disabled')
            }
        })
        newPassword.on('input', function() {
            if (!$(this).val().length) {
                $('.box-err-new_password').html('{{ __('Bạn chưa nhập mật khẩu mới') }}')
                $('input[name="new_password"]').addClass('border-danger')
            } else {
                $('.box-err-new_password').html('')
                $('input[name="new_password"]').removeClass('border-danger')
            }
        })
        newPasswordConfirm.on('input', function() {
            if (!$(this).val().length) {
                $('.box-err-new_password_confirm').html('{{ __('Bạn chưa nhập mật khẩu xác nhận') }}')
                $('input[name="new_password_confirm"]').addClass('border-danger')
            } else {
                $('.box-err-new_password_confirm').html('')
                $('input[name="new_password_confirm"]').removeClass('border-danger')
            }
        })
        changePasswordButton.click(function() {
            let current_password = currentPassword.val()
            let new_password = newPassword.val()
            let new_password_confirm = newPasswordConfirm.val()

            let error = false
            if (!current_password.length) {
                $('.box-err-current_password').html('{{ __('Bạn chưa nhập mật khẩu cũ') }}')
                currentPassword.addClass('border-danger')
                error = true
            }
            if (!new_password.length) {
                $('.box-err-new_password').html('{{ __('Bạn chưa nhập mật khẩu mới') }}')
                newPassword.addClass('border-danger')
                error = true
            }
            if (!new_password_confirm.length) {
                $('.box-err-new_password_confirm').html('{{ __('Bạn chưa nhập mật khẩu xác nhận') }}')
                newPasswordConfirm.addClass('border-danger')
                error = true
            }
            if (new_password.length && new_password_confirm.length && new_password.length !== new_password_confirm.length) {
                $('.box-err-new_password_confirm').html('{{ __('Mật khẩu không trùng khớp') }}')
                newPasswordConfirm.addClass('border-danger')
                error = true
            }
            if (error) return

            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            $.ajax({
                url: '{{ route('profile.change_password') }}',
                type: 'PUT',
                data: {
                    _token,
                    current_password,
                    new_password,
                    new_password_confirm
                },
                success: function (response) {
                    if (response.status) {
                        Swal.fire({
                            text: "{{ __('Đổi mật khẩu thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })
                        $('#kt_signin_change_password').trigger('reset')
                        $('#kt_signin_password').toggleClass("d-none")
                        $('#kt_signin_password_button').toggleClass("d-none")
                        $('#kt_signin_password_edit').toggleClass("d-none")
                        $('.box-err-current_password').html('')
                        currentPassword.removeClass('border-danger')
                        $('.box-err-new_password').html('')
                        newPassword.removeClass('border-danger')
                        $('.box-err-new_password_confirm').html('')
                        newPasswordConfirm.removeClass('border-danger')
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.current_password) {
                        $('.box-err-current_password').html(errors.current_password[0])
                        currentPassword.addClass('border-danger')
                    } else {
                        $('.box-err-current_password').html('')
                        currentPassword.removeClass('border-danger')
                    }
                    if (errors.new_password) {
                        $('.box-err-new_password').html(errors.new_password[0])
                        newPassword.addClass('border-danger')
                    } else {
                        $('.box-err-new_password').html('')
                        newPassword.removeClass('border-danger')
                    }
                    if (errors.new_password_confirm) {
                        $('.box-err-new_password_confirm').html(errors.new_password_confirm[0])
                        newPasswordConfirm.addClass('border-danger')
                    } else {
                        $('.box-err-new_password_confirm').html('')
                        newPasswordConfirm.removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#kt_password_submit_ajax .indicator-label').removeAttr('style')
                $('#kt_password_submit_ajax .indicator-progress').removeAttr('style')
                changePasswordButton.attr('disabled', false)
            })
        })
    </script>
@endpush
