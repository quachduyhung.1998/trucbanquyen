<div class="row">
    <div class="col-12 mb-5"><h3>{{ __('Cập nhật đối tượng bản quyền') }}: {{ $objectCopyright->name }}</h3></div>

    <div class="col-md-6 col-12 mb-7">
        @if ($objectCopyright->library)
            @if($objectCopyright->library->type === \App\Models\Library::TYPE_IMAGE)
                <img src="{{ \App\Helpers\FileHelper::generateLink($objectCopyright->library->url) }}" alt="{{ $objectCopyright->name ?? '' }}" class="w-100" style="max-height: 500px">
            @else
                <video id="player" class="w-100" style="max-height: 500px" playsinline controls data-poster="{{ $objectCopyright->library->thumb ? \App\Helpers\FileHelper::generateLink($objectCopyright->library->thumb) : asset('assets/media/logos/ttbqs-logo.svg') }}">
                    <source src="{{ \App\Helpers\FileHelper::generateLink($objectCopyright->library->url) }}" type="video/mp4" />
                </video>
            @endif
        @endif
    </div>

    <div class="col-md-6 col-12 mb-7">
        <div class="mb-7">
            <label class="required fw-semibold fs-6 mb-2">{{ __('Tên bản quyền') }}</label>
            <input type="text" name="name" class="form-control mb-3 mb-lg-0 @error('name') border-danger @enderror" value="{{ old('name', isset($objectCopyright) ? $objectCopyright->name : '') }}"/>
            @error('name')
                <span class="text-danger d-block mt-1">{{ $message }}</span>
            @enderror
        </div>

        <div>
            <label class="fw-semibold fs-6 mb-2">{{ __('Tên tác giả') }}</label>
            <input type="text" name="author" class="form-control mb-3 mb-lg-0" value="{{ old('author', isset($objectCopyright) ? $objectCopyright->author : '') }}"/>
        </div>
    </div>
</div>
