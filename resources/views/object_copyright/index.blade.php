@extends('layouts.app')
@section('title', __('Đối tượng bản quyền'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}"
                                   class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('object_copyright.index') }}"
                                  class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}"
                                           data-kt-user-table-filter="search"
                                           class="form-control w-lg-350px w-100 ps-13" placeholder="Từ khóa"/>
                                </div>
                                <select name="type" class="form-select fw-bold w-lg-150px w-100 my-1"
                                        data-kt-select2="true" data-placeholder="{{ __('Loại') }}"
                                        data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\Library::$typeLabel as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ request('type') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="field_id" class="form-select fw-bold w-lg-250px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Chọn lĩnh vực') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach($fields as $key => $value)
                                        <option value="{{ $key }}" {{ request('field_id') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="user_id" class="form-select fw-bold w-lg-250px w-100 my-1"
                                        data-kt-select2="true" data-placeholder="{{ __('Tác giả') }}"
                                        data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach($users as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ request('user_id') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        @can('object_copyright.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a href="" class="btn btn-primary" data-bs-toggle="modal"
                                       data-bs-target="#kt_modal_add_ai_approve">
                                        <i class="ki-outline ki-file-up fs-2"></i>{{ __('Tải lên') }}
                                    </a>
                                </div>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="text-center">{{ __('STT') }}</th>
                                <th class="min-w-125px">{{ __('Ảnh') }}</th>
                                <th class="min-w-125px">{{ __('Tên bản quyền') }}</th>
                                <th class="min-w-125px text-center">{{ __('Loại') }}</th>
                                <th class="min-w-125px text-center">{{ __('Lĩnh vực') }}</th>
                                <th class="min-w-125px">{{ __('Tác giả') }}</th>
                                <th class="min-w-125px">{{ __('Chủ sở hữu') }}</th>
                                <th class="min-w-125px">{{ __('Thời gian tạo') }}</th>
                                <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @if ($objectCopyrights->total() == 0)
                                <tr>
                                    <td colspan="8">{{ __('Không có dữ liệu') }}</td>
                                </tr>
                            @else
                                @foreach($objectCopyrights as $index => $objectCopyright)
                                    <tr>
                                        <td class="text-center">{{ ($objectCopyrights->currentPage() - 1) * $objectCopyrights->perPage() + $index+1 }}</td>
                                        <td class="d-flex align-items-center">
                                            <a href="{{ route('object_copyright.edit', $objectCopyright->id) }}">
                                                <img
                                                    src="{{ \App\Helpers\FileHelper::generateLink($objectCopyright->library && $objectCopyright->library->type === \App\Models\Library::TYPE_IMAGE ? $objectCopyright->library->url : $objectCopyright->library->thumb) }}"
                                                    alt="{{ $objectCopyright->name }}" class="w-100px rounded">
                                            </a>
                                        </td>
                                        <td>{{ $objectCopyright->name }}</td>
                                        <td class="text-center">
                                            @if ($objectCopyright->library?->type)
                                                <span
                                                    class="badge badge-{{ $objectCopyright->library?->type === \App\Models\Library::TYPE_IMAGE ? 'danger' : 'success' }}">
                                                        {{ \App\Models\Library::$typeLabel[$objectCopyright->library?->type] ?? '' }}
                                                    </span>
                                            @endif
                                        </td>
                                        <td class="text-center">{{ $objectCopyright->library?->field?->name }}</td>
                                        <td>{{ $objectCopyright->author }}</td>
                                        <td>{{ $objectCopyright->user?->full_name }}</td>
                                        <td>{{ $objectCopyright->created_at->format('H:i - d/m/Y') }}</td>
                                        <td class="text-end">
                                            <a href="#"
                                               class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm"
                                               data-kt-menu-trigger="click"
                                               data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                <i class="ki-outline ki-down fs-5 ms-1"></i>
                                            </a>
                                            <div
                                                class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                data-kt-menu="true">
                                                @can('object_copyright.edit')
                                                    <div class="menu-item px-3">
                                                        <a href="{{ route('object_copyright.edit', $objectCopyright->id) }}"
                                                           class="menu-link px-3 btn-edit-user">{{ __('Cập nhật') }}</a>
                                                    </div>
                                                @endcan
                                                @can('object_copyright.delete')
                                                    <div class="menu-item px-3">
                                                        <a href="#"
                                                           class="menu-link px-3 text-danger btn-delete-object-copyright"
                                                           data-id="{{ $objectCopyright->id }}"
                                                           data-name="{{ $objectCopyright->name }}">{{ __('Xóa') }}</a>
                                                    </div>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $objectCopyrights->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add Object Copyright-->
    @can('object_copyright.create')
        <div class="modal fade" id="kt_modal_add_ai_approve" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered mw-650px">
                <div class="modal-content">
                    <div class="modal-header" id="kt_modal_add_ai_approve_header">
                        <h2 class="fw-bold">{{ __('Thêm đối tượng bản quyền') }}</h2>
                        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal"
                             aria-label="Close">
                            <i class="ki-outline ki-cross fs-1"></i>
                        </div>
                    </div>
                    <div class="modal-body px-5">
                        <form id="kt_modal_add_ai_approve_form" class="form" action="javascript:void(0)">
                            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_ai_approve_scroll"
                                 data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto"
                                 data-kt-scroll-dependencies="#kt_modal_add_ai_approve_header"
                                 data-kt-scroll-wrappers="#kt_modal_add_ai_approve_scroll"
                                 data-kt-scroll-offset="300px">
                                <div class="col-12 mb-7">
                                    <label class="required fw-semibold fs-6 mb-2">{{ __('Lĩnh vực') }}</label>
                                    <select name="field_id" class="form-select fw-bold field_select" data-kt-select2="true" data-placeholder="{{ __('Chọn lĩnh vực') }}" data-allow-clear="true" data-hide-search="true">
                                        <option></option>
                                        @foreach($fields as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="fv-row mb-7">
                                    <!--begin::Dropzone-->
                                    <div class="dropzone dropzone-with-field disable-dropzone" id="dropzonejs_object_copyright">
                                        <div
                                            class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span
                                                    class="path1"></span><span class="path2"></span></i>
                                            <div class="ms-4">
                                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm nội dung') }}</h3>
                                                <div
                                                    class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                                                <div
                                                    class="fs-7 fw-semibold text-gray-500">{{ __('Loại tập tin cho phép: mp4, png, jpeg, jpg') }}</div>
                                                {{--                                                <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 200MB/file') }}</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Dropzone-->
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="reset" class="btn btn-primary me-3 btn-close-modal-and-reload"
                                        data-bs-dismiss="modal"
                                        data-kt-categories-modal-action="cancel">{{ __('Đóng và tải lại trang') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    <!--end::Modal - Add Object Copyright-->
@endsection

@push('js')
    <script>
        initDropzoneUploadOneFileVideo(
            '#dropzonejs_object_copyright',
            '{{ config('default.base_url_server_upload') }}',
            '{{ auth()->user()->full_name }}',
            '{{ route('api_callback.update_video') }}',
            function (response, file) {
                const file_id = response.data.video_id
                const title = file.name
                const author = response.data.author
                const url = response.data.link
                const thumb = response.data.thumbnail
                const duration = response.data.duration
                const type = file.type
                const size = file.size
                const field_id = $('.field_select').val()
                $.ajax({
                    url: '{{ route('library.storeObjectCopyright') }}',
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        file_id,
                        title,
                        author,
                        url,
                        thumb,
                        duration,
                        type,
                        size,
                        field_id
                    },
                    success: function (response) {
                        if (response.status) {
                            toastr.success("{{ __('Thêm đối tượng thành công') }}");
                            $('#kt_modal_add_ai_approve').modal('hide')
                            setTimeout(function () {
                                window.location.reload()
                            }, 1500)
                        } else {
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }
                }).always(function () {
                    KTApp.hidePageLoading()
                })
            }
        )

        $('.btn-delete-object-copyright').click(function () {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ __('Xóa đối tượng bản quyền') }}",
                html: "{{ __('Bạn có chắc muốn xóa đối tượng') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('object_copyright.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa đối tượng thành công') }}");
                                setTimeout(function () {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
