@extends('layouts.app')
@section('title', __('Gói thành viên'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('package.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-package-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="{{ __('Từ khóa') }}" />
                                </div>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('package.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_package">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm mới') }}
                                    </button>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5" id="kt_table_packages">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ __('Tên gói') }}</th>
                                    <th class="min-w-125px">{{ __('Mô tả') }}</th>
                                    <th class="min-w-125px">{{ __('Giá') }}</th>
                                    <th class="min-w-125px">{{ __('Ngày tạo') }}</th>
                                    @canany(['package.edit', 'package.delete'])
                                        <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                                    @endcanany
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($packages->total() == 0)
                                    <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                                @else
                                    @foreach($packages as $index => $package)
                                        <tr>
                                            <td class="text-center">{{ ($packages->currentPage() - 1) * $packages->perPage() + $index+1 }}</td>
                                            <td>{{ $package->name }}</td>
                                            <td>{!! $package->description !!}</td>
                                            <td>{{ number_format($package->price, decimal_separator: ',', thousands_separator: '.') }}</td>
                                            <td>{{ $package->created_at->format('H:i - d/m/Y') }}</td>
                                            @canany(['package.edit', 'package.delete'])
                                                <td class="text-end">
                                                    <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                        <i class="ki-outline ki-down fs-5 ms-1"></i>
                                                    </a>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                        @can('package.edit')
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 btn-edit-package" data-url="{{ route('package.edit', $package->id) }}">{{ __('Cập nhật') }}</a>
                                                            </div>
                                                        @endcan
                                                        @can('package.delete')
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3 text-danger btn-delete-package" data-id="{{ $package->id }}" data-name="{{ $package->name }}">{{ __('Xóa') }}</a>
                                                            </div>
                                                        @endcan
                                                    </div>
                                                </td>
                                            @endcanany
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        {{ $packages->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add package-->
    <div class="modal fade" id="kt_modal_add_package" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content">
                <div class="modal-header" id="kt_modal_add_package_header">
                    <h2 class="fw-bold">{{ __('Thêm gói thành viên') }}</h2>
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                </div>
                <div class="modal-body px-5">
                    <form id="kt_modal_add_package_form" class="form">
                        @csrf
                        <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_package_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_package_header" data-kt-scroll-wrappers="#kt_modal_add_package_scroll" data-kt-scroll-offset="300px">
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên gói') }}</label>
                                <input type="text" name="name" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-name"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                                <textarea name="description" id="kt_docs_ckeditor_classic"></textarea>
                                <span class="text-danger d-block mt-1 box-err-description"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Giá') }}</label>
                                <input type="text" name="price" class="form-control mb-3 mb-lg-0 input-format-number" />
                                <span class="text-danger d-block mt-1 box-err-price"></span>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-packages-modal-action="cancel">{{ __('Hủy') }}</button>
                            @can('package.create')
                                <button type="button" id="btn-submit-add-package" class="btn btn-primary" data-kt-packages-modal-action="submit">
                                    <span class="indicator-label">{{ __('Lưu') }}</span>
                                    <span class="indicator-progress">{{ __('Đang lưu') }}...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                </button>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal - Add package-->

    <!--begin::Modal - Update package-->
    <div class="modal fade" id="kt_modal_update_package" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script>
        let description;
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor_classic'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {
                description = editor
            })
            .catch(error => {
                console.error(error);
            });

        $('#btn-submit-add-package').click(function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let name = $('#kt_modal_add_package_form input[name="name"]').val()
            let price = $('#kt_modal_add_package_form input[name="price"]').val()
            $.ajax({
                url: '{{ route('package.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    name,
                    description: description.getData(),
                    price
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Thêm gói thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_add_package').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_add_package_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_add_package_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_package_form .box-err-name').html('')
                        $('#kt_modal_add_package_form input[name="name"]').removeClass('border-danger')
                    }
                    if (errors.price) {
                        $('#kt_modal_add_package_form .box-err-price').html(errors.price[0])
                        $('#kt_modal_add_package_form input[name="price"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_package_form .box-err-price').html('')
                        $('#kt_modal_add_package_form input[name="price"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-add-package .indicator-label').removeAttr('style')
                $('#btn-submit-add-package .indicator-progress').removeAttr('style')
                $('#btn-submit-add-package').attr('disabled', false)
            })
        })

        $('.btn-edit-package').click(function() {
            const url = $(this).data('url')
            $.ajax({
                url,
                beforeSend: KTApp.showPageLoading(),
                success: function(response) {
                    $('#kt_modal_update_package .modal-dialog').html(response)
                    $('#kt_modal_update_package').modal('show')
                }
            }).always(function() {
                KTApp.hidePageLoading()
            })
        })

        $(document).on('click', '#btn-submit-update-package', function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let url = '{{ route('package.update') }}'
            let id = $('#kt_modal_update_package_form input[name="id"]').val()
            let name = $('#kt_modal_update_package_form input[name="name"]').val()
            let price = $('#kt_modal_update_package_form input[name="price"]').val()
            $.ajax({
                url,
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    id,
                    name,
                    description: description.getData(),
                    price
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật gói thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_update_package').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_update_package_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_update_package_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_package_form .box-err-name').html('')
                        $('#kt_modal_update_package_form input[name="name"]').removeClass('border-danger')
                    }
                    if (errors.price) {
                        $('#kt_modal_update_package_form .box-err-price').html(errors.price[0])
                        $('#kt_modal_update_package_form input[name="price"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_package_form .box-err-price').html('')
                        $('#kt_modal_update_package_form input[name="price"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-update-package .indicator-label').removeAttr('style')
                $('#btn-submit-update-package .indicator-progress').removeAttr('style')
                $('#btn-submit-update-package').attr('disabled', false)
            })
        })

        $('.btn-delete-package').click(function() {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ __('Xóa gói thành viên') }}",
                html: "{{ __('Bạn có chắc muốn xóa gói') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('package.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa gói thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
