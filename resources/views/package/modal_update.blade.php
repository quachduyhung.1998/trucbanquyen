<div class="modal-content">
    <div class="modal-header" id="kt_modal_update_package_header">
        <h2 class="fw-bold">{{ __('Cập nhật gói thành viên') }}</h2>
        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
            <i class="ki-outline ki-cross fs-1"></i>
        </div>
    </div>
    <div class="modal-body px-5">
        <form id="kt_modal_update_package_form" class="form">
            @csrf
            <input type="hidden" name="id" value="{{ $package->id }}">
            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_update_package_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_package_header" data-kt-scroll-wrappers="#kt_modal_update_package_scroll" data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Tên gói') }}</label>
                    <input type="text" name="name" class="form-control mb-3 mb-lg-0" value="{{ $package->name }}" />
                    <span class="text-danger d-block mt-1 box-err-name"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                    <textarea name="description" id="kt_docs_ckeditor_classic_update">{{ $package->description }}</textarea>
                    <span class="text-danger d-block mt-1 box-err-description"></span>
                </div>
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ __('Giá') }}</label>
                    <input type="text" name="price" class="form-control mb-3 mb-lg-0 input-format-number" value="{{ $package->price }}" />
                    <span class="text-danger d-block mt-1 box-err-price"></span>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-packages-modal-action="cancel">{{ __('Hủy') }}</button>
                @can('package.edit')
                    <button type="button" id="btn-submit-update-package" class="btn btn-primary" data-kt-packages-modal-action="submit">
                        <span class="indicator-label">{{ __('Cập nhật') }}</span>
                        <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                    </button>
                @endcan
            </div>
        </form>
    </div>
</div>

<script>
    formatInputNumber()
    ClassicEditor
        .create(document.querySelector('#kt_docs_ckeditor_classic_update'), {
            toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
        })
        .then(editor => {
            description = editor
        })
        .catch(error => {
            console.error(error);
        });
</script>
