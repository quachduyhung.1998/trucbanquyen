@extends('layouts.app')
@section('title', __('Lịch sử hoạt động'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="alert alert-dismissible bg-light-warning d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
                    <i class="ki-duotone ki-information-5 fs-5tx text-warning mb-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    <div class="text-center">
                        <h1 class="fw-bold mb-5">{{ __('Tính năng đang được phát triển') }}</h1>
                        <div class="separator separator-dashed border-warning opacity-25 mb-5"></div>
                        <div class="d-flex flex-center flex-wrap">
                            <a href="{{ route('dashboard') }}" class="btn btn-warning m-2">{{ __('Đến trang tổng quan') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection
