<div class="modal-content">
    <div class="modal-header" id="kt_modal_update_category_header">
        <h2 class="fw-bold">{{ $category->type === \App\Models\Category::LINH_VUC ? __('Cập nhật lĩnh vực') : __('Cập nhật loại hình') }}</h2>
        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
            <i class="ki-outline ki-cross fs-1"></i>
        </div>
    </div>
    <div class="modal-body px-5">
        <form id="kt_modal_update_category_form" class="form" action="javascript:void(0)">
            <input type="hidden" name="id" value="{{ $category->id }}">
            <input type="hidden" name="type" value="{{ $category->type }}">
            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_update_category_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_category_header" data-kt-scroll-wrappers="#kt_modal_update_category_scroll" data-kt-scroll-offset="300px">
                <div class="fv-row mb-7">
                    <label class="required fw-semibold fs-6 mb-2">{{ \App\Models\Category::$typeLabel[$category->type] }}</label>
                    <input type="text" name="name" class="form-control mb-3 mb-lg-0" value="{{ $category->name }}" />
                    <span class="text-danger d-block mt-1 box-err-name"></span>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-categorys-modal-action="cancel">{{ __('Hủy') }}</button>
                @can('category.edit')
                    <button type="button" id="btn-submit-update-category" class="btn btn-primary" data-kt-categorys-modal-action="submit">
                        <span class="indicator-label">{{ __('Cập nhật') }}</span>
                        <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                    </button>
                @endcan
            </div>
        </form>
    </div>
</div>
