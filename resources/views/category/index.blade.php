@extends('layouts.app')
@section('title', \App\Models\Category::$typeLabel[$type])

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route(\Illuminate\Support\Facades\Route::currentRouteName()) }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-category-table-filter="search" class="form-control w-lg-250px w-100 ps-13" placeholder="{{ __('Từ khóa') }}" />
                                </div>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('category.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_category">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ $type === \App\Models\Category::LINH_VUC ? __('Thêm lĩnh vực') : __('Thêm loại hình') }}
                                    </button>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5" id="kt_table_categories">
                            <thead>
                                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                    <th class="text-center">{{ __('STT') }}</th>
                                    <th class="min-w-125px">{{ \App\Models\Category::$typeLabel[$type] }}</th>
                                    <th class="min-w-125px">{{ __('Ngày tạo') }}</th>
                                    <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                                </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                                @if ($categories->total() == 0)
                                    <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                                @else
                                    @foreach($categories as $index => $category)
                                        <tr>
                                            <td class="text-center">{{ ($categories->currentPage() - 1) * $categories->perPage() + $index+1 }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->created_at->format('H:i - d/m/Y') }}</td>
                                            <td class="text-end">
                                                <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                    <i class="ki-outline ki-down fs-5 ms-1"></i>
                                                </a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                    @can('category.edit')
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3 btn-edit-category" data-url="{{ route('category.edit', $category->id) }}">{{ __('Cập nhật') }}</a>
                                                        </div>
                                                    @endcan
                                                    @can('category.delete')
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3 text-danger btn-delete-category" data-id="{{ $category->id }}" data-name="{{ $category->name }}">{{ __('Xóa') }}</a>
                                                        </div>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        {{ $categories->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add category-->
    <div class="modal fade" id="kt_modal_add_category" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content">
                <div class="modal-header" id="kt_modal_add_category_header">
                    <h2 class="fw-bold">{{ $type === \App\Models\Category::LINH_VUC ? __('Thêm lĩnh vực') : __('Thêm loại hình') }}</h2>
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                </div>
                <div class="modal-body px-5">
                    <form id="kt_modal_add_category_form" class="form" action="javascript:void(0)">
                        <input type="hidden" name="type" value="{{ $type }}">
                        <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_category_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_category_header" data-kt-scroll-wrappers="#kt_modal_add_category_scroll" data-kt-scroll-offset="300px">
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ \App\Models\Category::$typeLabel[$type] }}</label>
                                <input type="text" name="name" class="form-control mb-3 mb-lg-0" />
                                <span class="text-danger d-block mt-1 box-err-name"></span>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-categories-modal-action="cancel">{{ __('Hủy') }}</button>
                            @can('category.create')
                                <button type="button" id="btn-submit-add-category" class="btn btn-primary" data-kt-categories-modal-action="submit">
                                    <span class="indicator-label">{{ __('Lưu') }}</span>
                                    <span class="indicator-progress">{{ __('Đang lưu') }}...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                </button>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal - Add category-->

    <!--begin::Modal - Update category-->
    <div class="modal fade" id="kt_modal_update_category" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('#btn-submit-add-category').click(function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let name = $('#kt_modal_add_category_form input[name="name"]').val()
            let type = $('#kt_modal_add_category_form input[name="type"]').val()
            $.ajax({
                url: '{{ route('category.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    name,
                    type
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ $type === \App\Models\Category::LINH_VUC ? __('Thêm lĩnh vực thành công') : __('Thêm loại hình thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_add_category').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_add_category_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_add_category_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_add_category_form .box-err-name').html('')
                        $('#kt_modal_add_category_form input[name="name"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-add-category .indicator-label').removeAttr('style')
                $('#btn-submit-add-category .indicator-progress').removeAttr('style')
                $('#btn-submit-add-category').attr('disabled', false)
            })
        })

        $('.btn-edit-category').click(function() {
            const url = $(this).data('url')
            $.ajax({
                url,
                beforeSend: KTApp.showPageLoading(),
                success: function(response) {
                    $('#kt_modal_update_category .modal-dialog').html(response)
                    $('#kt_modal_update_category').modal('show')
                }
            }).always(function() {
                KTApp.hidePageLoading()
            })
        })

        $(document).on('click', '#btn-submit-update-category', function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let url = '{{ route('category.update') }}'
            let id = $('#kt_modal_update_category_form input[name="id"]').val()
            let name = $('#kt_modal_update_category_form input[name="name"]').val()
            let type = $('#kt_modal_update_category_form input[name="type"]').val()
            $.ajax({
                url,
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    id,
                    name,
                    type
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ $type === \App\Models\Category::LINH_VUC ? __('Cập nhật lĩnh vực thành công') : __('Cập nhật loại hình thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_update_category').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.name) {
                        $('#kt_modal_update_category_form .box-err-name').html(errors.name[0])
                        $('#kt_modal_update_category_form input[name="name"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_category_form .box-err-name').html('')
                        $('#kt_modal_update_category_form input[name="name"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-update-category .indicator-label').removeAttr('style')
                $('#btn-submit-update-category .indicator-progress').removeAttr('style')
                $('#btn-submit-update-category').attr('disabled', false)
            })
        })

        $('.btn-delete-category').click(function() {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ $type === \App\Models\Category::LINH_VUC ? __('Xóa lĩnh vực') : __('Xóa loại hình') }}",
                html: "{{ $type === \App\Models\Category::LINH_VUC ? __('Bạn có chắc muốn xóa lĩnh vực') : __('Bạn có chắc muốn xóa loại hình') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('category.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ $type === \App\Models\Category::LINH_VUC ? __('Xóa lĩnh vực thành công') : __('Xóa loại hình thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
