@extends('layouts.app')
@section('title', __('Cập nhật bản quyền'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('ai_approve.update', $aiApprove->id) }}" method="POST">
                            @csrf
                            @include('ai_approve.form')

                            <div class="separator mb-7"></div>

                            @can('ai_approve.edit')
                                @if($aiApprove->status !== \App\Models\AiApprove::STATUS_APPROVAL)
                                    <button type="button" class="btn btn-success me-3 btn-change-status" data-status="{{ \App\Models\AiApprove::STATUS_APPROVAL }}">
                                        {{ __('Duyệt') }}
                                    </button>
                                @endif
                                @if($aiApprove->status !== \App\Models\AiApprove::STATUS_NOT_APPROVAL)
                                    <button type="button" class="btn btn-danger me-3 btn-change-status" data-status="{{ \App\Models\AiApprove::STATUS_NOT_APPROVAL }}">
                                        {{ __('Vi phạm') }}
                                    </button>
                                @endif
                            @endcan
                            <a href="{{ route('ai_approve.index') }}" class="btn btn-light">{{ __('Trở lại') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdn.plyr.io/3.7.8/plyr.css" />
@endpush

@push('js')
    <script src="https://cdn.plyr.io/3.7.8/plyr.js"></script>
    <script>
        const player = new Plyr('#player');

        $('.btn-change-status').click(function() {
            const id = '{{ $aiApprove->id }}'
            const name = '{{ $aiApprove->name }}'
            const status = $(this).data('status')
            let icon = 'success'
            let color = 'success'
            let title = `<b class="text-${color}">{{ __('Duyệt') }}</b>`
            let html = `{{ __('Bạn có chắc muốn duyệt bản quyền cho') }} <b>${name}</b>?`
            let confirmButtonText = "{{ __('Duyệt') }}"
            if (status == '{{ \App\Models\AiApprove::STATUS_NOT_APPROVAL }}') {
                icon = 'error'
                color = 'danger'
                title = `<b class="text-${color}">{{ __('Xác nhận nội dung vi phạm') }}</b>`
                html = `{{ __('Bạn có chắc chắn muốn cập nhật trạng thái cho') }} <b>${name}</b> sang <span class="text-${color}">"vi phạm"</span> không?`
                confirmButtonText = "{{ __('Xác nhận') }}"
            }
            Swal.fire({
                title,
                html,
                icon,
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText,
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: `btn btn-${color}`,
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('ai_approve.change_status') }}',
                        type: 'PUT',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id,
                            status
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Cập nhật trạng thái thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })

        $('.btn-select-time').click(function() {
            $('#preview_frame_image').attr('src', $(this).data('frame_image'))
            $('#preview_image_similar').attr('src', $(this).data('image_similar'))
            $('#preview_similarity').html($(this).data('similarity') + '%')
            $('.badge.badge-secondary.bg-hover-danger.bg-danger').removeClass('bg-danger text-white')
            $(this).addClass('bg-danger text-white')
            player.currentTime = $(this).data('time');
        })
    </script>
@endpush
