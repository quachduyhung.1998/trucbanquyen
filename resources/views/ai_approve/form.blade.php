@php
    $type = \App\Models\Library::TYPE_IMAGE;
    $name = '';
    $thumb = '';
    $link = '';
    if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT) {
        $name = $aiApprove->objectCopyright?->name ?? '';
        if ($aiApprove->objectCopyright?->library?->type === \App\Models\Library::TYPE_IMAGE) {
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->objectCopyright?->library?->url);
        }  else {
            $type = \App\Models\Library::TYPE_VIDEO;
            $thumb = $aiApprove->objectCopyright?->library?->thumb ? \App\Helpers\FileHelper::generateLink($aiApprove->objectCopyright?->library?->thumb) : asset('assets/media/logos/ttbqs-logo.svg');
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->objectCopyright?->library?->url);
        }
    } elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE) {
        $name = $aiApprove->objectFace?->name ?? '';
        if ($aiApprove->objectFace?->library?->type === \App\Models\Library::TYPE_IMAGE) {
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->objectFace?->library?->url);
        }  else {
            $type = \App\Models\Library::TYPE_VIDEO;
            $thumb = $aiApprove->objectFace?->library?->thumb ? \App\Helpers\FileHelper::generateLink($aiApprove->objectFace?->library?->thumb) : asset('assets/media/logos/ttbqs-logo.svg');
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->objectFace?->library?->url);
        }
    } elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY) {
        $name = $aiApprove->library?->title ?? '';
        if ($aiApprove->library?->type === \App\Models\Library::TYPE_IMAGE) {
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->library?->url);
        }  else {
            $type = \App\Models\Library::TYPE_VIDEO;
            $thumb = $aiApprove->library?->thumb ? \App\Helpers\FileHelper::generateLink($aiApprove->library?->thumb) : asset('assets/media/logos/ttbqs-logo.svg');
            $link = \App\Helpers\FileHelper::generateLink($aiApprove->library?->url);
        }
    }
@endphp

<div class="row">
    <div class="col-12 mb-5"><h3>@yield('title'): {{ $name }}</h3></div>

    <div class="col-md-6 col-12 mb-7">
        @if($type === \App\Models\Library::TYPE_IMAGE)
            <img src="{{ $link }}" alt="{{ $name }}" class="w-100" style="max-height: 500px">
        @else
            <video id="player" class="w-100" style="max-height: 500px" playsinline controls data-poster="{{ $thumb }}">
                <source src="{{ $link }}" type="video/mp4" />
            </video>
        @endif

        <div class="mt-5 fw-bold">
            <span>{{ __('AI kiểm tra') }}:</span>
            <span class="badge badge-lg badge-{{ \App\Models\AiApprove::$statusAiDetectColor[$aiApprove->status_ai_detect] }}">{{ \App\Models\AiApprove::$statusAiDetectLabel[$aiApprove->status_ai_detect] }}</span>
        </div>
        <div class="mt-2 fw-bold">
            <span>{{ __('Trạng thái duyệt') }}:</span>
            <span class="badge badge-lg badge-{{ \App\Models\AiApprove::$statusColor[$aiApprove->status] }}">{{ \App\Models\AiApprove::$statusLabel[$aiApprove->status] }}</span>
        </div>
    </div>

    <div class="col-md-6 col-12 mb-7">
        @if($aiApprove->objectCopyright?->library?->data_similar === null || (is_array($aiApprove->objectCopyright->library->data_similar) && $aiApprove->objectCopyright->library->data_similar['similary'] < config('default.violation_threshold', 0.85)))
            @if($aiApprove->status_ai_detect === \App\Models\AiApprove::STATUS_AI_DETECT_PENDING)
                <div class="h-500px text-center bg-light-secondary rounded p-4">
                    <span class="fs-2x">{{ __('Chưa có dữ liệu phân tích') }}</span>
                </div>
            @else
                <div class="h-500px text-center bg-light-primary rounded p-4">
                    <span class="fs-2x">{{ __('Không có dấu hiệu vi phạm') }}</span>
                </div>
            @endif
        @elseif ($aiApprove->objectCopyright->library->data_similar && is_array($aiApprove->objectCopyright->library->data_similar))
            <div class="min-h-500px bg-light-danger rounded p-4">
                <span class="badge badge-danger fs-4 mb-5">{{ __('Chứa khung hình vi phạm') }}</span> ({{ __('Độ tương đồng') }}: {{ number_format(($aiApprove->objectCopyright->library->data_similar['similary'] ?? 0) * 100, 2) }}%)
                @isset($aiApprove->objectCopyright->library->data_similar['frames'][0])
                    <div class="d-flex flex-md-row flex-column gap-5 mb-2">
                        <div>
                            <div class="fw-bold fs-3 mb-2">{{ __('Khung hình trùng khớp') }}</div>
                            <img id="preview_frame_image" src="{{ \App\Helpers\FileHelper::generateLink($aiApprove->objectCopyright->library->data_similar['frames'][0]['frame_image']) }}" alt="" class="w-300px">
                        </div>
                        <div>
                            <div class="fw-bold fs-3 mb-2">{{ __('Khung hình tìm thấy') }}</div>
                            <img id="preview_image_similar" src="{{ \App\Helpers\FileHelper::generateLink($aiApprove->objectCopyright->library->data_similar['frames'][0]['image_similar']) }}" alt="" class="w-300px">
                        </div>
                    </div>
                    <div class="mb-5">{{ __('Độ tương đồng của 2 khung hình') }}: <span id="preview_similarity">{{ number_format(($aiApprove->objectCopyright->library->data_similar['frames'][0]['similarity'] ?? 0) * 100, 2) }}%</span></div>
                @endisset
                <div class="fw-bold fs-3 mb-2">{{ __('Các khung hình có dấu hiệu vi phạm') }}</div>
                <div class="d-flex align-items-center gap-3 flex-wrap">
                    @if (!empty($aiApprove->objectCopyright?->library?->data_similar['frames']))
                        @foreach($aiApprove->objectCopyright?->library?->data_similar['frames'] as $index => $item)
                            @if(isset($item['t']) && isset($item['similarity']) && $item['similarity'] >= config('default.violation_threshold', 0.85))
                                <span class="btn-select-time d-block w-65px badge badge-secondary cursor-pointer bg-hover-danger text-hover-white {{ $index === 0 ? 'bg-danger text-white' : '' }}" data-time="{{ $item['t'] ?? 0 }}" data-frame_image="{{ \App\Helpers\FileHelper::generateLink($item['frame_image']) }}" data-image_similar="{{ \App\Helpers\FileHelper::generateLink($item['image_similar']) }}" data-similarity="{{ number_format($item['similarity'] * 100, 2) }}" style="transition: none">
                                    {{ gmdate('H:i:s', $item['t'] ?? 0) }}
                                </span>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        @endif
    </div>
</div>
