@extends('layouts.app')
@section('title', __('AI duyệt bản quyền'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}"
                                   class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('ai_approve.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}"
                                           data-kt-user-table-filter="search"
                                           class="form-control w-lg-350px w-100 ps-13" placeholder="Từ khóa"/>
                                </div>
{{--                                <select name="type" class="form-select fw-bold w-lg-125px w-100 my-1"--}}
{{--                                        data-kt-select2="true" data-placeholder="{{ __('Loại') }}"--}}
{{--                                        data-allow-clear="true" data-hide-search="true">--}}
{{--                                    <option></option>--}}
{{--                                    @foreach(\App\Models\Library::$typeLabel as $key => $value)--}}
{{--                                        <option--}}
{{--                                            value="{{ $key }}" {{ request('type') == $key ? 'selected' : '' }}>{{ $value }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
                                <select name="field_id" class="form-select fw-bold w-lg-250px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Chọn lĩnh vực') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach($fields as $key => $value)
                                        <option value="{{ $key }}" {{ request('field_id') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="status_ai_detect" class="form-select fw-bold w-lg-250px w-100 my-1"
                                        data-kt-select2="true" data-placeholder="{{ __('AI kiểm tra') }}"
                                        data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\AiApprove::$statusAiDetectLabel as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ request('status_ai_detect') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="status" class="form-select fw-bold w-lg-175px w-100 my-1"
                                        data-kt-select2="true" data-placeholder="{{ __('Trạng thái') }}"
                                        data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\AiApprove::$statusLabel as $key => $value)
                                        <option
                                            value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit"
                                        class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('ai_approve.create')
                            {{--                            <div class="card-toolbar">--}}
                            {{--                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">--}}
                            {{--                                    <a href="" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_ai_approve">--}}
                            {{--                                        <i class="ki-outline ki-file-up fs-2"></i>{{ __('Tải lên') }}--}}
                            {{--                                    </a>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="text-center">{{ __('STT') }}</th>
                                <th class="min-w-125px">{{ __('Ảnh') }}</th>
                                <th class="min-w-125px">{{ __('Tên') }}</th>
{{--                                <th class="min-w-125px text-center">{{ __('Loại') }}</th>--}}
                                <th class="min-w-125px text-center">{{ __('Lĩnh vực') }}</th>
                                <th class="min-w-125px text-center">{{ __('AI kiểm tra') }}</th>
                                <th class="min-w-125px text-center">{{ __('Trạng thái') }}</th>
                                <th class="min-w-125px">{{ __('Thời gian tạo') }}</th>
                                <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @if ($aiApproves->total() == 0)
                                <tr>
                                    <td colspan="8">{{ __('Không có dữ liệu') }}</td>
                                </tr>
                            @else
                                @foreach($aiApproves as $index => $aiApprove)
                                    <tr>
                                        <td class="text-center">{{ ($aiApproves->currentPage() - 1) * $aiApproves->perPage() + $index+1 }}</td>
                                        <td class="d-flex align-items-center">
                                            <a href="{{ route('ai_approve.edit', $aiApprove->id) }}">
                                                @if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                                                    <img src="{{ \App\Helpers\FileHelper::generateLink(($aiApprove->objectCopyright?->library?->type === \App\Models\Library::TYPE_IMAGE) ? $aiApprove->objectCopyright?->library?->url : $aiApprove->objectCopyright?->library?->thumb) }}"
                                                         alt="{{ $aiApprove->objectCopyright?->name }}"
                                                         class="w-100px rounded">
                                                @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE)
                                                    <img src="{{ \App\Helpers\FileHelper::generateLink(($aiApprove->objectFace?->library?->type === \App\Models\Library::TYPE_IMAGE) ? $aiApprove->objectFace?->library?->url : $aiApprove->objectFace?->library?->thumb) }}"
                                                         alt="{{ $aiApprove->objectFace?->name }}"
                                                         class="w-100px rounded">
                                                @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY)
                                                    <img src="{{ \App\Helpers\FileHelper::generateLink(($aiApprove->library?->type === \App\Models\Library::TYPE_IMAGE) ? $aiApprove->library?->url : $aiApprove->library?->thumb) }}"
                                                         alt="{{ $aiApprove->name }}"
                                                         class="w-100px rounded">
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('ai_approve.edit', $aiApprove->id) }}" class="text-gray-800 text-hover-primary">
                                                @if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                                                    {{ $aiApprove->objectCopyright?->name }}
                                                @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE)
                                                    {{ $aiApprove->objectFace?->name }}
                                                @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY)
                                                    {{ $aiApprove->library?->title }}
                                                @endif
                                            </a>
                                        </td>
{{--                                        <td class="text-center">--}}
{{--                                            @if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT && $aiApprove->objectCopyright?->library?->type)--}}
{{--                                                <span class="badge badge-{{ $aiApprove->objectCopyright?->library?->type === \App\Models\Library::TYPE_IMAGE ? 'success' : 'danger' }}">--}}
{{--                                                    {{ \App\Models\Library::$typeLabel[$aiApprove->objectCopyright?->library?->type] ?? '' }}--}}
{{--                                                </span>--}}
{{--                                            @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE && $aiApprove->objectFace?->library?->type)--}}
{{--                                                <span class="badge badge-{{ $aiApprove->objectFace?->library?->type === \App\Models\Library::TYPE_IMAGE ? 'success' : 'danger' }}">--}}
{{--                                                    {{ \App\Models\Library::$typeLabel[$aiApprove->objectFace?->library?->type] ?? '' }}--}}
{{--                                                </span>--}}
{{--                                            @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY && $aiApprove->library)--}}
{{--                                                <span class="badge badge-{{ $aiApprove->library->type === \App\Models\Library::TYPE_IMAGE ? 'success' : 'danger' }}">--}}
{{--                                                    {{ \App\Models\Library::$typeLabel[$aiApprove->library->type] ?? '' }}--}}
{{--                                                </span>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
                                        <td class="text-center">
                                            @if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                                                {{ $aiApprove->objectCopyright->library?->field?->name }}
                                            @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE)
                                                {{ $aiApprove->objectFace->library?->field?->name }}
                                            @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY)
                                                {{ $aiApprove->library?->field?->name }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($aiApprove->status_ai_detect)
                                                <span
                                                    class="badge badge-{{ \App\Models\AiApprove::$statusAiDetectColor[$aiApprove->status_ai_detect] ?? '' }}">
                                                        {{ \App\Models\AiApprove::$statusAiDetectLabel[$aiApprove->status_ai_detect] ?? '' }}
                                                    </span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($aiApprove->status)
                                                <span
                                                    class="badge badge-{{ \App\Models\AiApprove::$statusColor[$aiApprove->status] ?? '' }}">
                                                        {{ \App\Models\AiApprove::$statusLabel[$aiApprove->status] ?? '' }}
                                                    </span>
                                            @endif
                                        </td>
                                        <td>{{ $aiApprove->created_at->format('H:i - d/m/Y') }}</td>
                                        <td class="text-end" style="min-width: 115px">
                                            <a href="#"
                                               class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm"
                                               data-kt-menu-trigger="click"
                                               data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                <i class="ki-outline ki-down fs-5 ms-1"></i>
                                            </a>
                                            <div
                                                class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                data-kt-menu="true">
                                                @can('ai_approve.edit')
                                                    @if($aiApprove->status_ai_detect === \App\Models\AiApprove::STATUS_AI_DETECT_PENDING)
                                                        @if($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                                                            <div class="menu-item px-3">
                                                                <a href="javascript:void(0)" class="menu-link px-3 btn-request-ai-detect" data-file_id="{{ $aiApprove->objectCopyright?->library?->file_id }}">{{ __('AI kiểm tra') }}</a>
                                                            </div>
                                                        @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_OBJECT_FACE)
                                                            <div class="menu-item px-3">
                                                                <a href="javascript:void(0)" class="menu-link px-3 btn-request-ai-detect" data-file_id="{{ $aiApprove->objectFace?->library?->file_id }}">{{ __('AI kiểm tra') }}</a>
                                                            </div>
                                                        @elseif($aiApprove->model_type === \App\Models\AiApprove::MODEL_TYPE_LIBRARY)
                                                            <div class="menu-item px-3">
                                                                <a href="javascript:void(0)" class="menu-link px-3 btn-request-ai-detect" data-file_id="{{ $aiApprove->library?->file_id }}">{{ __('AI kiểm tra') }}</a>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endcan
                                                @can('ai_approve.edit')
                                                    <div class="menu-item px-3">
                                                        <a href="{{ route('ai_approve.edit', $aiApprove->id) }}"
                                                           class="menu-link px-3 btn-edit-user">{{ __('Cập nhật') }}</a>
                                                    </div>
                                                @endcan
                                                @can('ai_approve.delete')
                                                    <div class="menu-item px-3">
                                                        <a href="#"
                                                           class="menu-link px-3 text-danger btn-delete-ai-approve"
                                                           data-id="{{ $aiApprove->id }}"
                                                           data-name="{{ $aiApprove->name }}">{{ __('Xóa') }}</a>
                                                    </div>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $aiApproves->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>

    <!--begin::Modal - Add AI Approve-->
    @can('ai_approve.create')
        <div class="modal fade" id="kt_modal_add_ai_approve" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered mw-650px">
                <div class="modal-content">
                    <div class="modal-header" id="kt_modal_add_ai_approve_header">
                        <h2 class="fw-bold">{{ __('Thêm nội dung bản quyền') }}</h2>
                        <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal"
                             aria-label="Close">
                            <i class="ki-outline ki-cross fs-1"></i>
                        </div>
                    </div>
                    <div class="modal-body px-5">
                        <form id="kt_modal_add_ai_approve_form" class="form" action="javascript:void(0)">
                            <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_add_ai_approve_scroll"
                                 data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto"
                                 data-kt-scroll-dependencies="#kt_modal_add_ai_approve_header"
                                 data-kt-scroll-wrappers="#kt_modal_add_ai_approve_scroll"
                                 data-kt-scroll-offset="300px">
                                <div class="fv-row mb-7">
                                    <!--begin::Dropzone-->
                                    <div class="dropzone" id="dropzonejs_images">
                                        <div
                                            class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span
                                                    class="path1"></span><span class="path2"></span></i>
                                            <div class="ms-4">
                                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                                                <div
                                                    class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                                                <div
                                                    class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                                                <div
                                                    class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 20MB/file') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Dropzone-->
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="reset" class="btn btn-primary me-3 btn-close-modal-and-reload"
                                        data-bs-dismiss="modal"
                                        data-kt-categories-modal-action="cancel">{{ __('Đóng và tải lại trang') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    <!--end::Modal - Add AI Approve-->
@endsection

@push('js')
    <script>
        initDropzoneUploadMultiFile('#dropzonejs_images', function (url, file) {
            $.ajax({
                url: '{{ route('ai_approve.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    name: file.name,
                    url,
                    type: file.type,
                },
                success: function (response) {
                    if (response.status) {
                        $('table tbody tr:first-child').before(`
                            <tr>
                                <td class="text-center">1</td>
                                <td class="d-flex align-items-center">
                                    <a href="/ai-approves/${response.objectFace.id}/edit">
                                        <img src="${url}" alt="${response.objectFace.name}" class="w-125px rounded">
                                    </a>
                                </td>
                                <td>${response.objectFace.name}</td>
                                <td>
                                    <div class="badge badge-success fw-bold">
                                        ${response.objectFace.type === 'IMAGE' ? 'Ảnh' : 'Video'}
                                    </div>
                                </td>
                                <td>
                                    <div class="badge badge-secondary fw-bold">
                                        Chờ xử lý
                                    </div>
                                </td>
                                <td>
                                    <div class="badge badge-secondary fw-bold">
                                        Chờ xử lý
                                    </div>
                                </td>
                                <td>{{ date('H:i - d/m/Y') }}</td>
                                <td class="text-end">
                                    <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Thao tác
                                        <i class="ki-outline ki-down fs-5 ms-1"></i>
                                    </a>
                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                        <div class="menu-item px-3">
                                            <a href="/ai-approves/${response.objectFace.id}/edit" class="menu-link px-3 btn-edit-user">Cập nhật</a>
                                        </div>
                                        <div class="menu-item px-3">
                                            <a href="#" class="menu-link px-3 text-danger btn-delete-ai-approve" data-id="${response.objectFace.id}" data-name="${response.objectFace.name}">Xóa</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        `)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                }
            })
        }, 20, 100, '.jpeg, .jpg, .png, .mp4')

        $('.btn-delete-ai-approve').click(function () {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ __('Xóa bản quyền') }}",
                html: "{{ __('Bạn có chắc muốn xóa bản quyền') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('ai_approve.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa bản quyền thành công') }}");
                                setTimeout(function () {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })

        $('.btn-request-ai-detect').click(function () {
            const file_id = $(this).data('file_id')
            Swal.fire({
                title: "{{ __('Ai kiểm tra') }}",
                html: "{{ __('Yêu cầu AI kiểm tra nội dung') }}",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Kiểm tra ngay') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('ai_approve.request_ai_detect') }}',
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            file_id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Cập nhật bản quyền thành công') }}");
                                setTimeout(function () {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.warning("{{ __('Chưa có dữ liệu do AI đang trong tiến trình xử lý') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
