<div class="row">
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

    @isset($objectFace)
        <div class="col-12 mb-5"><h3>{{ __('Cập nhật đối tượng khuôn mặt') }}: {{ $objectFace->name }}</h3></div>
    @endisset

    <div class="col-md-6 col-12 mb-7">
        <label class="fw-semibold fs-6 mb-2">{{ __('Hình ảnh') }}</label>
        <!--begin::Dropzone-->
        <div class="dropzone" id="dropzonejs_images">
            <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                <div class="ms-4">
                    <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Thêm hình ảnh') }}</h3>
                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Loại ảnh cho phép: png, jpeg, jpg') }}</div>
                    <div class="fs-7 fw-semibold text-gray-500">{{ __('Dung lượng tối đa: 2MB') }}</div>
                </div>
            </div>
        </div>
        <!--end::Dropzone-->

        <div class="d-flex flex-wrap align-items-center gap-2 mt-5 list-image">
            @if(old('images'))
                @foreach(old('images') as $image)
                    <div class="image-item">
                        <div class="overlay-layer"></div>
                        <div class="d-flex align-items-center gap-1 group-btn-image">
                            <a href="{{ asset($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                            <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                <span class="path3"></span>
                                <span class="path4"></span>
                            </i>
                        </div>
                        <input type="hidden" name="images[]" value="{{ $image }}">
                        <img src="{{ $image }}" alt="{{ $objectFace->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
                    </div>
                @endforeach
            @elseif(isset($objectFace) && $objectFace->images)
                @foreach($objectFace->images as $image)
                    <div class="image-item">
                        <div class="overlay-layer"></div>
                        <div class="d-flex align-items-center gap-1 group-btn-image">
                            <a href="{{ asset($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                            <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                <span class="path3"></span>
                                <span class="path4"></span>
                            </i>
                        </div>
                        <input type="hidden" name="images[]" value="{{ $image }}">
                        <img src="{{ $image }}" alt="{{ $objectFace->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <div class="col-md-6 col-12 mb-7">
        <div class="row">
            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên nhân vật') }}</label>
                <input type="text" name="name" class="form-control mb-3 mb-lg-0 @error('name') border-danger @enderror" value="{{ old('name', isset($objectFace) ? $objectFace->name : '') }}" />
                @error('name')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Chức vụ') }}</label>
                <input type="text" name="position" class="form-control mb-3 mb-lg-0 @error('position') border-danger @enderror" value="{{ old('position', isset($objectFace) ? $objectFace->position : '') }}" />
                @error('position')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Thẻ cảnh báo') }}</label>
                <select name="tag_alerts[]" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn thẻ cảnh báo') }}" data-allow-clear="true" data-hide-search="true" data-control="select2" multiple="multiple">
                    <option></option>
                    @foreach(\App\Models\ObjectFace::$tagAlerts as $key => $value)
                        <option value="{{ $key }}" {{ old('tag_alerts') ? (is_array(old('tag_alerts')) && in_array($key, old('tag_alerts')) ? 'selected' : '') : ((isset($objectFace) && is_array($objectFace->tag_alerts) && in_array($key, $objectFace->tag_alerts)) ? 'selected' : '') }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Ghi chú') }}</label>
                <textarea name="note" rows="3" class="form-control mb-3 mb-lg-0">{{ old('note', isset($objectFace) ? $objectFace->note : '') }}</textarea>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        initDropzoneUploadMultiFile('#dropzonejs_images', function (url, file) {
            let html = `<div class="image-item">
                <div class="overlay-layer"></div>
                <div class="d-flex align-items-center gap-1 group-btn-image">
                    <a href="${url}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem ảnh') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </div>
                <input type="hidden" name="images[]" value="${url}">
                <img src="${url}" alt="{{ $objectFace->name ?? '' }}" class="w-md-100px w-90px border rounded shadow">
            </div>`
            $('.list-image').append(html)
        })

        $(document).on('click', '.btn-remove-image', function () {
            Swal.fire({
                title: "{{ __('Bạn có chắc muốn xóa ảnh này không?') }}",
                icon: "question",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $(this).closest('.image-item').remove()
                }
            })

        })
    </script>
@endpush
