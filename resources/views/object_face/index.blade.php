@extends('layouts.app')
@section('title', __('Đối tượng khuôn mặt'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('object_face.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user-table-filter="search" class="form-control w-lg-350px w-100 ps-13" placeholder="Từ khóa" />
                                </div>
                                <select name="tag_alerts[]" class="form-select fw-bold w-lg-300px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Thẻ cảnh báo') }}" data-allow-clear="true" data-hide-search="true" data-control="select2" multiple="multiple">
                                    <option></option>
                                    @foreach(\App\Models\ObjectFace::$tagAlerts as $key => $value)
                                        <option value="{{ $key }}" {{ (request('tag_alerts') && is_array(request('tag_alerts')) && in_array($key, request('tag_alerts'))) ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('object_face.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a href="{{ route('object_face.create') }}" class="btn btn-primary">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm mới') }}
                                    </a>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="text-center">{{ __('STT') }}</th>
                                <th class="min-w-125px">{{ __('Ảnh') }}</th>
                                <th class="min-w-125px">{{ __('Tên') }}</th>
                                <th class="min-w-125px">{{ __('Chức vụ') }}</th>
                                <th class="min-w-125px">{{ __('Thẻ cảnh bảo') }}</th>
                                <th class="min-w-125px">{{ __('Thời gian tạo') }}</th>
                                <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @if ($objectFaces->total() == 0)
                                <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                            @else
                                @foreach($objectFaces as $index => $objectFace)
                                    <tr>
                                        <td class="text-center">{{ ($objectFaces->currentPage() - 1) * $objectFaces->perPage() + $index+1 }}</td>
                                        <td class="d-flex align-items-center">
                                            <a href="{{ route('object_face.edit', $objectFace->id) }}">
                                                <img src="{{ asset(($objectFace->images && count($objectFace->images) > 0) ? $objectFace->images[0] : '') }}" alt="{{ $objectFace->name }}" class="w-100px rounded">
                                            </a>
                                        </td>
                                        <td>{{ $objectFace->name }}</td>
                                        <td>{{ $objectFace->position }}</td>
                                        <td>
                                            @if ($objectFace->tag_alerts)
                                                @foreach($objectFace->tag_alerts as $value)
                                                    <div class="badge badge-{{ \App\Models\ObjectFace::$tagAlertColors[$value] }}">
                                                        {{ \App\Models\ObjectFace::$tagAlerts[$value] }}
                                                    </div>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{ $objectFace->created_at->format('H:i - d/m/Y') }}</td>
                                        <td class="text-end">
                                            <a href="#" class="btn btn-light btn-active-light-primary btn-flex btn-center btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{ __('Thao tác') }}
                                                <i class="ki-outline ki-down fs-5 ms-1"></i>
                                            </a>
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">
                                                @can('object_face.edit')
                                                    <div class="menu-item px-3">
                                                        <a href="{{ route('object_face.edit', $objectFace->id) }}" class="menu-link px-3 btn-edit-user">{{ __('Cập nhật') }}</a>
                                                    </div>
                                                @endcan
                                                @can('object_face.delete')
                                                    <div class="menu-item px-3">
                                                        <a href="#" class="menu-link px-3 text-danger btn-delete-object-copyright" data-id="{{ $objectFace->id }}" data-name="{{ $objectFace->name }}">{{ __('Xóa') }}</a>
                                                    </div>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $objectFaces->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        $('.btn-delete-object-copyright').click(function() {
            const id = $(this).data('id')
            const name = $(this).data('name')
            Swal.fire({
                title: "{{ __('Xóa đối tượng khuôn mặt') }}",
                html: "{{ __('Bạn có chắc muốn xóa đối tượng') }} <b>" + name + '</b>?',
                icon: "warning",
                buttonsStyling: false,
                showCancelButton: true,
                confirmButtonText: "{{ __('Xóa') }}",
                cancelButtonText: "{{ __("Hủy") }}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-light-secondary text-black-50"
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '{{ route('object_face.delete') }}',
                        type: 'DELETE',
                        data: {
                            _token: '{{ csrf_token() }}',
                            id
                        },
                        beforeSend: KTApp.showPageLoading(),
                        success: function (response) {
                            if (response.status) {
                                toastr.success("{{ __('Xóa đối tượng thành công') }}");
                                setTimeout(function() {
                                    window.location.reload()
                                }, 1500)
                            } else {
                                toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                            }
                        }
                    }).always(function () {
                        KTApp.hidePageLoading()
                    })
                }
            })
        })
    </script>
@endpush
