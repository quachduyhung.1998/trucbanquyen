<!DOCTYPE html>
<html>
<head>
    <title>{{ request('name', 'Web Viewer') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
</head>

<!-- Import WebViewer as a script tag -->
<script src="{{ asset('WebViewer/lib/webviewer.min.js') }}"></script>

<body style="height: 100vh; margin: 0;">
    <div id='viewer' style='width: 100%; height: 100%; margin: 0 auto;'></div>

    <script>
        WebViewer({
            path: 'WebViewer/lib', // path to the PDFTron 'lib' folder on your server
            licenseKey: 'Insert commercial license key here after purchase',
            initialDoc: '{{ request()->get('file') ?? '' }}',
            config: '{{ asset('WebViewer/config.js?v=1.0') }}'
        }, document.getElementById('viewer'))
        .then(instance => {
            const { documentViewer, annotationManager } = instance.Core;

            // call methods from instance, documentViewer and annotationManager as needed

            // you can also access major namespaces from the instance as follows:
            // const Tools = instance.Core.Tools;
            // const Annotations = instance.Core.Annotations;

            // documentViewer.isReadOnly = true
            documentViewer.addEventListener('documentLoaded', () => {
                // call methods relating to the loaded document
            });
        });
    </script>
</body>
</html>
