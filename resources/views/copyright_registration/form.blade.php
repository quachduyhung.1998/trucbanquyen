<div class="row">
    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">

    @isset($copyrightRegistration)
        <div class="col-12 mb-5">
            <div class="d-flex flex-md-row flex-column align-items-md-center justify-content-between">
                <h3 class="mb-md-0 mb-3">{{ __('Thông tin sản phẩm') }}</h3>
                <div class="d-flex flex-md-row flex-column align-items-md-center gap-md-5 gap-3">
                    <span class="fs-5">{{ __('Người tạo') }}: <b>{{ $copyrightRegistration->user?->full_name }}</b></span>
                    <span class="fs-5">{{ __('Trạng thái') }}: <span class="badge badge-lg badge-{{ \App\Models\CopyrightRegistration::$statusColor[$copyrightRegistration->status] ?? '' }}">{{ \App\Models\CopyrightRegistration::$statusLabel[$copyrightRegistration->status] ?? '' }}</span></span>
                </div>
            </div>
        </div>
    @else
        <div class="col-12 mb-5"><h3 class="mb-md-0 mb-3">{{ __('Thông tin sản phẩm') }}</h3></div>
    @endisset

    <div class="col-md-6 col-12 mb-7">
        <div class="row">
            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Tên sản phẩm') }}</label>
                <input type="text" name="name" class="form-control mb-3 mb-lg-0 @error('name') border-danger @enderror" value="{{ old('name', isset($copyrightRegistration) ? $copyrightRegistration->name : '') }}" />
                @error('name')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Loại hình') }}</label>
                <select name="type_id" class="form-select fw-bold" data-kt-select2="true" data-placeholder="{{ __('Chọn loại hình') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($types as $key => $value)
                        <option value="{{ $key }}" {{ old('type_id') ? (old('type_id') == $key ? 'selected' : '') : ((isset($copyrightRegistration) && $copyrightRegistration->type_id == $key) ? 'selected' : '') }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 mb-7">
                <label class="required fw-semibold fs-6 mb-2">{{ __('Lĩnh vực') }}</label>
                <select name="field_id" class="form-select field_select fw-bold @error('field_id') border-danger @enderror" data-kt-select2="true" data-placeholder="{{ __('Chọn lĩnh vực') }}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    @foreach($fields as $key => $value)
                        <option value="{{ $key }}" {{ old('field_id') ? (old('field_id') == $key ? 'selected' : '') : ((isset($copyrightRegistration) && $copyrightRegistration->field_id == $key) ? 'selected' : '') }}>{{ $value }}</option>
                    @endforeach
                </select>
                @error('field_id')
                    <span class="text-danger d-block mt-1">{{ $message }}</span>
                @enderror
            </div>

            <div class="col-12 mb-7">
                <label class="fw-semibold fs-6 mb-2">{{ __('Mô tả') }}</label>
                <textarea name="description" id="kt_docs_ckeditor" class="form-control mb-3 mb-lg-0">{{ old('description', isset($copyrightRegistration) ? $copyrightRegistration->description : '') }}</textarea>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-12">
        <div class="col-12 mb-7">
            <label class="required fw-semibold fs-6 mb-2">{{ __('Nội dung bản quyền cần bảo vệ') }}</label>
            <!--begin::Dropzone-->
            <div class="dropzone dropzone-with-field @if(!isset($copyrightRegistration) || !$copyrightRegistration->field_id) disable-dropzone @endif @error('url_content_protection') border-danger bg-light-danger @enderror {{ auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status === \App\Models\CopyrightRegistration::STATUS_CANCEL ? 'cursor-not-allowed' : '' }}" id="dropzonejs_url_content_protection">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary @error('url_content_protection') text-danger @enderror"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Chọn file') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Upload 01 file nội dung số để đăng ký bảo vệ bản quyền. Vui lòng lưu lại file gốc để bảo vệ quyền lợi của mình trong tương lai.') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Loại file cho phép: x-wav, wav, mpeg, mp3, pdf, jpeg, png, mp4, doc, docx') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Dung lượng tối đa: 200MB/file') }}</div>
                    </div>
                </div>
            </div>
            <!--end::Dropzone-->
            @error('url_content_protection')
                <span class="text-danger d-block mt-1">{{ $message }}</span>
            @enderror

            <div class="mt-5 list-image box-url_content_protection">
                @if(old('url_content_protection'))
                    <div class="image-item mb-2 p-3 bg-light-info rounded">
                        @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status !== \App\Models\CopyrightRegistration::STATUS_CANCEL))
                            <div class="overlay-layer"></div>
                            <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                <a href="{{ \App\Helpers\FileHelper::generateLink(old('url_content_protection')) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                    <span class="path3"></span>
                                    <span class="path4"></span>
                                </i>
                            </div>
                            <input type="hidden" name="url_content_protection" value="{{ old('url_content_protection') }}">
                        @endif
                        @php $urlCustom = explode('/', old('url_content_protection')) @endphp
                        <span>{{ end($urlCustom) }}</span>
                    </div>
                @elseif(isset($copyrightRegistration) && !$errors->has('url_content_protection'))
                    <div class="image-item mb-2 p-3 bg-light-info rounded">
                        @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status !== \App\Models\CopyrightRegistration::STATUS_CANCEL))
                            <div class="overlay-layer"></div>
                            <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                <a href="{{ \App\Helpers\FileHelper::generateLink($copyrightRegistration->url_content_protection) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                    <span class="path3"></span>
                                    <span class="path4"></span>
                                </i>
                            </div>
                            <input type="hidden" name="url_content_protection" value="{{ $copyrightRegistration->url_content_protection }}">
                        @endif
                        @php $urlCustom = explode('/', $copyrightRegistration->url_content_protection) @endphp
                        <span>{{ end($urlCustom) }}</span>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-12 mb-7">
            <div class="d-flex align-items-center justify-content-between">
                <label class="fw-semibold fs-6 mb-2">{{ __('Hồ sơ đăng ký bản quyền') }}</label>
                <a href="{{ asset('templates/Tai_Lieu_ĐKBQTG-20230522T090931Z-001.zip') }}" class="d-flex align-items-center gap-1 text-primary fs-5" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Tải xuống') }}">
                    <i class="ki-duotone ki-file-down fs-1 text-primary">
                        <span class="path1"></span>
                        <span class="path2"></span>
                    </i>
                    <span>{{ __('Mẫu hồ sơ đăng ký') }}</span>
                </a>
            </div>
            <!--begin::Dropzone-->
            <div class="dropzone {{ auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status === \App\Models\CopyrightRegistration::STATUS_CANCEL ? 'cursor-not-allowed' : '' }}" id="dropzonejs_url_copyright_registrations">
                <div class="dz-message needsclick align-items-center flex-shrink-0 mt-xl-0 mt-5">
                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span class="path2"></span></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">{{ __('Chọn file') }}</h3>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Kéo và thả tập tin vào đây hoặc click chọn') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Tải lên tối đa 10 tài liệu liên quan. Sau khi đăng ký bản quyền thành công, chỉ mã hash dữ liệu sẽ được lưu trữ và dữ liệu gốc sẽ bị xóa. Vui lòng giữ các tệp gốc để bảo vệ quyền của bạn trong tương lai.') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Loại file cho phép: x-wav, wav, mpeg, mp3, pdf, jpeg, png, mp4, doc, docx') }}</div>
                        <div class="fs-7 fw-semibold text-gray-500">- {{ __('Dung lượng tối đa: 200MB/file') }}</div>
                    </div>
                </div>
            </div>
            <!--end::Dropzone-->

            <div class="mt-5 list-image box-url_copyright_registrations">
                @if(old('url_copyright_registrations'))
                    @foreach(old('url_copyright_registrations') as $image)
                        <div class="image-item mb-2 p-3 bg-light-info rounded">
                            @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status !== \App\Models\CopyrightRegistration::STATUS_CANCEL))
                                <div class="overlay-layer"></div>
                                <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                    <a href="{{ \App\Helpers\FileHelper::generateLink($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                        <span class="path3"></span>
                                        <span class="path4"></span>
                                    </i>
                                </div>
                                <input type="hidden" name="url_copyright_registrations[]" value="{{ $image }}">
                            @endif
                            @php $urlCustom = explode('/', $image) @endphp
                            <span>{{ end($urlCustom) }}</span>
                        </div>
                    @endforeach
                @elseif(isset($copyrightRegistration) && $copyrightRegistration->url_copyright_registrations)
                    @foreach($copyrightRegistration->url_copyright_registrations as $image)
                        <div class="image-item mb-2 p-3 bg-light-info rounded">
                            @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && isset($copyrightRegistration) && $copyrightRegistration->status !== \App\Models\CopyrightRegistration::STATUS_CANCEL))
                                <div class="overlay-layer"></div>
                                <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                                    <a href="{{ \App\Helpers\FileHelper::generateLink($image) }}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                                    <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                                        <span class="path1"></span>
                                        <span class="path2"></span>
                                        <span class="path3"></span>
                                        <span class="path4"></span>
                                    </i>
                                </div>
                                <input type="hidden" name="url_copyright_registrations[]" value="{{ $image }}">
                            @endif
                            @php $urlCustom = explode('/', $image) @endphp
                            <span>{{ end($urlCustom) }}</span>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {

            })
            .catch(error => {
                console.error(error);
            });

        @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && (!isset($copyrightRegistration) || $copyrightRegistration->status !== \App\Models\CopyrightRegistration::STATUS_CANCEL)))
            function generateHtml(urlPreview, inputValue, inputHiddenName) {
                let linkDisplay = inputValue.split('/')
                return `<div class="image-item mb-2 p-3 bg-light-info rounded">
                    <div class="overlay-layer"></div>
                    <div class="d-flex align-items-center justify-content-end gap-1 group-btn-file">
                        <a href="${urlPreview}" target="_blank" class="lh-1" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xem') }}"><i class="ki-solid ki-eye fs-2 text-success"></i></a>
                        <i class="ki-duotone ki-trash-square fs-1 text-danger cursor-pointer btn-remove-image" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="top" title="{{ __('Xóa') }}">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                            <span class="path4"></span>
                        </i>
                    </div>
                    <input type="hidden" name="${inputHiddenName}" value="${inputValue}">
                    <span>${linkDisplay[linkDisplay.length - 1]}</span>
                </div>`
            }

            initDropzoneUploadOneFileAndShowPreview('#dropzonejs_url_content_protection', function(response, file) {
                if (!file.type.includes('video')) {
                    $('.list-image.box-url_content_protection').html(generateHtml(response.url, response.url, 'url_content_protection'))
                } else {
                    saveToLibrary(response, file)
                    $('.list-image.box-url_content_protection').html(generateHtml('{{ config('default.url_server_file') }}'+response.data.link, response.data.link, 'url_content_protection'))
                }
            }, 200, '.x-wav, .wav, .mpeg, .mp3, .pdf, .jpeg, .png, .mp4, .doc, .docx')

            let canUploadFiles = 10 - parseInt('{{ (isset($copyrightRegistration) && $copyrightRegistration->url_copyright_registrations) ? count($copyrightRegistration->url_copyright_registrations) : 0 }}');
            initDropzoneUploadMultiFile('#dropzonejs_url_copyright_registrations', function (response, file) {
                if (!file.type.includes('video')) {
                    $('.list-image.box-url_copyright_registrations').append(generateHtml(response.url, response.url, 'url_copyright_registrations[]'))
                } else {
                    saveToLibrary(response, file)
                    $('.list-image.box-url_copyright_registrations').append(generateHtml('{{ config('default.url_server_file') }}'+response.data.link, response.data.link, 'url_copyright_registrations[]'))
                }
            }, canUploadFiles, 200, '.x-wav, .wav, .mpeg, .mp3, .pdf, .jpeg, .png, .mp4, .doc, .docx')

            function saveToLibrary(response, file) {
                const file_id = response.data.video_id
                const title = file.name
                const author = response.data.author
                const url = response.data.link
                const thumb = response.data.thumbnail
                const duration = response.data.duration
                const type = file.type
                const size = file.size
                const field_id = $('.field_select').val()
                $.ajax({
                    url: '{{ route('library.store') }}',
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        file_id,
                        title,
                        author,
                        url,
                        thumb,
                        duration,
                        type,
                        size,
                        field_id
                    },
                    success: function (response) {
                        if (response.status) {
                            {{--toastr.success("{{ __('Thêm đối tượng thành công') }}");--}}
                        } else {
                            toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                        }
                    }
                }).always(function () {
                    KTApp.hidePageLoading()
                })
            }

            $(document).on('click', '.btn-remove-image', function () {
                Swal.fire({
                    title: "{{ __('Bạn có chắc muốn xóa tệp này không?') }}",
                    icon: "question",
                    buttonsStyling: false,
                    showCancelButton: true,
                    confirmButtonText: "{{ __('Xóa') }}",
                    cancelButtonText: "{{ __("Hủy") }}",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-light-secondary text-black-50"
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).closest('.image-item').remove()
                    }
                })

            })
        @endif
    </script>
@endpush
