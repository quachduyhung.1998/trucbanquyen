@extends('layouts._blank')
@section('title', __('403'))

@push('css')
    <style>
        body {
            background-image: url('{{ asset('assets/media/auth/bg1.jpg') }}');
        }
        [data-bs-theme="dark"] body {
            background-image: url('{{ asset('assets/media/auth/bg1-dark.jpg') }}');
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-center flex-column-fluid">
        <div class="d-flex flex-column flex-center text-center p-10">
            <div class="card card-flush w-lg-650px py-5">
                <div class="card-body py-15 py-lg-20">
                    <h1 class="fw-bolder fs-2qx text-gray-900 mb-8">{{ __('Trang hiện tại đã hết hạn, bạn vui lòng load lại trang để tiếp tục') }}</h1>
                    <div class="mb-0">
                        <a href="{{ url()->previous() }}" class="btn btn-sm btn-primary">{{ __('Trở về') }}</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
