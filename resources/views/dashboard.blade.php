@extends('layouts.app')
@section('title', __('Tổng quan'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="row g-5 g-xl-10">
                    <div class="col-xl-4">
                        <div class="card card-flush h-xl-100">
                            <!--begin::Heading-->
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-250px bg-success" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-white pt-15">
                                    <span class="fw-bold fs-2x mb-3">{{ __('Đăng ký bản quyền') }}</span>
                                    <div class="fs-4 text-white">
                                        <span class="opacity-75">{{ __('Có') }}</span>
                                        <span class="position-relative d-inline-block">
                                            <a href="{{ route('copyright_registration.index') }}" class="link-white opacity-75-hover fw-bold d-block mb-1 text-primary">{{ $reportCopyrightRegistration['complete'] }} {{ __('bản quyền') }}</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                        <span class="opacity-75">{{ __('hoàn thành') }}</span>
                                    </div>
                                </h3>
                            </div>
                            <!--end::Heading-->
                            <!--begin::Body-->
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <a href="{{ route('copyright_registration.index', ['status' => \App\Models\CopyrightRegistration::STATUS_PENDING]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-timer fs-1 text-warning"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightRegistration['pending'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightRegistration::$statusLabel[\App\Models\CopyrightRegistration::STATUS_PENDING] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_registration.index', ['status' => \App\Models\CopyrightRegistration::STATUS_BEING_DEPLOYMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-watch fs-1 text-success"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightRegistration['being_development'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightRegistration::$statusLabel[\App\Models\CopyrightRegistration::STATUS_BEING_DEPLOYMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_registration.index', ['status' => \App\Models\CopyrightRegistration::STATUS_NEED_FURTHER_IMPROVEMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-update-folder fs-1 text-info"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightRegistration['further_improvement'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightRegistration::$statusLabel[\App\Models\CopyrightRegistration::STATUS_NEED_FURTHER_IMPROVEMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_registration.index', ['status' => \App\Models\CopyrightRegistration::STATUS_REJECT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-delete-folder fs-1 text-danger"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightRegistration['reject'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightRegistration::$statusLabel[\App\Models\CopyrightRegistration::STATUS_REJECT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-flush h-xl-100">
                            <!--begin::Heading-->
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-250px bg-info" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-white pt-15">
                                    <span class="fw-bold fs-2x mb-3">{{ __('Bảo vệ bản quyền') }}</span>
                                    <div class="fs-4 text-white">
                                        <span class="opacity-75">{{ __('Có') }}</span>
                                        <span class="position-relative d-inline-block">
                                            <a href="{{ route('copyright_protection.index') }}" class="link-white opacity-75-hover fw-bold d-block mb-1 text-primary">{{ $reportCopyrightProtection['complete'] }} {{ __('bản quyền') }}</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                        <span class="opacity-75">{{ __('hoàn thành') }}</span>
                                    </div>
                                </h3>
                            </div>
                            <!--end::Heading-->
                            <!--begin::Body-->
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <a href="{{ route('copyright_protection.index', ['status' => \App\Models\CopyrightProtection::STATUS_PENDING]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-timer fs-1 text-warning"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightProtection['pending'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightProtection::$statusLabel[\App\Models\CopyrightProtection::STATUS_PENDING] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_protection.index', ['status' => \App\Models\CopyrightProtection::STATUS_BEING_DEPLOYMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-watch fs-1 text-success"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightProtection['being_development'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightProtection::$statusLabel[\App\Models\CopyrightProtection::STATUS_BEING_DEPLOYMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_protection.index', ['status' => \App\Models\CopyrightProtection::STATUS_NEED_FURTHER_IMPROVEMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-update-folder fs-1 text-info"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightProtection['further_improvement'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightProtection::$statusLabel[\App\Models\CopyrightProtection::STATUS_NEED_FURTHER_IMPROVEMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_protection.index', ['status' => \App\Models\CopyrightProtection::STATUS_REJECT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-delete-folder fs-1 text-danger"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightProtection['reject'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightProtection::$statusLabel[\App\Models\CopyrightProtection::STATUS_REJECT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-flush h-xl-100">
                            <!--begin::Heading-->
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-250px bg-danger" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-white pt-15">
                                    <span class="fw-bold fs-2x mb-3">{{ __('Báo cáo vi phạm') }}</span>
                                    <div class="fs-4 text-white">
                                        <span class="opacity-75">{{ __('Có') }}</span>
                                        <span class="position-relative d-inline-block">
                                            <a href="{{ route('copyright_report.index') }}" class="link-white opacity-75-hover fw-bold d-block mb-1 text-primary">{{ $reportCopyrightReport['complete'] }} {{ __('bản quyền') }}</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                        <span class="opacity-75">{{ __('hoàn thành') }}</span>
                                    </div>
                                </h3>
                            </div>
                            <!--end::Heading-->
                            <!--begin::Body-->
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <a href="{{ route('copyright_report.index', ['status' => \App\Models\CopyrightReport::STATUS_PENDING]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-timer fs-1 text-warning"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightReport['pending'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightReport::$statusLabel[\App\Models\CopyrightReport::STATUS_PENDING] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_report.index', ['status' => \App\Models\CopyrightReport::STATUS_BEING_DEPLOYMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-watch fs-1 text-success"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightReport['being_development'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightReport::$statusLabel[\App\Models\CopyrightReport::STATUS_BEING_DEPLOYMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_report.index', ['status' => \App\Models\CopyrightReport::STATUS_NEED_FURTHER_IMPROVEMENT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-update-folder fs-1 text-info"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightReport['further_improvement'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightReport::$statusLabel[\App\Models\CopyrightReport::STATUS_NEED_FURTHER_IMPROVEMENT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('copyright_report.index', ['status' => \App\Models\CopyrightReport::STATUS_REJECT]) }}" class="d-block bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-8">
                                                    <span class="symbol-label">
                                                        <i class="ki-outline ki-delete-folder fs-1 text-danger"></i>
                                                    </span>
                                                </div>
                                                <div class="m-0">
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{ $reportCopyrightReport['reject'] }}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">{{ \App\Models\CopyrightReport::$statusLabel[\App\Models\CopyrightReport::STATUS_REJECT] }}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-12 col-xxl-4 mb-md-5 mb-xl-10">
                        <div class="card card-flush h-md-50 mb-5 mb-xl-10">
                            <div class="card-header pt-5">
                                <div class="card-title d-flex flex-column">
                                    <div class="d-flex align-items-center">
                                        <span class="fs-2hx fw-bold text-gray-900 me-2 lh-1 ls-n2">{{ $totalVideoUploaded }}</span>
                                        <span class="fs-4 fw-semibold text-gray-500 me-2 align-self-start">{{ __('Video tải lên') }}</span>
                                    </div>
                                    <span class="text-gray-500 pt-1 fw-semibold fs-6">{{ __('7 ngày gần đây') }}</span>
                                </div>
                            </div>
                            <div class="card-body d-flex align-items-end px-0 pb-0">
                                <div id="video_uploaded" class="w-100" style="height: 80px"></div>
                            </div>
                        </div>
                        <div class="card card-flush h-md-50 mb-5 mb-xl-10">
                            <div class="card-header pt-5">
                                <div class="card-title d-flex flex-column">
                                    <div class="d-flex align-items-center">
                                        <span class="fs-2hx fw-bold text-gray-900 me-2 lh-1 ls-n2">{{ $totalVideoViolated }}</span>
                                        <span class="fs-4 fw-semibold text-gray-500 me-2 align-self-start">{{ __('Video cảnh báo') }}</span>
                                    </div>
                                    <span class="text-gray-500 pt-1 fw-semibold fs-6">{{ __('7 ngày gần đây') }}</span>
                                </div>
                            </div>
                            <div class="card-body d-flex align-items-end px-0 pb-0">
                                <div id="video_violate" class="w-100" style="height: 80px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xl-12 col-xxl-8 mb-5 mb-xl-0">
                        <div class="card card-flush overflow-hidden h-md-100">
                            <div class="card-header py-5">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bold text-gray-900">{{ __('30 ngày gần đây') }}</span>
                                </h3>
                            </div>
                            <div class="card-body d-flex justify-content-between flex-column pb-1 px-0">
                                <div id="kt_charts_widget_3" class="min-h-auto ps-4 pe-6" style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        const VideoUploaded = {
            init: function () {
                !function () {
                    const e = document.getElementById("video_uploaded");
                    if (e) {
                        const t = parseInt(KTUtil.css(e, "height")), a = KTUtil.getCssVariableValue("--bs-gray-500"),
                            l = KTUtil.getCssVariableValue("--bs-border-dashed-color"),
                            r = KTUtil.getCssVariableValue("--bs-primary"),
                            o = KTUtil.getCssVariableValue("--bs-gray-300"), i = new ApexCharts(e, {
                                series: [
                                    {
                                        name: "Videos",
                                        data: [
                                            @foreach($videoUploadedLast7daysStatistics as $item)
                                                {{ $item['total_video'] }},
                                            @endforeach
                                        ]
                                    }
                                ],
                                chart: {
                                    fontFamily: "inherit",
                                    type: "bar",
                                    height: t,
                                    toolbar: {show: !1},
                                    sparkline: {enabled: !0}
                                },
                                plotOptions: {bar: {horizontal: !1, columnWidth: ["55%"], borderRadius: 6}},
                                legend: {show: !1},
                                dataLabels: {enabled: !1},
                                stroke: {show: !0, width: 9, colors: ["transparent"]},
                                xaxis: {
                                    categories: [
                                        @foreach($videoUploadedLast7daysStatistics as $item)
                                            '{{ $item['date'] }}',
                                        @endforeach
                                    ],
                                    axisBorder: {show: !1},
                                    axisTicks: {show: !1, tickPlacement: "between"},
                                    labels: {show: !1, style: {colors: a, fontSize: "12px"}},
                                    crosshairs: {show: !1}
                                },
                                yaxis: {labels: {show: !1, style: {colors: a, fontSize: "12px"}}},
                                fill: {type: "solid"},
                                states: {
                                    normal: {filter: {type: "none", value: 0}},
                                    hover: {filter: {type: "none", value: 0}},
                                    active: {allowMultipleDataPointsSelection: !1, filter: {type: "none", value: 0}}
                                },
                                tooltip: {
                                    style: {fontSize: "12px"}
                                },
                                colors: [r, o],
                                grid: {
                                    padding: {left: 10, right: 10},
                                    borderColor: l,
                                    strokeDashArray: 4,
                                    yaxis: {lines: {show: !0}}
                                }
                            });
                        setTimeout((function () {
                            i.render()
                        }), 300)
                    }
                }()
            }
        };
        "undefined" != typeof module && (module.exports = VideoUploaded), KTUtil.onDOMContentLoaded((function () {
            VideoUploaded.init()
        }));

        const VideoViolate = {
            init: function () {
                !function () {
                    const e = document.getElementById("video_violate");
                    if (e) {
                        const t = parseInt(KTUtil.css(e, "height")), a = KTUtil.getCssVariableValue("--bs-gray-500"),
                            l = KTUtil.getCssVariableValue("--bs-border-dashed-color"),
                            r = KTUtil.getCssVariableValue("--bs-primary"),
                            o = KTUtil.getCssVariableValue("--bs-gray-300"), i = new ApexCharts(e, {
                                series: [
                                    {
                                        name: "Videos",
                                        data: [
                                            @foreach($videoViolateLast7daysStatistics as $item)
                                                {{ $item['total_video'] }},
                                            @endforeach
                                        ]
                                    }
                                ],
                                chart: {
                                    fontFamily: "inherit",
                                    type: "bar",
                                    height: t,
                                    toolbar: {show: !1},
                                    sparkline: {enabled: !0}
                                },
                                plotOptions: {bar: {horizontal: !1, columnWidth: ["55%"], borderRadius: 6}},
                                legend: {show: !1},
                                dataLabels: {enabled: !1},
                                stroke: {show: !0, width: 9, colors: ["transparent"]},
                                xaxis: {
                                    categories: [
                                        @foreach($videoViolateLast7daysStatistics as $item)
                                            '{{ $item['date'] }}',
                                        @endforeach
                                    ],
                                    axisBorder: {show: !1},
                                    axisTicks: {show: !1, tickPlacement: "between"},
                                    labels: {show: !1, style: {colors: a, fontSize: "12px"}},
                                    crosshairs: {show: !1}
                                },
                                yaxis: {labels: {show: !1, style: {colors: a, fontSize: "12px"}}},
                                fill: {type: "solid"},
                                states: {
                                    normal: {filter: {type: "none", value: 0}},
                                    hover: {filter: {type: "none", value: 0}},
                                    active: {allowMultipleDataPointsSelection: !1, filter: {type: "none", value: 0}}
                                },
                                tooltip: {
                                    style: {fontSize: "12px"}
                                },
                                colors: [r, o],
                                grid: {
                                    padding: {left: 10, right: 10},
                                    borderColor: l,
                                    strokeDashArray: 4,
                                    yaxis: {lines: {show: !0}}
                                }
                            });
                        setTimeout((function () {
                            i.render()
                        }), 300)
                    }
                }()
            }
        };
        "undefined" != typeof module && (module.exports = VideoViolate), KTUtil.onDOMContentLoaded((function () {
            VideoViolate.init()
        }));

        const KTChartsWidget3 = function () {
            const e = {self: null, rendered: !1}, t = function (e) {
                const t = document.getElementById("kt_charts_widget_3");
                if (t) {
                    const a = parseInt(KTUtil.css(t, "height")),
                        l = KTUtil.getCssVariableValue("--bs-gray-500"),
                        r = KTUtil.getCssVariableValue("--bs-border-dashed-color"),
                        o = KTUtil.getCssVariableValue("--bs-success"), i = {
                            series: [
                                {
                                    name: "{{ __('Video tải lên') }}",
                                    data: [
                                        @foreach($videoUploadedLast30daysStatistics as $item)
                                            '{{ $item['total_video'] }}',
                                        @endforeach
                                    ]
                                },
                                {
                                    name: "{{ __('Video cảnh báo') }}",
                                    data: [
                                        @foreach($videoViolateLast30daysStatistics as $item)
                                            '{{ $item['total_video'] }}',
                                        @endforeach
                                    ]
                                }
                            ],
                            chart: {fontFamily: "inherit", type: "area", height: a, toolbar: {show: !1}},
                            plotOptions: {},
                            legend: {show: !1},
                            dataLabels: {enabled: !1},
                            fill: {
                                type: "gradient",
                                gradient: {shadeIntensity: 1, opacityFrom: .4, opacityTo: 0, stops: [0, 80, 100]}
                            },
                            stroke: {curve: "smooth", show: !0, width: 3, colors: [o]},
                            xaxis: {
                                categories: [
                                    @foreach($videoUploadedLast30daysStatistics as $item)
                                        '{{ $item['date'] }}',
                                    @endforeach
                                ],
                                axisBorder: {show: !1},
                                axisTicks: {show: !1},
                                tickAmount: 6,
                                labels: {rotate: 0, rotateAlways: !0, style: {colors: l, fontSize: "12px"}},
                                crosshairs: {position: "front", stroke: {color: o, width: 1, dashArray: 3}},
                                tooltip: {enabled: !0, formatter: void 0, offsetY: 0, style: {fontSize: "12px"}}
                            },
                            yaxis: {
                                tickAmount: 4,
                                labels: {
                                    style: {colors: l, fontSize: "12px"}
                                }
                            },
                            states: {
                                normal: {filter: {type: "none", value: 0}},
                                hover: {filter: {type: "none", value: 0}},
                                active: {allowMultipleDataPointsSelection: !1, filter: {type: "none", value: 0}}
                            },
                            tooltip: {
                                style: {fontSize: "12px"}
                            },
                            colors: [KTUtil.getCssVariableValue("--bs-success")],
                            grid: {borderColor: r, strokeDashArray: 4, yaxis: {lines: {show: !0}}},
                            markers: {strokeColor: o, strokeWidth: 3}
                        };
                    e.self = new ApexCharts(t, i), setTimeout((function () {
                        e.self.render(), e.rendered = !0
                    }), 200)
                }
            };
            return {
                init: function () {
                    t(e), KTThemeMode.on("kt.thememode.change", (function () {
                        e.rendered && e.self.destroy(), t(e)
                    }))
                }
            }
        }();
        "undefined" != typeof module && (module.exports = KTChartsWidget3), KTUtil.onDOMContentLoaded((function () {
            KTChartsWidget3.init()
        }));
    </script>
@endpush
