@extends('layouts.app')
@section('title', __('Cập nhật bảo vệ bản quyền'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('copyright_protection.update', $copyrightProtection->id) }}" method="POST">
                            @csrf
                            @include('copyright_protection.form')

                            <div class="separator mb-7"></div>

                            <div class="d-flex align-items-center justify-content-between">
                                @can('copyright_protection.edit')
                                    @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && $copyrightProtection->status !== \App\Models\CopyrightProtection::STATUS_CANCEL))
                                        <button type="button" class="btn btn-danger me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_update_copyright_protection">
                                            <span class="indicator-label">{{ __('Cập nhật trạng thái') }}</span>
                                            <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                            </span>
                                        </button>
                                    @endif
                                @endcan
                                <div>
                                    @can('copyright_protection.edit')
                                        <button type="submit" class="btn btn-primary me-3" {{ auth()->user()->role === \App\Models\User::ROLE_MEMBER && $copyrightProtection->status === \App\Models\CopyrightProtection::STATUS_CANCEL ? 'disabled' : '' }}>
                                            <span class="indicator-label">{{ __('Cập nhật') }}</span>
                                            <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                            </span>
                                        </button>
                                    @endcan
                                    <a href="{{ route('copyright_protection.index') }}" class="btn btn-light">{{ __('Trở lại') }}</a>
                                </div>
                            </div>
                        </form>

                        <div class="border-1 border-dashed border-primary rounded mt-7 p-8">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <label class="fw-bold fs-3 mb-2">{{ __('Tạo tin nhắn mới') }}</label>
                                    <textarea name="notification_content" id="kt_docs_ckeditor_notification" class="form-control mb-3 mb-lg-0"></textarea>
                                    <span class="text-danger d-block mt-1 box-err-notification_content"></span>
                                    <button type="button" id="btn-submit-notification_content" class="btn btn-primary mt-3 mb-5" data-model_type="{{ \App\Models\Notification::MODEL_TYPE_COPYRIGHT_PROTECTION }}" data-model_id="{{ $copyrightProtection->id }}">
                                        <span class="indicator-label">{{ __('Gửi') }}</span>
                                        <span class="indicator-progress">{{ __('Đang gửi') }}...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                        </span>
                                    </button>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="fw-bold fs-3 mb-2">{{ __('Danh sách tin nhắn') }}</div>
                                    <div id="box-notification" class="mh-500px overflow-y-auto">
                                        @if ($notifications->count())
                                            @foreach($notifications as $notification)
                                                <div id="notification-{{ $notification->id }}" class="bg-light-{{ request('notification_access') == $notification->id ? 'info' : 'success' }} rounded mb-3 p-5 pb-1 notification-item">
                                                    <div class="d-flex align-items-center justify-content-between mb-2">
                                                        <b>{{ $notification->user?->full_name }}</b>
                                                        <span>{{ $notification->created_at->diffForHumans() }}</span>
                                                    </div>
                                                    @if ($notification->status_content)
                                                        <div class="mb-{{ $notification->content ? 2 : 4 }}">
                                                            <i>{{ __('Cập nhật trạng thái') }}:</i>&nbsp;
                                                            <i class="badge badge-{{ \App\Models\CopyrightProtection::$statusColor[$notification->status_content] ?? '' }}">{{ \App\Models\CopyrightProtection::$statusLabel[$notification->status_content] ?? '' }}</i>
                                                        </div>
                                                    @endif
                                                    <div>{!! $notification->content !!}</div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="notification-item"></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--begin::Modal - Update package-->
    <div class="modal fade" id="kt_modal_update_copyright_protection" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <div class="modal-content">
                <div class="modal-header" id="kt_modal_update_copyright_protection_header">
                    <h2 class="fw-bold">{{ __('Cập nhật trạng thái') }}</h2>
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                </div>
                <div class="modal-body px-5">
                    <form id="kt_modal_update_copyright_protection_form" class="form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $copyrightProtection->id }}">
                        <div class="d-flex flex-column scroll-y px-5 px-lg-10" id="kt_modal_update_copyright_protection_scroll" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_update_copyright_protection_header" data-kt-scroll-wrappers="#kt_modal_update_copyright_protection_scroll" data-kt-scroll-offset="300px">
                            <div class="fv-row mb-7">
                                <label class="required fw-semibold fs-6 mb-2">{{ __('Trạng thái') }}</label>
                                <select name="status" class="form-select fw-bold my-1" data-kt-select2="true" data-placeholder="{{ __('Chọn trạng thái') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @if (auth()->user()->role === \App\Models\User::ROLE_MEMBER)
                                        <option value="{{ \App\Models\CopyrightProtection::STATUS_CANCEL }}">{{ \App\Models\CopyrightProtection::$statusLabel[\App\Models\CopyrightProtection::STATUS_CANCEL] ?? '' }}</option>
                                    @else
                                        @foreach(\App\Models\CopyrightProtection::$statusLabel as $key => $value)
                                            @continue($copyrightProtection->status === $key || $key === \App\Models\CopyrightProtection::STATUS_CANCEL)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="text-danger d-block mt-1 box-err-status"></span>
                            </div>
                            <div class="fv-row mb-7">
                                <label class="fw-semibold fs-6 mb-2">{{ __('Lời nhắn') }}</label>
                                <textarea name="message" id="kt_docs_ckeditor_notification_change_status"></textarea>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal" data-kt-packages-modal-action="cancel">{{ __('Hủy') }}</button>
                            @can('copyright_protection.edit')
                                @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER || (auth()->user()->role === \App\Models\User::ROLE_MEMBER && $copyrightProtection->status !== \App\Models\CopyrightProtection::STATUS_CANCEL))
                                    <button type="button" id="btn-submit-change-status" class="btn btn-primary" data-kt-packages-modal-action="submit">
                                        <span class="indicator-label">{{ __('Cập nhật') }}</span>
                                        <span class="indicator-progress">{{ __('Đang cập nhật') }}...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                        </span>
                                    </button>
                                @endif
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        let description_change_status;
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor_notification_change_status'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {
                description_change_status = editor
            })
            .catch(error => {
                console.error(error);
            });

        $(document).on('click', '#btn-submit-change-status', function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            let url = '{{ route('copyright_protection.change_status') }}'
            let id = '{{ $copyrightProtection->id }}'
            let status = $('#kt_modal_update_copyright_protection_form select[name="status"]').val()
            $.ajax({
                url,
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    id,
                    status,
                    notification: description_change_status.getData(),
                },
                success: function (result) {
                    if (result.status) {
                        Swal.fire({
                            text: "{{ __('Cập nhật trạng thái thành công') }}",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        })

                        $('#kt_modal_update_copyright_protection').modal('hide')

                        setTimeout(function() {
                            window.location.reload()
                        }, 1500)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    const errors = error.responseJSON.errors
                    if (errors.status) {
                        $('#kt_modal_update_copyright_protection_form .box-err-status').html(errors.status[0])
                        $('#kt_modal_update_copyright_protection_form select[name="status"]').addClass('border-danger')
                    } else {
                        $('#kt_modal_update_copyright_protection_form .box-err-status').html('')
                        $('#kt_modal_update_copyright_protection_form select[name="status"]').removeClass('border-danger')
                    }
                }
            }).always(function () {
                $('#btn-submit-change-status .indicator-label').removeAttr('style')
                $('#btn-submit-change-status .indicator-progress').removeAttr('style')
                $('#btn-submit-change-status').attr('disabled', false)
            })
        })

        let notification_content;
        ClassicEditor
            .create(document.querySelector('#kt_docs_ckeditor_notification'), {
                toolbar: ['undo', 'redo', '|', 'heading', '|', 'bold', 'italic', '|', 'blockQuote', 'bulletedList', 'numberedList', 'outdent', 'indent']
            })
            .then(editor => {
                notification_content = editor
            })
            .catch(error => {
                console.error(error);
            });
        $('#btn-submit-notification_content').click(function() {
            $(this).find('.indicator-label').css('display', 'none')
            $(this).find('.indicator-progress').css('display', 'block')
            $(this).attr('disabled', true)
            const model_type = $(this).data('model_type')
            const model_id = $(this).data('model_id')
            const notification = notification_content.getData()
            if (notification === '') {
                $('.box-err-notification_content').html('{{ __('Bạn chưa nhập nội dung tin nhắn') }}')
                $(this).find('.indicator-label').removeAttr('style')
                $(this).find('.indicator-progress').removeAttr('style')
                $(this).attr('disabled', false)
                return;
            }
            $.ajax({
                url: '{{ route('notification.store') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    model_type,
                    model_id,
                    notification_content: notification
                },
                success: function (result) {
                    if (result.status) {
                        notification_content.setData('')
                        $('#box-notification .notification-item:first-child').before(`
                            <div class="bg-light-info rounded mb-3 p-5 pb-1 notification-item">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <b>{{ auth()->user()->full_name }}</b>
                                    <span>{{ now()->diffForHumans() }}</span>
                                </div>
                                <div>${notification}</div>
                            </div>
                        `)
                    } else {
                        toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                    }
                },
                error: function(error) {
                    toastr.error("{{ __('Đã có lỗi xảy ra, vui lòng thử lại sau') }}");
                }
            }).always(function () {
                $('#btn-submit-notification_content .indicator-label').removeAttr('style')
                $('#btn-submit-notification_content .indicator-progress').removeAttr('style')
                $('#btn-submit-notification_content').attr('disabled', false)
                $('.box-err-notification_content').html('')
            })
        })
    </script>
@endpush
