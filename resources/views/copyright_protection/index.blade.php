@extends('layouts.app')
@section('title', __('Bảo vệ bản quyền'))

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar pt-7 pt-lg-10 d-lg-none">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex align-items-stretch">
                <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                    <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                        <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-500 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">@yield('title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <div id="kt_app_content_container" class="app-container container-fluid">
                <div class="card">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form action="{{ route('copyright_protection.index') }}" class="d-lg-flex align-items-center gap-2">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></i>
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" data-kt-user-table-filter="search" class="form-control w-lg-350px w-100 ps-13" placeholder="Từ khóa" />
                                </div>
                                <select name="status" class="form-select fw-bold w-lg-300px w-100 my-1" data-kt-select2="true" data-placeholder="{{ __('Trạng thái') }}" data-allow-clear="true" data-hide-search="true">
                                    <option></option>
                                    @foreach(\App\Models\CopyrightProtection::$statusLabel as $key => $value)
                                        <option value="{{ $key }}" {{ request('status') == $key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary w-100 fw-semibold px-6 my-1">{{ __('Tìm kiếm') }}</button>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card toolbar-->
                        @can('copyright_protection.create')
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a href="{{ route('copyright_protection.create') }}" class="btn btn-primary">
                                        <i class="ki-outline ki-plus fs-2"></i>{{ __('Thêm mới') }}
                                    </a>
                                </div>
                            </div>
                        @endcan
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body table-responsive py-4">
                        <table class="table align-middle table-row-dashed table-responsive fs-6 gy-5">
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="text-center">{{ __('STT') }}</th>
                                <th class="min-w-125px">{{ __('Tên sản phẩm') }}</th>
                                <th class="min-w-125px">{{ __('Loại hình') }}</th>
                                <th class="min-w-125px">{{ __('Lĩnh vực') }}</th>
                                <th class="min-w-125px">{{ __('Chủ sở hữu') }}</th>
                                @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER && auth()->user()->role !== \App\Models\User::ROLE_CSKH)
                                    <th class="min-w-125px">{{ __('Người tạo') }}</th>
                                @endif
                                <th class="min-w-125px text-center">{{ __('Trạng thái') }}</th>
                                <th class="min-w-125px">{{ __('Thời gian tạo') }}</th>
                                <th class="text-end min-w-125px">{{ __('Thao tác') }}</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @if ($copyrightProtections->total() == 0)
                                <tr><td colspan="8">{{ __('Không có dữ liệu') }}</td></tr>
                            @else
                                @foreach($copyrightProtections as $index => $copyrightProtection)
                                    <tr>
                                        <td class="text-center">{{ ($copyrightProtections->currentPage() - 1) * $copyrightProtections->perPage() + $index+1 }}</td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <a href="{{ route('copyright_protection.edit', $copyrightProtection->id) }}" class="text-gray-800 text-hover-primary mb-1">{{ $copyrightProtection->name }}</a>
{{--                                                <div>--}}
{{--                                                    <span class="fs-7">{{ __('AI kiểm tra') }}:</span>--}}
{{--                                                    <span class="badge badge-{{ \App\Models\CopyrightRegistration::$statusAiDetectColor[$copyrightProtection->status_ai_detect] ?? '' }}">--}}
{{--                                                        {{ \App\Models\CopyrightRegistration::$statusAiDetectLabel[$copyrightProtection->status_ai_detect] ?? '' }}--}}
{{--                                                    </span>--}}
{{--                                                </div>--}}
                                            </div>
                                        </td>
                                        <td>{{ $copyrightProtection->type?->name }}</td>
                                        <td>{{ $copyrightProtection->field?->name }}</td>
                                        <td>{{ $copyrightProtection->owner_name }}</td>
                                        @if (auth()->user()->role !== \App\Models\User::ROLE_MEMBER && auth()->user()->role !== \App\Models\User::ROLE_CSKH)
                                            <td>{{ $copyrightProtection->user?->full_name }}</td>
                                        @endif
                                        <td class="text-center">
                                            <span class="badge badge-{{ \App\Models\CopyrightRegistration::$statusColor[$copyrightProtection->status] ?? '' }}">
                                                {{ \App\Models\CopyrightRegistration::$statusLabel[$copyrightProtection->status] ?? '' }}
                                            </span>
                                        </td>
                                        <td>{{ $copyrightProtection->created_at->format('H:i - d/m/Y') }}</td>
                                        <td class="text-end">
                                            @can('copyright_protection.edit')
                                                <a href="{{ route('copyright_protection.edit', $copyrightProtection->id) }}" class="btn btn-light btn-active-light-primary">
                                                    <i class="ki-solid ki-pencil fs-3"></i>
                                                    <span>{{ __('Cập nhật') }}</span>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{ $copyrightProtections->appends($_GET)->links('layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Content-->
    </div>
@endsection
