<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar"
     data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px"
     data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
    <div class="app-sidebar-header d-flex flex-stack d-none d-lg-flex pt-8 pb-2" id="kt_app_sidebar_header">
        <!--begin::Logo-->
        <a href="@can('view_dashboard') {{ route('dashboard') }} @else # @endcan" class="app-sidebar-logo">
            <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}"
                 class="h-40px d-none d-sm-inline app-sidebar-logo-default theme-light-show"/>
            <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}"
                 class="h-20px h-lg-25px theme-dark-show"/>
        </a>
        <!--end::Logo-->
        <!--begin::Sidebar toggle-->
        <div id="kt_app_sidebar_toggle"
             class="app-sidebar-toggle btn btn-sm btn-icon bg-light btn-color-gray-700 btn-active-color-primary d-none d-lg-flex rotate"
             data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
             data-kt-toggle-name="app-sidebar-minimize">
            <i class="ki-outline ki-text-align-right rotate-180 fs-1"></i>
        </div>
        <!--end::Sidebar toggle-->
    </div>
    <!--begin::Navs-->
    <div class="app-sidebar-navs flex-column-fluid py-6" id="kt_app_sidebar_navs">
        <div id="kt_app_sidebar_navs_wrappers" class="app-sidebar-wrapper hover-scroll-y my-2" data-kt-scroll="true"
             data-kt-scroll-activate="true" data-kt-scroll-height="auto"
             data-kt-scroll-dependencies="#kt_app_sidebar_header" data-kt-scroll-wrappers="#kt_app_sidebar_navs"
             data-kt-scroll-offset="5px">
            <div id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false"
                 class="app-sidebar-menu-primary menu menu-column menu-rounded menu-sub-indention menu-state-bullet-primary">
                <div class="menu-item mb-2 d-block d-lg-none">
                    <div class="menu-heading text-uppercase fs-7 fw-bold">Menu</div>
                    <div class="app-sidebar-separator separator"></div>
                </div>
                @can('view_dashboard')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'dashboard') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-home-2 fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Tổng quan') }}</span>
                        </a>
                    </div>
                @endcan
                @canany(['object_copyright.view', 'object_face.view'])
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ (str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'object_copyright.') || str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'object_face.')) ? 'here show' : '' }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <i class="ki-outline ki-user-tick fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Quản lý đối tượng') }}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            @can('object_copyright.view')
                                <div class="menu-item">
                                    <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'object_copyright.') ? 'active' : '' }}" href="{{ route('object_copyright.index') }}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">{{ __('Đối tượng bản quyền') }}</span>
                                    </a>
                                </div>
                            @endcan
                            @can('object_face.view')
                                <div class="menu-item">
                                    <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'object_face.') ? 'active' : '' }}" href="{{ route('object_face.index') }}">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">{{ __('Đối tượng khuôn mặt') }}</span>
                                    </a>
                                </div>
                            @endcan
                            <div class="menu-item">
                                <a class="menu-link" href="javascript:void(0)">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">{{ __('Đối tượng Audio') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endcanany
                @can('ai_approve.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'ai_approve.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'ai_approve.') ? 'active' : '' }}" href="{{ route('ai_approve.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-tablet-text-up fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('AI duyệt bản quyền') }}</span>
                        </a>
                    </div>
                @endcan
                @can('category.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'field.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'field.') ? 'active' : '' }}" href="{{ route('field.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-abstract-26 fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Lĩnh vực') }}</span>
                        </a>
                    </div>
                @endcan
                @can('category.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'type.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'type.') ? 'active' : '' }}" href="{{ route('type.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-abstract-25 fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Loại hình') }}</span>
                        </a>
                    </div>
                @endcan
                @can('package.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'package.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'package.') ? 'active' : '' }}" href="{{ route('package.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-security-user fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Gói thành viên') }}</span>
                        </a>
                    </div>
                @endcan
                @can('user.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'user.') ? 'active' : '' }}" href="{{ route('user.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-people fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Tài khoản') }}</span>
                        </a>
                    </div>
                @endcan
                @can('member.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'member.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'member.') ? 'active' : '' }}" href="{{ route('member.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-profile-user fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Thành viên') }}</span>
                        </a>
                    </div>
                @endcan
                @can('copyright_registration.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_registration.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_registration.') ? 'active' : '' }}" href="{{ route('copyright_registration.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-shield fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Đăng ký bản quyền') }}</span>
                        </a>
                    </div>
                @endcan
                @can('copyright_protection.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_protection.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_protection.') ? 'active' : '' }}" href="{{ route('copyright_protection.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-shield-tick fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Bảo vệ bản quyền') }}</span>
                        </a>
                    </div>
                @endcan
                @can('copyright_report.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_report.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'copyright_report.') ? 'active' : '' }}" href="{{ route('copyright_report.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-graph-up fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Báo cáo vi phạm') }}</span>
                        </a>
                    </div>
                @endcan
                @can('history.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'history.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'history.') ? 'active' : '' }}" href="{{ route('history.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-calendar-tick fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Lịch sử hoạt động') }}</span>
                        </a>
                    </div>
                @endcan
                @can('setting.view')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'setting.') ? 'here show' : '' }}">
                        <a class="menu-link {{ str_contains(\Illuminate\Support\Facades\Route::currentRouteName(), 'setting.') ? 'active' : '' }}" href="{{ route('setting.index') }}">
                            <span class="menu-icon">
                                <i class="ki-outline ki-setting-2 fs-2"></i>
                            </span>
                            <span class="menu-title">{{ __('Cài đặt') }}</span>
                        </a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    <!--end::Navs-->
</div>
