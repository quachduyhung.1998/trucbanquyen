@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item previous disabled"><a href="#" class="page-link"><i class="previous"></i></a></li>
        @else
            <li class="page-item previous"><a href="{{ $paginator->previousPageUrl() }}" class="page-link"><i class="previous"></i></a></li>
        @endif

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if ($i == $paginator->currentPage())
                <li class="page-item active"><span class="page-link">{{ $i }}</span></li>
            @elseif ($i > 1 && $i == $paginator->currentPage() - 2)
                <li class="page-item"><span class="page-link">...</span></li>
            @elseif ($i == 1 || ($i == $paginator->currentPage() + 1 || $i == $paginator->currentPage() - 1)  || $i == $paginator->lastPage())
                <li><a href="{{ $paginator->url($i) }}" class="page-link">{{ $i }}</a></li>
            @elseif ($i == $paginator->lastPage() - 1)
                <li class="page-item"><span class="page-link">...</span></li>
            @endif
        @endfor

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item next"><a href="{{ $paginator->nextPageUrl() }}" class="page-link"><i class="next"></i></a></li>
        @else
            <li class="page-item next disabled"><a href="#" class="page-link"><i class="next"></i></a></li>
        @endif
    </ul>
@endif
