@php
    $prev_color = $current_color = '';
    $prev_user_id = '';
@endphp
@foreach($notifications as $notification)
    @php
        while ($current_color == $prev_color && $notification->user_id !== $prev_user_id) {
            $current_color = config('default.color')[rand(1, 5)];
            $prev_user_id = $notification->user_id;
        }
        $prev_color = $current_color;
        $contentName = '';
        $contentStatusChange = '';
        if ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION) {
            $contentName = $notification->copyrightRegistration?->name;
            if ($notification->status_content) {
                $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightRegistration::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightRegistration::$statusLabel[$notification->status_content] ?? '') . '</span>';
            }
        } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_PROTECTION) {
            $contentName = $notification->copyrightProtection?->name;
            if ($notification->status_content) {
                $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightProtection::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightProtection::$statusLabel[$notification->status_content] ?? '') . '</span>';
            }
        } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REPORT) {
            $contentName = $notification->copyrightReport?->object;
            if ($notification->copyrightReport?->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED) {
                $contentName = $notification->copyrightReport?->copyrightProtected?->name;
            }
            if ($notification->status_content) {
                $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightReport::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightReport::$statusLabel[$notification->status_content] ?? '') . '</span>';
            }
        }
        $message = '';
        if ($notification->content) {
            if ($notification->status_content) {
                $message .= ' ' . __('với lời nhắn');
            }
            $message .= ': ' . str_replace(['<p>', '</p>'], ['', ''], $notification->content);
        }
    @endphp
    <a href="{{ route('notification.click', $notification->id) }}" class="position-relative d-flex flex-stack bg-hover-light-secondary rounded p-4 header-notification-item" data-notification-id="{{ $notification->id }}">
        <span class="position-absolute top-5"></span>
        <div class="d-flex align-items-center">
            <div class="symbol symbol-50px me-4">
                <div class="symbol-label rounded-circle fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $notification->user?->full_name)), 0, 1, 'UTF-8')) }}</div>
            </div>
            <div class="mb-0 me-2">
                <div class="fs-6 text-gray-800 notification-item-content">
                    <b>{{ $notification->user?->full_name }}</b>
                    <span>
                        {{ $notification->status_content ? __('đã cập nhật trạng thái sang') : __('đã thêm tin nhắn mới vào') }}
                        {!! $contentStatusChange !!}
                        {{ $notification->status_content ? __('cho') : '' }}
                        <b>{{ \App\Models\Notification::$modelTypeLabel[$notification->model_type] ?? '' }} ({{ $contentName }})</b>{{ $message }}
                    </span>
                </div>
                <span class="fs-8 {{ $notification->notificationUsers[0]->status === \App\Models\NotificationUser::STATUS_READ ? 'text-gray-500' : '' }}">{{ $notification->created_at->diffForHumans() }}</span>
            </div>
        </div>
    </a>
@endforeach
