<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
{{--    <meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>@hasSection('title') @yield('title') - @endif {{ config('app.name') }}</title>

    <link rel="shortcut icon" href="{{ asset('assets/media/logos/ttbqs-logo.svg') }}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />

    <!--begin::Vendor Stylesheets(used for this page only)-->
    @stack('css_this_page')
    <!--end::Vendor Stylesheets-->

    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->

    <link href="{{ asset('assets/css/app.css') }}?v={{ filemtime('assets/css/app.css') }}" rel="stylesheet" type="text/css" />

    @stack('css')
</head>
<body id="kt_app_body" data-kt-app-header-fixed="true" data-kt-app-header-fixed-mobile="true" data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true" data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true" data-kt-app-sidebar-push-footer="true" class="app-default">
    <div class="d-flex flex-column flex-root app-root" id="kt_app_root">
        <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
            @include('layouts.header')
            <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
                @include('layouts.sidebar')
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    @yield('content')
                    @include('layouts.footer')
                </div>
            </div>
        </div>
    </div>

    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <i class="ki-outline ki-arrow-up"></i>
    </div>
    <!--end::Scrolltop-->

    <!--begin::Page loading(append to body)-->
    <div class="page-loader flex-column bg-dark bg-opacity-25">
        <span class="spinner-border text-primary" role="status"></span>
        <span class="text-gray-800 fs-6 fw-semibold mt-5">Loading...</span>
    </div>
    <!--end::Page loading-->

    <!--begin::Theme mode setup on page load-->
    <script>
        var defaultThemeMode = "light";
        var themeMode;
        if (document.documentElement) {
            if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
                themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
            } else {
                if (localStorage.getItem("data-bs-theme") !== null) {
                    themeMode = localStorage.getItem("data-bs-theme");
                } else {
                    themeMode = defaultThemeMode;
                }
            }
            if (themeMode === "system") {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            }
            document.documentElement.setAttribute("data-bs-theme", themeMode);
        }
    </script>
    <!--end::Theme mode setup on page load-->

    <script>var hostUrl = "assets/";</script>
    <!--begin::Global Javascript Bundle(mandatory for all pages)-->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Javascript Bundle-->

    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js"></script>
    <script>
        const firebaseConfig = {
            apiKey: "AIzaSyCMldkdaK2VDYHlEu3Ssjj79dtMewROfwo",
            authDomain: "trucbanquyen-9558a.firebaseapp.com",
            projectId: "trucbanquyen-9558a",
            storageBucket: "trucbanquyen-9558a.appspot.com",
            messagingSenderId: "1086859153735",
            appId: "1:1086859153735:web:cfae58b3198bd38579fc62",
            measurementId: "G-P04V8DWLPC"
        };

        const app = firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();

        function initFirebaseMessagingRegistration() {
            messaging.requestPermission().then(function () {
                return messaging.getToken()
            }).then(function(fcm_token) {
                $.ajax({
                    url: '{{ route('notification.save_token') }}',
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        fcm_token
                    }
                })
            }).catch(function (err) {
                console.log(`Token Error :: ${err}`);
            });
        }

        initFirebaseMessagingRegistration();

        messaging.onMessage(function(payload) {
            let data = JSON.parse(payload.data['gcm.notification.data'])
            $.ajax({
                url: `/notifications/click/${data.notification_id}`
            })
            Swal.fire({
                title: "{{ __('Thông báo') }}",
                html: data.html,
                iconHtml: "",
                buttonsStyling: false,
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            })
        });
    </script>

    <script src="{{ asset('assets/js/app.js') }}?v={{ filemtime('assets/js/app.js') }}"></script>

    <script>
        @if (\Session::has('success'))
            toastr.success("{{ \Session::get('success') }}");
        @endif
        @if (\Session::has('error'))
            toastr.error("{{ \Session::get('error') }}");
        @endif
        @if (request('notification_access'))
            const domNotification = $("#notification-{{ request('notification_access') }}")
            if (domNotification.length) {
                $('html, body').animate({
                    scrollTop: domNotification.offset().top
                }, 800);
            }
        @endif
    </script>

    @stack('js')
</body>
</html>
