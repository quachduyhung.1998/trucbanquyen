<div id="kt_app_header" class="app-header">
    <div class="app-container container-fluid d-flex align-items-stretch flex-stack" id="kt_app_header_container">
        <!--begin::Sidebar toggle-->
        <div class="d-flex align-items-center d-block d-lg-none ms-n3" title="Show sidebar menu">
            <div class="btn btn-icon btn-active-color-primary w-35px h-35px me-2" id="kt_app_sidebar_mobile_toggle">
                <i class="ki-outline ki-abstract-14 fs-2"></i>
            </div>
            <!--begin::Logo image-->
            <a href="{{ route('dashboard') }}">
                <img alt="Logo" src="{{ asset('assets/media/logos/ttbqs-logo.svg') }}" class="h-30px" />
            </a>
            <!--end::Logo image-->
        </div>
        <!--end::Sidebar toggle-->
        <!--begin::Navbar-->
        <div class="app-navbar flex-lg-grow-1" id="kt_app_header_navbar">
            <div class="app-navbar-item d-flex align-items-stretch flex-lg-grow-1">
                <!--begin::Toolbar-->
                <div id="kt_app_toolbar" class="app-toolbar">
                    <div id="kt_app_toolbar_container" class="d-flex align-items-stretch">
                        <div class="app-toolbar-wrapper d-flex flex-stack flex-wrap gap-4 w-100">
                            <div class="page-title d-flex flex-column justify-content-center gap-1 me-3">
                                <h1 class="page-heading d-flex flex-column justify-content-center text-gray-900 fw-bold fs-3 m-0">@yield('title')</h1>
                                @if (\Illuminate\Support\Facades\Route::currentRouteName() != 'dashboard')
                                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0">
                                        <li class="breadcrumb-item text-muted">
                                            <a href="{{ route('dashboard') }}" class="text-muted text-hover-primary">{{ __('Tổng quan') }}</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <span class="bullet bg-gray-500 w-5px h-2px"></span>
                                        </li>
                                        <li class="breadcrumb-item text-muted">@yield('title')</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Toolbar-->
            </div>
            <!--begin::Notifications-->
            <div class="app-navbar-item ms-1 ms-md-3">
                {{-- data-kt-menu-trigger="{default: 'click', lg: 'hover'}" --}}
                <div class="btn btn-icon btn-color-gray-600 btn-active-light btn-active-color-primary w-35px h-35px w-md-40px h-md-40px position-relative btn-view-notification" data-kt-menu-trigger="{default: 'click'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                    <i class="ki-outline ki-notification-on fs-1"></i>
                    @if ($HAS_NEW_NOTIFICATION)
                        <span class="position-absolute top-0 start-100 translate-middle badge badge-circle badge-danger w-15px h-15px ms-n4 mt-3 box-has-new-notification">{{ $TOTAL_NOTIFICATION_UNREAD }}</span>
                    @endif
                </div>
                <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true" id="kt_menu_notifications">
                    <!--begin::Heading-->
                    <div class="d-flex flex-column bgi-no-repeat rounded-top" style="background-image:url('{{ asset('assets/media/misc/menu-header-bg.jpg') }}')">
                        <h3 class="text-white fw-semibold px-9 my-10">{{ __('Thông báo') }}
                            <span class="fs-8 opacity-75 ps-3">{{ $TOTAL_NOTIFICATION }} {{ __('thông báo') }}</span>
                        </h3>
                    </div>
                    <div class="scroll-y mh-325px my-5 px-4 header-list-notification">
                        @if ($TOTAL_NOTIFICATION)
                            @php
                                $prev_color = $current_color = '';
                                $prev_user_id = '';
                            @endphp
                            @foreach($NOTIFICATIONS as $notification)
                                @php
                                    while ($current_color == $prev_color && $notification->user_id !== $prev_user_id) {
                                        $current_color = config('default.color')[rand(1, 5)];
                                        $prev_user_id = $notification->user_id;
                                    }
                                    $prev_color = $current_color;
                                    $contentName = '';
                                    $contentStatusChange = '';
                                    if ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION) {
                                        $contentName = $notification->copyrightRegistration?->name;
                                        if ($notification->status_content) {
                                            $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightRegistration::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightRegistration::$statusLabel[$notification->status_content] ?? '') . '</span>';
                                        }
                                    } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_PROTECTION) {
                                        $contentName = $notification->copyrightProtection?->name;
                                        if ($notification->status_content) {
                                            $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightProtection::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightProtection::$statusLabel[$notification->status_content] ?? '') . '</span>';
                                        }
                                    } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REPORT) {
                                        $contentName = $notification->copyrightReport?->object;
                                        if ($notification->copyrightReport?->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED) {
                                            $contentName = $notification->copyrightReport?->copyrightProtected?->name;
                                        }
                                        if ($notification->status_content) {
                                            $contentStatusChange = '<span class="badge badge-' . (\App\Models\CopyrightReport::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightReport::$statusLabel[$notification->status_content] ?? '') . '</span>';
                                        }
                                    }
                                    $message = '';
                                    if ($notification->content) {
                                        if ($notification->status_content) {
                                            $message .= ' ' . __('với lời nhắn');
                                        }
                                        $message .= ': ' . str_replace(['<p>', '</p>'], ['', ''], $notification->content);
                                    }
                                @endphp
                                <a href="{{ route('notification.click', $notification->id) }}" class="position-relative d-flex flex-stack bg-hover-light-secondary rounded p-4 header-notification-item" data-notification-id="{{ $notification->id }}">
                                    <span class="position-absolute top-5"></span>
                                    <div class="d-flex align-items-center">
                                        <div class="symbol symbol-50px me-4">
                                            <div class="symbol-label rounded-circle fs-3 bg-light-{{ $current_color }} text-{{ $current_color }}">{{ strtoupper(mb_substr(last(explode(' ', $notification->user?->full_name)), 0, 1, 'UTF-8')) }}</div>
                                        </div>
                                        <div class="mb-0 me-2">
                                            <div class="fs-6 text-gray-800 text-break notification-item-content">
                                                <b>{{ $notification->user?->full_name }}</b>
                                                <span>
                                                    {{ $notification->status_content ? __('đã cập nhật trạng thái sang') : __('đã thêm tin nhắn mới vào') }}
                                                    {!! $contentStatusChange !!}
                                                    {{ $notification->status_content ? __('cho') : '' }}
                                                    <b>{{ \App\Models\Notification::$modelTypeLabel[$notification->model_type] ?? '' }} ({{ $contentName }})</b>{{ $message }}
                                                </span>
                                            </div>
                                            <span class="fs-8 {{ $notification->notificationUsers[0]->status === \App\Models\NotificationUser::STATUS_READ ? 'text-gray-500' : '' }}">{{ $notification->created_at->diffForHumans() }}</span>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @else
                            <div class="d-flex flex-column px-9">
                                <div class="pt-10 pb-0">
                                    <div class="text-center text-gray-600 fw-semibold pt-1">{{ __('Bạn không có thông báo nào.') }}</div>
                                </div>
                                <div class="text-center px-4">
                                    <img class="mw-100 mh-200px" alt="image" src="{{ asset('assets/media/illustrations/sketchy-1/1.png') }}" />
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="py-3 text-center border-top">
                        <a href="" class="btn btn-color-gray-600 btn-active-color-primary">
                            {{ __('Xem tất cả') }}
                            <i class="ki-outline ki-arrow-right fs-5"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!--end::Notifications-->

            <!--begin::User menu-->
            <div class="app-navbar-item ms-1 ms-md-3" id="kt_header_user_menu_toggle">
                <div class="cursor-pointer symbol symbol-circle symbol-35px symbol-md-45px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
{{--                    <img src="{{ auth()->user()->avatar ?? asset('assets/media/avatars/blank.png') }}" alt="user" />--}}
                    <div class="symbol symbol-circle symbol-35px overflow-hidden">
                        <div class="symbol-label fs-3 bg-light-danger text-danger">{{ strtoupper(mb_substr(last(explode(' ', auth()->user()->full_name)), 0, 1, 'UTF-8')) }}</div>
                    </div>
                </div>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                    <div class="menu-item px-3">
                        <div class="menu-content d-flex align-items-center px-3">
                            <div class="symbol symbol-50px me-5">
{{--                                <img alt="Logo" src="{{ auth()->user()->avatar ?? asset('assets/media/avatars/blank.png') }}" />--}}
                                <div class="symbol symbol-circle symbol-50px overflow-hidden">
                                    <div class="symbol-label fs-3 bg-light-danger text-danger">{{ strtoupper(mb_substr(last(explode(' ', auth()->user()->full_name)), 0, 1, 'UTF-8')) }}</div>
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="fw-bold d-flex align-items-center fs-5">{{ auth()->user()->full_name }}
                                    <span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">{{ \App\Models\User::$roleLabel[auth()->user()->role] ?? '' }}</span></div>
                                <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">{{ auth()->user()->email }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="separator my-2"></div>
                    <div class="menu-item px-5 my-1">
                        <a href="{{ route('profile') }}" class="menu-link px-5">{{ __('Thông tin tài khoản') }}</a>
                    </div>
                    <div class="menu-item px-5" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="left-start" data-kt-menu-offset="-15px, 0">
                        <a href="#" class="menu-link px-5">
                            <span class="menu-title position-relative">{{ __('Giao diện') }}
                                <span class="ms-5 position-absolute translate-middle-y top-50 end-0">
                                    <i class="ki-outline ki-night-day theme-light-show fs-2"></i>
                                    <i class="ki-outline ki-moon theme-dark-show fs-2"></i>
                                </span>
                            </span>
                        </a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-gray-500 menu-active-bg menu-state-color fw-semibold py-4 fs-base w-150px" data-kt-menu="true" data-kt-element="theme-mode-menu">
                            <div class="menu-item px-3 my-0">
                                <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="light">
                                    <span class="menu-icon" data-kt-element="icon">
                                        <i class="ki-outline ki-night-day fs-2"></i>
                                    </span>
                                    <span class="menu-title">{{ __('Sáng') }}</span>
                                </a>
                            </div>
                            <div class="menu-item px-3 my-0">
                                <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="dark">
                                    <span class="menu-icon" data-kt-element="icon">
                                        <i class="ki-outline ki-moon fs-2"></i>
                                    </span>
                                    <span class="menu-title">{{ __('Tối') }}</span>
                                </a>
                            </div>
                            <div class="menu-item px-3 my-0">
                                <a href="#" class="menu-link px-3 py-2" data-kt-element="mode" data-kt-value="system">
                                    <span class="menu-icon" data-kt-element="icon">
                                        <i class="ki-outline ki-screen fs-2"></i>
                                    </span>
                                    <span class="menu-title">{{ __('Mặc định') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="separator my-2"></div>
                    <div class="menu-item px-5">
                        <a href="{{ route('logout') }}" class="menu-link px-5">{{ __('Đăng xuất') }}</a>
                    </div>
                </div>
            </div>
            <!--end::User menu-->
        </div>
        <div class="app-navbar-separator separator d-none d-lg-flex"></div>
    </div>
</div>
