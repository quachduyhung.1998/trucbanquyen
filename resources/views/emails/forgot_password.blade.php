<!DOCTYPE html>
<html lang="en">
<body style="background-color: #edf2f7; font-size: 16px; color: #718096;">
<div style="margin: auto; padding: 25px 0; text-align: center;">
    <a href="{{ route('dashboard') }}" style="box-sizing: border-box; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; color: #3d4852; font-size: 19px; font-weight: bold; text-decoration: none; display: inline-block;">{{ config('app.name') }}</a>
</div>
<div style="background-color: #FFF; margin: auto; padding: 32px; width: 500px;">
    <h1 style="color: #3d4852; font-size: 18px; font-weight: bold;">{{ __('Xin chào') }} {{ $name }},</h1>
    <p style="box-sizing: border-box; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0;text-align: left;">{{ __('Chúc mừng bạn đã nhận được') }} <b>{{ money($amountReceived, 'VND') }}</b> {{ __('hoa hồng từ đơn hàng trị giá') }} <b>{{ money($priceOrder, 'VND') }}</b>.</p>
    <div style="margin: 20px auto; width: 100%; text-align: center;">
        <a href="{{ route('filament.resources.commission-histories.index') }}" style="box-sizing: border-box; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; border-radius: 4px; color: #fff; display: inline-block; overflow: hidden; text-decoration: none; background-color: #2d3748; border-bottom: 8px solid #2d3748; border-left: 18px solid #2d3748; border-right: 18px solid #2d3748; border-top: 8px solid #2d3748;" target="_blank">
            {{ __('Xem lịch sử nhận hoa hồng') }}
        </a>
    </div>
    <p style="box-sizing: border-box; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0;text-align: left;">{{ __('Cảm ơn bạn đã sử dụng ứng dụng của chúng tôi!') }}</p>
    <p style="box-sizing: border-box; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; font-size: 16px; line-height: 1.5em; margin-top: 0;text-align: left;">{{ __('Trân trọng') }},<br>{{ config('app.name') }}</p>
</div>
<div style="padding: 32px;">
    <p style="color: #b0adc5; font-size: 12px; text-align: center">© 2023 {{ config('app.name') }}. All rights reserved.</p>
</div>
</body>
</html>
