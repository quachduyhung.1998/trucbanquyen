$(document).on('input', '.input-format-number', function() {
    let amount = $(this).val();
    let temp = amount.replace(/\./g,"");
    amount = temp.replace(/,/g,"");
    if ($.isNumeric(amount)) {
        value = new Intl.NumberFormat().format(amount);
        $(this).val(value);
    }
})

function formatInputNumber() {
    $('.input-format-number').each(function() {
        let amount = $(this).val();
        let temp = amount.replace(/\./g,"");
        amount = temp.replace(/\,/g,"");
        if ($.isNumeric(amount)) {
            value = new Intl.NumberFormat().format(amount);
            $(this).val(value);
        }
    })
}

function initDropzoneUploadOneFile(idElement, callback = undefined, maxFilesize = 2, acceptedFiles = '.jpeg, .jpg, .png', toInput = null, imagePreview = null) {
    let _token = $('meta[name="csrf-token"]').attr('content')
    let dropzone = new Dropzone(idElement, {
        url: "/upload-file", // Set the url for your upload script location
        method: "post",
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize, // MB
        acceptedFiles,
        parallelUploads: 1, // Số lượng file được xử lý đồng thời : 1
        // addRemoveLinks: true,
        sending: function(file, xhr, formData) {
            formData.append("_token", _token);
        },
        accept: function(file, done) {
            done()
        },
        success: function (file, response) {
            if (toInput) $(toInput).val(response.url)
            else $(idElement).find('input[type="hidden"]').val(response.url)

            if (imagePreview) {
                $(imagePreview).attr('src', response.url)
            } else {
                $(idElement).find('.image_preview').attr('href', response.url)
                $(idElement).find('.image_preview img').attr('src', response.url)
            }

            if (callback && {}.toString.call(callback) === '[object Function]') {
                callback(response.url, file);
            }

            toastr.success("Tải file lên thành công");
        },
        error: function (file, errorMessage) {
            if (errorMessage === 'You can\'t upload files of this type.') {
                toastr.error('File không đúng định dạng')
            } else if (errorMessage.includes('File is too big')) {
                toastr.error(`File quá lớn (${(file.size/1024/1024).toFixed(2)}MiB). Dung lượng file cho phép: 2MiB`)
            } else {
                toastr.error(errorMessage);
            }
        },
        addedfile: function (file) {
            if (this.files[1] != null) {
                this.removeFile(this.files[0]);
            }
        }
    });
}

function initDropzoneUploadOneFileAndShowPreview(idElement, callback = undefined, maxFilesize = 2, acceptedFiles = '.jpeg, .jpg, .png', baseUrlServerUpload = null, author = null, urlCallback = null) {
    let _token = $('meta[name="csrf-token"]').attr('content')
    let dropzone = new Dropzone(idElement, {
        url: "/upload-file", // Set the url for your upload script location
        method: "post",
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize, // MB
        acceptedFiles,
        parallelUploads: 1, // Số lượng file được xử lý đồng thời : 1
        // addRemoveLinks: true,
        sending: function(file, xhr, formData) {
            let nameSplit = file.name.split('.')
            nameSplit.splice(nameSplit.length - 1, 1)

            formData.append("_token", _token);
            formData.append("author", author);
            formData.append("title", nameSplit.join(' '));
            formData.append("callback", urlCallback);
        },
        accept: function(file, done) {
            done()
        },
        success: function (file, response) {
            if (callback && {}.toString.call(callback) === '[object Function]') {
                callback(response, file);
            }

            toastr.success("Tải file lên thành công");
        },
        error: function (file, errorMessage) {
            if (typeof errorMessage === 'string') {
                if (errorMessage === 'You can\'t upload files of this type.') {
                    toastr.error('File không đúng định dạng')
                } else if (errorMessage.includes('File is too big')) {
                    toastr.error(`File quá lớn (${(file.size / 1024 / 1024).toFixed(2)}MiB). Dung lượng file cho phép: 2MiB`)
                }
            } else {
                toastr.error('Đã có lỗi xảy ra, vui lòng thử lại sau');
            }
        }
    });

    dropzone.on('addedfile', function(file) {
        if (this.files[1] != null) {
            this.removeFile(this.files[0]);
            $('.dz-image-preview:first-child').remove()
        }
    })
}

function initDropzoneUploadMultiFile(idElement, callback = undefined, maxFiles = 20, maxFilesize = 2, acceptedFiles = '.jpeg, .jpg, .png', baseUrlServerUpload = null, author = null, urlCallback = null) {
    let _token = $('meta[name="csrf-token"]').attr('content')
    let dropzone = new Dropzone(idElement, {
        url: "/upload-file", // Set the url for your upload script location
        method: "post",
        paramName: "file", // The name that will be used to transfer the file
        maxFiles,
        maxFilesize, // MB
        acceptedFiles,
        parallelUploads: 1, // Số lượng file được xử lý đồng thời : 1
        // addRemoveLinks: true,
        sending: function(file, xhr, formData) {
            let nameSplit = file.name.split('.')
            nameSplit.splice(nameSplit.length - 1, 1)

            formData.append("_token", _token);
            formData.append("author", author);
            formData.append("title", nameSplit.join(' '));
            formData.append("callback", urlCallback);
        },
        accept: function(file, done) {
            done()
        },
        success: function (file, response) {
            if (callback && {}.toString.call(callback) === '[object Function]') {
                callback(response, file);
            }

            toastr.success("Tải file lên thành công");
        },
        error: function (file, errorMessage) {
            if (typeof errorMessage === 'string') {
                if (errorMessage === 'You can\'t upload files of this type.') {
                    toastr.error('File không đúng định dạng')
                } else if (errorMessage.includes('File is too big')) {
                    toastr.error(`File quá lớn (${(file.size / 1024 / 1024).toFixed(2)}MiB). Dung lượng file cho phép: 2MiB`)
                }
            } else {
                toastr.error('Đã có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        // addedfile: function () {
        //     if (this.files[1] != null) {
        //         this.removeFile(this.files[0]);
        //     }
        // }
    });
}

function initDropzoneUploadOneFileVideo(idElement, baseUrlServerUpload, author, urlCallback, callback = undefined, maxFilesize = 500, acceptedFiles = '.mp4') {
    let _token = $('meta[name="csrf-token"]').attr('content')
    let dropzone = new Dropzone(idElement, {
        url: `${baseUrlServerUpload}/api/v1/video/add`, // Set the url for your upload script location
        method: "post",
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize, // MB
        acceptedFiles,
        parallelUploads: 1, // Số lượng file được xử lý đồng thời : 1
        // addRemoveLinks: true,
        sending: function(file, xhr, formData) {
            let nameSplit = file.name.split('.')
            nameSplit.splice(nameSplit.length - 1, 1)

            formData.append("author", author);
            formData.append("title", nameSplit.join(' '));
            formData.append("callback", urlCallback);
        },
        accept: function(file, done) {
            done()
        },
        success: function (file, response) {
            if (callback && {}.toString.call(callback) === '[object Function]') {
                callback(response, file);
            }

            toastr.success("Tải file lên thành công");
        },
        error: function (file, errorMessage) {
            if (errorMessage.detail) {
                toastr.error('Đã có lỗi xảy ra, vui lòng thử lại sau');
            } else if (errorMessage === 'You can\'t upload files of this type.') {
                toastr.error('File không đúng định dạng')
            } else if (errorMessage === 'You can not upload any more files.') {
                toastr.error('Đã quá số lượng file cho phép')
            } else if (errorMessage.includes('File is too big')) {
                toastr.error(`File quá lớn (${(file.size/1024/1024).toFixed(2)}MB). Dung lượng file cho phép: ${maxFilesize}MB`)
            } else {
                toastr.error(errorMessage);
            }
        }
    });

    dropzone.on('addedfile', function(file) {
        if (this.files[1] != null) {
            this.removeFile(this.files[0]);
            $('.dz-image-preview:first-child').remove()
        }
    })
}

//scroll load notification
let scrollLoadNotification = () => {
    let is_busy = false;	// Biến dùng kiểm tra nếu đang gửi ajax thì ko thực hiện gửi thêm
    let page = 1; 			// Biến lưu trữ trang hiện tại

    let first_notification_id;
    if ($('.header-notification-item').length) {
        first_notification_id = $('.header-notification-item').first().data('notification-id');
    }
    let listNotification = $('.header-list-notification');
    let loading = '<div class="loading"><div class="bar"><span class="dot1"></span><span class="dot2"></span><span class="dot3"></span></div></div>';
    listNotification.scroll(() => {
        if (listNotification.scrollTop() + listNotification.height() >= listNotification.height()) {
            if (is_busy === true) return false; // Nếu đang gửi ajax thì ngưng
            is_busy = true;	// Thiết lập đang gửi ajax
            listNotification.append(loading);
            page++;
            setTimeout(() => {
                listNotification.find('.loading').remove();
                $.ajax({
                    type: 'GET',
                    url: `/notifications/load-more`,
                    data: { page, first_notification_id },
                    success: (result) => {
                        if (result.length > 0) {
                            is_busy = false;
                            listNotification.append(result);
                        }
                    },
                });
            }, 1000);
        }
    });
}

$(document).ready(function() {
    formatInputNumber()
    scrollLoadNotification()

    $(".datepicker").flatpickr({
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
    });

    $('.btn-close-modal-and-reload').click(function() {
        window.location.reload()
    })

    $('.btn-view-notification').click(function() {
        $.ajax({
            url: '/notifications/view',
            success: function(result) {
                $('.box-has-new-notification').remove()
            }
        })
    })

    $('.field_select').change(function() {
        if($(this).val()) {
            $('.dropzone-with-field').removeClass('disable-dropzone')
        } else {
            $('.dropzone-with-field').addClass('disable-dropzone')
        }
    })
})
