importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyCMldkdaK2VDYHlEu3Ssjj79dtMewROfwo",
    projectId: "trucbanquyen-9558a",
    messagingSenderId: "1086859153735",
    appId: "1:1086859153735:web:cfae58b3198bd38579fc62"
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function({data}) {
    const notificationOptions= {
        body: data.body,
        icon: data.icon,
        image: data.image
    };
    return self.registration.showNotification(data.title, notificationOptions);
});
