// config.js executed inside the iframe
instance.UI.addEventListener(instance.UI.Events.VIEWER_LOADED, () => {
    const { documentViewer, annotationManager } = instance.Core;

    // Add customization here
    documentViewer.setMargin(20);
    documentViewer.addEventListener('fitModeUpdated', fitMode => {

    });

    instance.UI.disableElement('menuButton');
    instance.UI.disableElement('selectToolButton');
    instance.UI.disableElement('ribbons');
    instance.UI.disableElement('searchButton');
    instance.UI.disableElement('toggleNotesButton');
    instance.UI.disableElement('toolsHeader');
    instance.UI.disableElement('annotationPopup');
    // instance.UI.setTheme('dark');
});
