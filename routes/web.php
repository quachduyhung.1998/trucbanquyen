<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::get('forgot-password', function () {
        return view('auth.forgot_password');
    })->name('password.request');
    Route::post('forgot-password', [\App\Http\Controllers\AuthController::class, 'requestResetPassword'])->name('password.email');
    Route::get('reset-password/{token}', function (string $token) {
        return view('auth.reset_password', ['token' => $token]);
    })->name('password.reset');
    Route::post('reset-password', [\App\Http\Controllers\AuthController::class, 'updatePassword'])->name('password.update');
});

Route::group(['prefix' => 'login'], function () {
    Route::get('', [\App\Http\Controllers\AuthController::class, 'formLogin'])->name('login');
    Route::post('', [\App\Http\Controllers\AuthController::class,'login']);
});

Route::get('logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('logout');

Route::get('viewer',function (){
    return view('pdftron_viewer');
})->name('pdftron_viewer');

Route::middleware(['auth'])->group(function () {
    Route::get('', [\App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard')
        ->middleware('permission:view_dashboard');

    Route::post('upload-file', [\App\Http\Controllers\FileController::class, 'upload'])->name('file.upload');

    Route::group(['prefix' => 'libraries'], function () {
        Route::post('store-object-copyright', [\App\Http\Controllers\LibraryController::class, 'storeObjectCopyright'])->name('library.storeObjectCopyright');
        Route::post('store', [\App\Http\Controllers\LibraryController::class, 'store'])->name('library.store');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::post('store', [\App\Http\Controllers\NotificationController::class, 'store'])->name('notification.store');
        Route::get('view', [\App\Http\Controllers\NotificationController::class, 'view'])->name('notification.view');
        Route::get('click/{id}', [\App\Http\Controllers\NotificationController::class, 'click'])->name('notification.click');
        Route::get('load-more', [\App\Http\Controllers\NotificationController::class, 'loadMore'])->name('notification.load_more');
        Route::post('/save-token', [\App\Http\Controllers\NotificationController::class, 'saveToken'])->name('notification.save_token');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('', [\App\Http\Controllers\AuthController::class, 'profile'])->name('profile');
        Route::put('change-profile', [\App\Http\Controllers\AuthController::class, 'changeProfile'])->name('profile.change_profile');
        Route::put('change-username', [\App\Http\Controllers\AuthController::class, 'changeUsername'])->name('profile.change_username');
        Route::put('change-password', [\App\Http\Controllers\AuthController::class, 'changePassword'])->name('profile.change_password');
    });

    Route::group(['prefix' => 'object'], function () {
        // Đối Tượng Bản Quyền
        Route::group([
            'prefix' => 'copyrights',
            'middleware' => ['permission:object_copyright.view']
        ], function () {
            Route::get('', [\App\Http\Controllers\ObjectCopyrightController::class, 'index'])
                ->name('object_copyright.index')
                ->middleware('permission:object_copyright.view');
            Route::get('{id}/edit', [\App\Http\Controllers\ObjectCopyrightController::class, 'edit'])
                ->name('object_copyright.edit')
                ->middleware('permission:object_copyright.edit');
            Route::post('{id}/update', [\App\Http\Controllers\ObjectCopyrightController::class, 'update'])
                ->name('object_copyright.update')
                ->middleware('permission:object_copyright.edit');
            Route::delete('delete', [\App\Http\Controllers\ObjectCopyrightController::class, 'delete'])
                ->name('object_copyright.delete')
                ->middleware('permission:object_copyright.delete');
        });

        // Đối Tượng Khuôn Mặt
        Route::group([
            'prefix' => 'faces',
            'middleware' => ['permission:object_face.view']
        ], function () {
            Route::get('', [\App\Http\Controllers\ObjectFaceController::class, 'index'])
                ->name('object_face.index')
                ->middleware('permission:object_face.view');
            Route::get('create', [\App\Http\Controllers\ObjectFaceController::class, 'create'])
                ->name('object_face.create')
                ->middleware('permission:object_face.create');
            Route::post('store', [\App\Http\Controllers\ObjectFaceController::class, 'store'])
                ->name('object_face.store')
                ->middleware('permission:object_face.create');
            Route::get('{id}/edit', [\App\Http\Controllers\ObjectFaceController::class, 'edit'])
                ->name('object_face.edit')
                ->middleware('permission:object_face.edit');
            Route::post('{id}/update', [\App\Http\Controllers\ObjectFaceController::class, 'update'])
                ->name('object_face.update')
                ->middleware('permission:object_face.edit');
            Route::delete('delete', [\App\Http\Controllers\ObjectFaceController::class, 'delete'])
                ->name('object_face.delete')
                ->middleware('permission:object_face.delete');
        });
    });

    // Danh sách phê duyệt
    Route::group([
        'prefix' => 'ai-approves',
        'middleware' => ['permission:ai_approve.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\AiApproveController::class, 'index'])
            ->name('ai_approve.index')
            ->middleware('permission:ai_approve.view');
        Route::post('store', [\App\Http\Controllers\AiApproveController::class, 'store'])
            ->name('ai_approve.store')
            ->middleware('permission:ai_approve.create');
        Route::post('request-ai-detect', [\App\Http\Controllers\AiApproveController::class, 'requestAiDetect'])
            ->name('ai_approve.request_ai_detect')
            ->middleware('permission:ai_approve.edit');
        Route::put('change-status', [\App\Http\Controllers\AiApproveController::class, 'changeStatus'])
            ->name('ai_approve.change_status')
            ->middleware('permission:ai_approve.edit');
        Route::get('{id}/edit', [\App\Http\Controllers\AiApproveController::class, 'edit'])
            ->name('ai_approve.edit')
            ->middleware('permission:ai_approve.edit');
        Route::post('{id}/update', [\App\Http\Controllers\AiApproveController::class, 'update'])
            ->name('ai_approve.update')
            ->middleware('permission:ai_approve.edit');
        Route::delete('delete', [\App\Http\Controllers\AiApproveController::class, 'delete'])
            ->name('ai_approve.delete')
            ->middleware('permission:ai_approve.delete');
    });

    // Lĩnh vực
    Route::get('fields', [\App\Http\Controllers\CategoryController::class, 'listField'])
        ->name('field.index')
        ->middleware('permission:category.view');
    // Loại hình
    Route::get('types', [\App\Http\Controllers\CategoryController::class, 'listType'])
        ->name('type.index')
        ->middleware('permission:category.view');
    Route::group([
        'prefix' => 'categories',
        'middleware' => ['permission:category.view']
    ], function () {
        Route::post('store', [\App\Http\Controllers\CategoryController::class, 'store'])
            ->name('category.store')
            ->middleware('permission:category.create');
        Route::get('edit/{id}', [\App\Http\Controllers\CategoryController::class, 'edit'])
            ->name('category.edit')
            ->middleware('permission:category.edit');
        Route::post('update', [\App\Http\Controllers\CategoryController::class, 'update'])
            ->name('category.update')
            ->middleware('permission:category.edit');
        Route::delete('delete', [\App\Http\Controllers\CategoryController::class, 'delete'])
            ->name('category.delete')
            ->middleware('permission:category.delete');
    });

    // gói thành viên
    Route::group([
        'prefix' => 'packages',
        'middleware' => ['permission:package.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\PackageController::class, 'index'])
            ->name('package.index')
            ->middleware('permission:package.view');
        Route::post('store', [\App\Http\Controllers\PackageController::class, 'store'])
            ->name('package.store')
            ->middleware('permission:package.create');
        Route::get('edit/{id}', [\App\Http\Controllers\PackageController::class, 'edit'])
            ->name('package.edit')
            ->middleware('permission:package.edit');
        Route::post('update', [\App\Http\Controllers\PackageController::class, 'update'])
            ->name('package.update')
            ->middleware('permission:package.edit');
        Route::delete('delete', [\App\Http\Controllers\PackageController::class, 'delete'])
            ->name('package.delete')
            ->middleware('permission:package.delete');
    });

    // tài khoản
    Route::group([
        'prefix' => 'users',
        'middleware' => ['permission:user.view|member.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\UserController::class, 'index'])
            ->name('user.index')
            ->middleware('permission:user.view');
        Route::post('store', [\App\Http\Controllers\UserController::class, 'store'])
            ->name('user.store')
            ->middleware('permission:user.create');
        Route::get('edit/{id}', [\App\Http\Controllers\UserController::class, 'edit'])
            ->name('user.edit')
            ->middleware('permission:user.edit');
        Route::post('update', [\App\Http\Controllers\UserController::class, 'update'])
            ->name('user.update')
            ->middleware('permission:user.edit');
        Route::put('change-status', [\App\Http\Controllers\UserController::class, 'changeStatus'])
            ->name('user.change_status')
            ->middleware('permission:user.edit|member.edit');
        Route::put('reset-password', [\App\Http\Controllers\UserController::class, 'resetPassword'])
            ->name('user.reset_password')
            ->middleware('permission:user.edit|member.edit');
        Route::delete('delete', [\App\Http\Controllers\UserController::class, 'delete'])
            ->name('user.delete')
            ->middleware('permission:user.delete|member.delete');
    });

    // thành viên
    Route::group([
        'prefix' => 'members',
        'middleware' => ['permission:member.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\UserController::class, 'member'])
            ->name('member.index')
            ->middleware('permission:member.view');
        Route::get('create', [\App\Http\Controllers\UserController::class, 'createMember'])
            ->name('member.create')
            ->middleware('permission:member.create');
        Route::post('store', [\App\Http\Controllers\UserController::class, 'storeMember'])
            ->name('member.store')
            ->middleware('permission:member.create');
        Route::get('{id}/edit', [\App\Http\Controllers\UserController::class, 'editMember'])
            ->name('member.edit')
            ->middleware('permission:member.edit|member.view');
        Route::post('{id}/update', [\App\Http\Controllers\UserController::class, 'updateMember'])
            ->name('member.update')
            ->middleware('permission:member.edit');
    });

    // Đăng ký bản quyền
    Route::group([
        'prefix' => 'copyright-registrations',
        'middleware' => ['permission:copyright_registration.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\CopyrightRegistrationController::class, 'index'])
            ->name('copyright_registration.index')
            ->middleware('permission:copyright_registration.view');
        Route::get('create', [\App\Http\Controllers\CopyrightRegistrationController::class, 'create'])
            ->name('copyright_registration.create')
            ->middleware('permission:copyright_registration.create');
        Route::post('store', [\App\Http\Controllers\CopyrightRegistrationController::class, 'store'])
            ->name('copyright_registration.store')
            ->middleware('permission:copyright_registration.create');
        Route::post('change-status', [\App\Http\Controllers\CopyrightRegistrationController::class, 'changeStatus'])
            ->name('copyright_registration.change_status')
            ->middleware('permission:copyright_registration.edit');
        Route::get('{id}/edit', [\App\Http\Controllers\CopyrightRegistrationController::class, 'edit'])
            ->name('copyright_registration.edit')
            ->middleware('permission:copyright_registration.edit');
        Route::post('{id}/update', [\App\Http\Controllers\CopyrightRegistrationController::class, 'update'])
            ->name('copyright_registration.update')
            ->middleware('permission:copyright_registration.edit');
    });

    // Bảo vệ bản quyền
    Route::group([
        'prefix' => 'copyright-protections',
        'middleware' => ['permission:copyright_protection.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\CopyrightProtectionController::class, 'index'])
            ->name('copyright_protection.index')
            ->middleware('permission:copyright_protection.view');
        Route::get('create', [\App\Http\Controllers\CopyrightProtectionController::class, 'create'])
            ->name('copyright_protection.create')
            ->middleware('permission:copyright_protection.create');
        Route::post('store', [\App\Http\Controllers\CopyrightProtectionController::class, 'store'])
            ->name('copyright_protection.store')
            ->middleware('permission:copyright_protection.create');
        Route::post('change-status', [\App\Http\Controllers\CopyrightProtectionController::class, 'changeStatus'])
            ->name('copyright_protection.change_status')
            ->middleware('permission:copyright_protection.edit');
        Route::get('{id}/edit', [\App\Http\Controllers\CopyrightProtectionController::class, 'edit'])
            ->name('copyright_protection.edit')
            ->middleware('permission:copyright_protection.edit');
        Route::post('{id}/update', [\App\Http\Controllers\CopyrightProtectionController::class, 'update'])
            ->name('copyright_protection.update')
            ->middleware('permission:copyright_protection.edit');
    });

    // Báo cáo vi phạm
    Route::group([
        'prefix' => 'copyright-reports',
        'middleware' => ['permission:copyright_report.view']
    ], function () {
        Route::get('', [\App\Http\Controllers\CopyrightReportController::class, 'index'])
            ->name('copyright_report.index')
            ->middleware('permission:copyright_report.view');
        Route::get('create', [\App\Http\Controllers\CopyrightReportController::class, 'create'])
            ->name('copyright_report.create')
            ->middleware('permission:copyright_report.create');
        Route::post('store', [\App\Http\Controllers\CopyrightReportController::class, 'store'])
            ->name('copyright_report.store')
            ->middleware('permission:copyright_report.create');
        Route::post('change-status', [\App\Http\Controllers\CopyrightReportController::class, 'changeStatus'])
            ->name('copyright_report.change_status')
            ->middleware('permission:copyright_report.edit');
        Route::get('{id}/edit', [\App\Http\Controllers\CopyrightReportController::class, 'edit'])
            ->name('copyright_report.edit')
            ->middleware('permission:copyright_report.edit');
        Route::post('{id}/update', [\App\Http\Controllers\CopyrightReportController::class, 'update'])
            ->name('copyright_report.update')
            ->middleware('permission:copyright_report.edit');
    });

    Route::group(['prefix' => 'histories'], function () {
        Route::get('', function () {
            return view('history.index');
        })->name('history.index');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('', function () {
            return view('setting.index');
        })->name('setting.index');
    });
});
