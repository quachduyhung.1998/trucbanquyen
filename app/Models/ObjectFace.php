<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectFace extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "name",
        "images",
        "position",
        "tag_alerts",
        "note",
    ];

    protected $casts = [
        'images' => 'array',
        'tag_alerts' => 'array',
    ];

    public static array $tagAlerts = [
        1 => 'Nhân vật có tầm ảnh hưởng',
        2 => 'Khủng bố',
        3 => 'Tội phạm quốc tế',
        4 => 'Chính trị gia',
        5 => 'Nhân vật quan trọng',
        6 => 'Phản động'
    ];
    public static array $tagAlertColors = [
        1 => 'primary',
        2 => 'danger',
        3 => 'warning',
        4 => 'success',
        5 => 'info',
        6 => 'dark'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function library(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Library::class);
    }

    public function aiApproval(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(AiApprove::class, 'model_id', 'id');
    }
}
