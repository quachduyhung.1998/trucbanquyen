<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AiApprove extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "name",
//        "url",
//        "type",
        "model_id",
        "model_type",
        "status_ai_detect",
        "status",
    ];

    const MODEL_TYPE_OBJECT_COPYRIGHT = 'App\Models\ObjectCopyright';
    const MODEL_TYPE_OBJECT_FACE = 'App\Models\ObjectFace';
    const MODEL_TYPE_LIBRARY = 'App\Models\Library';

    const TYPE_IMAGE = 'IMAGE';
    const TYPE_VIDEO = 'VIDEO';

    public static array $typeLabel = [
        self::TYPE_IMAGE => 'Ảnh',
        self::TYPE_VIDEO => 'Video'
    ];

    const STATUS_AI_DETECT_PENDING = 1;
    const STATUS_AI_DETECT_POSSIBLE_VIOLATION = 2;
    const STATUS_AI_DETECT_NOT_FOUND_VIOLATION = 3;
    public static array $statusAiDetectLabel = [
        self::STATUS_AI_DETECT_PENDING => 'Chờ xử lý',
        self::STATUS_AI_DETECT_POSSIBLE_VIOLATION => 'Có khả năng vi phạm',
        self::STATUS_AI_DETECT_NOT_FOUND_VIOLATION => 'Không tìm thấy vi phạm'
    ];
    public static array $statusAiDetectColor = [
        self::STATUS_AI_DETECT_PENDING => 'secondary',
        self::STATUS_AI_DETECT_POSSIBLE_VIOLATION => 'danger',
        self::STATUS_AI_DETECT_NOT_FOUND_VIOLATION => 'primary'
    ];

    const STATUS_PENDING = 1;
    const STATUS_APPROVAL = 2;
    const STATUS_NOT_APPROVAL = 3;
    public static array $statusLabel = [
        self::STATUS_PENDING => 'Chờ xử lý',
        self::STATUS_APPROVAL => 'Duyệt',
        self::STATUS_NOT_APPROVAL => 'Không duyệt'
    ];
    public static array $statusColor = [
        self::STATUS_PENDING => 'secondary',
        self::STATUS_APPROVAL => 'primary',
        self::STATUS_NOT_APPROVAL => 'danger'
    ];

    public function objectCopyright(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ObjectCopyright::class, 'model_id', 'id');
    }

    public function objectFace(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ObjectFace::class, 'model_id', 'id');
    }

    public function library(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Library::class, 'model_id', 'id');
    }
}
