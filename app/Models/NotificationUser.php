<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationUser extends Model
{
    use HasFactory, SoftDeletes;

    protected  $fillable = [
        "notification_id",
        "user_id",
        "to_owner",
        "is_new",
        "status",
    ];

    const IS_TO_OWNER = 1;
    const IS_NOT_TO_OWNER = 2;

    const IS_NEW = 1;
    const IS_OLD = 2;

    const STATUS_UNREAD = 1;
    const STATUS_READ = 2;

    public function notification(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Notification::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
