<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'type'
    ];

    const LINH_VUC = 'LINH_VUC';
    const LOAI_HINH = 'LOAI_HINH';
    public static array $typeLabel = [
        self::LINH_VUC => 'Lĩnh vực',
        self::LOAI_HINH => 'Loại hình'
    ];
}
