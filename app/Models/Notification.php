<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use HasFactory, SoftDeletes;

    protected  $fillable = [
        "user_id",
        "model_type",
        "model_id",
        "content",
        "status_content",
    ];

    const MODEL_TYPE_COPYRIGHT_REGISTRATION = 'App\Models\CopyrightRegistration';
    const MODEL_TYPE_COPYRIGHT_PROTECTION = 'App\Models\CopyrightProtection';
    const MODEL_TYPE_COPYRIGHT_REPORT = 'App\Models\CopyrightReport';
    public static array $modelTypeLabel = [
        self::MODEL_TYPE_COPYRIGHT_REGISTRATION => 'Đăng ký bản quyền',
        self::MODEL_TYPE_COPYRIGHT_PROTECTION => 'Bảo vệ bản quyền',
        self::MODEL_TYPE_COPYRIGHT_REPORT => 'Báo cáo vi phạm'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function copyrightRegistration(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CopyrightRegistration::class, 'model_id', 'id');
    }

    public function copyrightProtection(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CopyrightProtection::class, 'model_id', 'id');
    }

    public function copyrightReport(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CopyrightReport::class, 'model_id', 'id');
    }

    public function notificationUsers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(NotificationUser::class);
    }
}
