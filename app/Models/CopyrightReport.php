<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CopyrightReport extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "object",
        "type_object",
        "field_id",
        "description",
        "url_contents",
        "subject_name",
        "subject_phone",
        "subject_email",
        "subject_website",
        "subject_address",
        "violation_from_date",
        "violation_to_date",
        "status",
    ];

    protected $casts = [
        'url_contents' => 'array',
    ];

    const TYPE_OBJECT_IN_COPYRIGHT_PROTECTED = 1;
    const TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED = 2;
    public static array $typeObjectLabel = [
        self::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED => 'Trong danh sách bảo vệ bản quyền',
        self::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED => 'Ngoài danh sách bảo vệ bản quyền'
    ];

    const STATUS_PENDING = 1;
    const STATUS_BEING_DEPLOYMENT = 2;
    const STATUS_NEED_FURTHER_IMPROVEMENT = 3;
    const STATUS_REJECT = 4;
    const STATUS_COMPLETE = 5;
    const STATUS_CANCEL = 6;
    public static array $statusLabel = [
        self::STATUS_PENDING => 'Chờ xử lý',
        self::STATUS_BEING_DEPLOYMENT => 'Đang triển khai',
        self::STATUS_NEED_FURTHER_IMPROVEMENT => 'Cần hoàn thiện hồ sơ',
        self::STATUS_REJECT => 'Từ chối',
        self::STATUS_COMPLETE => 'Hoàn thành',
        self::STATUS_CANCEL => 'Hủy bỏ'
    ];
    public static array $statusColor = [
        self::STATUS_PENDING => 'secondary',
        self::STATUS_BEING_DEPLOYMENT => 'success',
        self::STATUS_NEED_FURTHER_IMPROVEMENT => 'info',
        self::STATUS_REJECT => 'danger',
        self::STATUS_COMPLETE => 'primary',
        self::STATUS_CANCEL => 'dark'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function copyrightProtected(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(CopyrightProtection::class, 'object', 'id');
    }

    public function field(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'field_id', 'id')
            ->where('type', Category::LINH_VUC);
    }
}
