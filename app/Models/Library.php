<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Library extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "title",
        "type",
        "file_id",
        "url",
        "thumb",
        "size",
        "file_extension",
        "video_duration",
        "author",
        "data_similar",
        "field_id",
    ];

    protected $casts = [
        'data_similar' => 'json'
    ];

    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;

    public static array $typeLabel = [
        self::TYPE_IMAGE => 'Ảnh',
        self::TYPE_VIDEO => 'Video'
    ];

    public function objectCopyright(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ObjectCopyright::class);
    }

    public function field(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'field_id', 'id');
    }
}
