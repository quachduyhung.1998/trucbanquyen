<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CopyrightProtection extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "name",
        "type_id",
        "field_id",
        "description",
        "url_content_protection",
        "url_copyright_registrations",
        "certification_number",
        "author_name",
        "author_cccd",
        "author_address",
        "author_district",
        "author_city",
        "author_country",
        "owner_name",
        "owner_cccd",
        "owner_address",
        "owner_district",
        "owner_city",
        "owner_country",
        "status_ai_detect",
        "status",
    ];

    protected $casts = [
        'url_copyright_registrations' => 'array',
    ];

    const STATUS_AI_DETECT_PENDING = 1;
    const STATUS_AI_DETECT_POSSIBLE_VIOLATION = 2;
    const STATUS_AI_DETECT_NOT_FOUND_VIOLATION = 3;
    public static array $statusAiDetectLabel = [
        self::STATUS_AI_DETECT_PENDING => 'Chờ xử lý',
        self::STATUS_AI_DETECT_POSSIBLE_VIOLATION => 'Có khả năng vi phạm',
        self::STATUS_AI_DETECT_NOT_FOUND_VIOLATION => 'Không tìm thấy vi phạm'
    ];
    public static array $statusAiDetectColor = [
        self::STATUS_AI_DETECT_PENDING => 'secondary',
        self::STATUS_AI_DETECT_POSSIBLE_VIOLATION => 'danger',
        self::STATUS_AI_DETECT_NOT_FOUND_VIOLATION => 'primary'
    ];

    const STATUS_PENDING = 1;
    const STATUS_BEING_DEPLOYMENT = 2;
    const STATUS_NEED_FURTHER_IMPROVEMENT = 3;
    const STATUS_REJECT = 4;
    const STATUS_COMPLETE = 5;
    const STATUS_CANCEL = 6;
    public static array $statusLabel = [
        self::STATUS_PENDING => 'Chờ xử lý',
        self::STATUS_BEING_DEPLOYMENT => 'Đang triển khai',
        self::STATUS_NEED_FURTHER_IMPROVEMENT => 'Cần hoàn thiện hồ sơ',
        self::STATUS_REJECT => 'Từ chối',
        self::STATUS_COMPLETE => 'Hoàn thành',
        self::STATUS_CANCEL => 'Hủy bỏ'
    ];
    public static array $statusColor = [
        self::STATUS_PENDING => 'secondary',
        self::STATUS_BEING_DEPLOYMENT => 'success',
        self::STATUS_NEED_FURTHER_IMPROVEMENT => 'info',
        self::STATUS_REJECT => 'danger',
        self::STATUS_COMPLETE => 'primary',
        self::STATUS_CANCEL => 'dark'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'type_id', 'id')
            ->where('type', Category::LOAI_HINH);
    }

    public function field(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'field_id', 'id')
            ->where('type', Category::LINH_VUC);
    }
}
