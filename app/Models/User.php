<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "user_id",
        "username",
        "full_name",
        "email",
        "phone",
        "avatar",
        "role",
        "package_id",
        "status",
        "type_user",
        "paid",
        "fax",
        "unit_name",
        "unit_address",
        "operating_license",
        "business_license",
        "business_registration_number",
        "business_registration_address",
        "business_registration_date_issuance",
        "legal_entity_representative",
        "logo_company",
        "content_type",
        "nickname",
        "sex",
        "birthday",
        "job",
        "permanent_address",
        "contact_address",
        "cccd_number",
        "cccd_place",
        "cccd_date",
        "cccd_front_photo",
        "cccd_back_photo",
        "face_photo",
        "password",
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'business_registration_date_issuance' => 'date',
        'content_type' => 'array',
        'cccd_date' => 'date',
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_ADMIN = 2;
    const ROLE_SALE = 3;
    const ROLE_CSKH = 4;
    const ROLE_PHAP_LY = 5;
    const ROLE_MEMBER = 6;
    const ROLE_CTV = 7; // cộng tác viên
    public static array $roleLabel = [
        self::ROLE_ADMIN => 'Admin',
        self::ROLE_SALE => 'Sale',
        self::ROLE_CSKH => 'CSKH',
        self::ROLE_PHAP_LY => 'Pháp lý',
        self::ROLE_MEMBER => 'Thành viên',
        self::ROLE_CTV => 'Cộng tác viên'
    ];
    public static array $roleColor = [
        self::ROLE_ADMIN => 'danger',
        self::ROLE_SALE => 'warning',
        self::ROLE_CSKH => 'info',
        self::ROLE_PHAP_LY => 'primary',
        self::ROLE_MEMBER => 'success',
        self::ROLE_CTV => 'dark'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    public static array $statusLabel = [
        self::STATUS_ACTIVE => 'Đang hoạt động',
        self::STATUS_INACTIVE => 'Không hoạt động'
    ];

    const TYPE_USER_TO_CHUC = 'TO_CHUC';
    const TYPE_USER_CA_NHAN = 'CA_NHAN';
    public static array $typeUserLabel = [
        self::TYPE_USER_TO_CHUC => 'Tổ chức',
        self::TYPE_USER_CA_NHAN => 'Cá nhân'
    ];

    const PAID = 1;
    const UNPAID = 0;
    public static array $paymentStatusLabel = [
        self::PAID => 'Đã thanh toán',
        self::UNPAID => 'Chưa thanh toán'
    ];

    const FEMALE = 0;
    const MALE = 1;
    public static array $sexLabel = [
        self::MALE => 'Nam',
        self::FEMALE => 'Nữ'
    ];
}
