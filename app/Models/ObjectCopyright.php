<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectCopyright extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "name",
        "author",
//        "url",
//        "type",
        "library_id"
    ];

    const TYPE_IMAGE = 'IMAGE';
    const TYPE_VIDEO = 'VIDEO';

    public static array $typeLabel = [
        self::TYPE_IMAGE => 'Ảnh',
        self::TYPE_VIDEO => 'Video'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function library(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Library::class);
    }

    public function aiApproval(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(AiApprove::class, 'model_id', 'id');
    }
}
