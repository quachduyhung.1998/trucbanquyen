<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notification\CreateNotificationRequest;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\UserFirebaseToken;
use App\Services\Notification\NotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct(protected NotificationService $notificationService) {}

    public function store(CreateNotificationRequest $request)
    {
        $userId = auth()->user()->id;
        $created = $this->notificationService->create($userId, $request->model_type, $request->model_id, $request->notification_content);
        if ($created) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function view()
    {
        NotificationUser::query()->where('user_id', auth()->user()->id)->update(['is_new' => NotificationUser::IS_OLD]);
    }

    public function click($id)
    {
        $notification = Notification::find($id);
        if ($notification) {
            $notificationUser = NotificationUser::query()
                ->where('notification_id', $notification->id)
                ->where('user_id', auth()->user()->id)
                ->first();
            if ($notificationUser && $notificationUser->status === NotificationUser::STATUS_UNREAD) {
                $notificationUser->is_new = NotificationUser::IS_OLD;
                $notificationUser->status = NotificationUser::STATUS_READ;
                $notificationUser->save();
            }
            if ($notification->model_type === Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION) {
                $routeName = 'copyright_registration.edit';
            } elseif ($notification->model_type === Notification::MODEL_TYPE_COPYRIGHT_PROTECTION) {
                $routeName = 'copyright_protection.edit';
            } else {
                $routeName = 'copyright_report.edit';
            }
            return redirect()->route($routeName, ['id' => $notification->model_id, 'notification_access' => $notification->id]);
        }
        abort(404);
    }

    public function loadMore(Request $request)
    {
        $limit = 10;
        $offset = ($request->page - 1) * $limit;

        $notificationIds = NotificationUser::query()
            ->where('user_id', auth()->user()->id)
            ->pluck('notification_id')
            ->toArray();
        $notifications = Notification::with([
                'user',
                'copyrightRegistration',
                'copyrightProtection',
                'copyrightReport',
                'notificationUsers' => function ($q) {
                    $q->where('user_id', auth()->user()->id);
                }
            ])
            ->where('id', '<=', $request->first_notification_id)
            ->whereIn('id', $notificationIds)
            ->latest('id')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return view('layouts.notification_load_more', compact('notifications'));
    }

    public function saveToken(Request $request)
    {
        $checkExists = UserFirebaseToken::query()
            ->where('token', $request->fcm_token)
            ->first();
        if ($checkExists) {
            if ($checkExists->user_id !== auth()->user()->id) {
                $checkExists->user_id = auth()->user()->id;
                $checkExists->save();
            }
        } else {
            UserFirebaseToken::create([
                'user_id' => auth()->user()->id,
                'token' => $request->fcm_token,
            ]);
        }
    }
}
