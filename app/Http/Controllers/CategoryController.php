<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function listField(Request $request)
    {
        $type = Category::LINH_VUC;
        $categories = Category::query()
            ->where('type', Category::LINH_VUC)
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('category.index', compact('categories', 'type'));
    }

    public function listType(Request $request)
    {
        $type = Category::LOAI_HINH;
        $categories = Category::query()
            ->where('type', Category::LOAI_HINH)
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('category.index', compact('categories', 'type'));
    }

    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->except('_token'));
        if ($category) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id, Request $request)
    {
        $category = Category::find($id);
        if (!$category) abort(404);
        return view('category.modal_update', compact('category'));
    }

    public function update(CategoryRequest $request)
    {
        $category = Category::find($request->id);
        if ($category) {
            $category->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $category = Category::find($request->id);
        if ($category) {
            $category->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);

    }
}
