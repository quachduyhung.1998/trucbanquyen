<?php

namespace App\Http\Controllers;

use App\Http\Requests\ObjectCopyright\UpdateObjectCopyrightRequest;
use App\Models\Category;
use App\Models\ObjectCopyright;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ObjectCopyrightController extends Controller
{
    public function index(Request $request)
    {
        $userIds = ObjectCopyright::query()
            ->groupBy('user_id')
            ->pluck('user_id')
            ->toArray();
        $users = User::query()
            ->whereIn('id', $userIds)
            ->pluck('full_name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $objectCopyrights = ObjectCopyright::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('author', 'like', "%$request->keyword%");
            })
            ->whereHas('library', function($q) use ($request) {
                $q->when($request->type, function ($q1) use ($request) {
                    $q1->where('type', $request->type);
                });
                $q->when($request->field_id, function ($q1) use ($request) {
                    $q1->where('field_id', $request->field_id);
                });
            })
            ->when($request->user_id, function ($q) use ($request) {
                $q->where('user_id', $request->user_id);
            })
            ->latest('id')
            ->paginate(10);
        return view('object_copyright.index', compact('objectCopyrights', 'users', 'fields'));
    }

    public function edit($id)
    {
        $objectCopyright = ObjectCopyright::find($id);
        if (!$objectCopyright) abort(404);
        return view('object_copyright.edit', compact('objectCopyright'));
    }

    public function update($id, UpdateObjectCopyrightRequest $request)
    {
        $objectCopyright = ObjectCopyright::find($id);
        if ($objectCopyright) {
            $objectCopyright->update($request->except('_token'));
            return redirect()->route('object_copyright.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function delete(Request $request)
    {
        $objectCopyright = ObjectCopyright::find($request->id);
        if ($objectCopyright) {
            if ($objectCopyright->library) {
                if ($objectCopyright->library->file_id) {
                    Http::asForm()->post(config('default.base_url_server_upload') . '/api/v1/video/delete', [
                        'video_id' => $objectCopyright->library->file_id
                    ]);
                }
                $objectCopyright->library->delete();
            }
            $objectCopyright->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
