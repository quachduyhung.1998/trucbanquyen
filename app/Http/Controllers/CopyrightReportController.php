<?php

namespace App\Http\Controllers;

use App\Http\Requests\CopyrightReport\ChangeStatusCopyrightReportRequest;
use App\Http\Requests\CopyrightReport\CreateCopyrightReportRequest;
use App\Http\Requests\CopyrightReport\UpdateCopyrightReportRequest;
use App\Models\Category;
use App\Models\CopyrightProtection;
use App\Models\CopyrightReport;
use App\Models\Notification;
use App\Models\User;
use App\Services\Notification\NotificationService;
use Illuminate\Http\Request;

class CopyrightReportController extends Controller
{
    public function __construct(protected NotificationService $notificationService) {}

    public function index(Request $request)
    {
        $copyrightReports = CopyrightReport::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('object', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%")
                    ->orWhere('subject_name', 'like', "%$request->keyword%")
                    ->orWhere(function($q1) use ($request) {
                        $q1->whereHas('copyrightProtected', function($q2) use ($request) {
                            $q2->where('name', 'like', "%$request->keyword%");
                        });
                    });
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            });

        if (auth()->user()->role === User::ROLE_MEMBER) {
            $copyrightReports->where('user_id', auth()->user()->id);
        }

        $copyrightReports = $copyrightReports->latest('id')->paginate(10);
        return view('copyright_report.index', compact('copyrightReports'));
    }

    public function create()
    {
        $copyrightProtects = CopyrightProtection::query()
            ->where('status', CopyrightProtection::STATUS_COMPLETE)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        return view('copyright_report.create', compact('copyrightProtects', 'fields'));
    }

    public function store(CreateCopyrightReportRequest $request)
    {
        $data = $request->except('_token', 'object_in', 'object_out');
        $data['object'] = $request->object_in;
        if ($request->type_object == CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED) {
            $data['object'] = $request->object_out;
        }
        $copyrightReport = CopyrightReport::create($data);
        if ($copyrightReport) {
            return redirect()->route('copyright_report.index')->with('success', __('Thêm đối tượng thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function edit($id)
    {
        $copyrightReport = CopyrightReport::find($id);
        if (!$copyrightReport) abort(404);
        $copyrightProtects = CopyrightProtection::query()
            ->where('status', CopyrightProtection::STATUS_COMPLETE)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $notifications = Notification::with('user')
            ->where('model_type', Notification::MODEL_TYPE_COPYRIGHT_REPORT)
            ->where('model_id', $id)
            ->latest('id')
            ->get();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        return view('copyright_report.edit', compact('copyrightReport', 'copyrightProtects', 'notifications', 'fields'));
    }

    public function update($id, UpdateCopyrightReportRequest $request)
    {
        $data = $request->except('_token', 'object_in', 'object_out');
        $data['object'] = $request->object_in;
        if ($request->type_object == CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED) {
            $data['object'] = $request->object_out;
        }
        $copyrightReport = CopyrightReport::find($id);
        if ($copyrightReport) {
            $copyrightReport->update($data);
            return redirect()->route('copyright_report.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function changeStatus(ChangeStatusCopyrightReportRequest $request)
    {
        $copyrightReport = CopyrightReport::find($request->id);
        if ($copyrightReport) {
            $copyrightReport->status = $request->status;
            $copyrightReport->save();

            // notification
            $this->notificationService->create(
                auth()->user()->id,
                Notification::MODEL_TYPE_COPYRIGHT_REPORT,
                $copyrightReport->id,
                $request->notification,
                $request->status
            );

            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
