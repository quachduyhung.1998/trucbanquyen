<?php

namespace App\Http\Controllers;

use App\Http\Requests\Package\CreatePackageRequest;
use App\Http\Requests\Package\UpdatePackageRequest;
use App\Models\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index(Request $request)
    {
        $packages = Package::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%");
            })
            ->latest('id')
            ->paginate(10);
        return view('package.index', compact('packages'));
    }

    public function store(CreatePackageRequest $request)
    {
        $package = Package::create($request->except('_token'));
        if ($package) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $package = Package::find($id);
        if (!$package) abort(404);
        return view('package.modal_update', compact('package'));
    }

    public function update(UpdatePackageRequest $request)
    {
        $package = Package::find($request->id);
        if ($package) {
            $package->update($request->except('_token'));
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function delete(Request $request)
    {
        $package = Package::find($request->id);
        if ($package) {
            $package->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);

    }
}
