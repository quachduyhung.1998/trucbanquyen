<?php

namespace App\Http\Controllers;

use App\Models\AiApprove;
use App\Models\CopyrightProtection;
use App\Models\CopyrightRegistration;
use App\Models\CopyrightReport;
use App\Models\Library;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $reportCopyrightRegistration = [
            'pending' => 0,
            'being_development' => 0,
            'further_improvement' => 0,
            'reject' => 0,
            'complete' => 0,
        ];
        $copyrightRegistrations = CopyrightRegistration::query()
            ->when(auth()->user()->role === User::ROLE_MEMBER, function($q) {
                $q->where('user_id', auth()->user()->id);
            })->get();
        foreach ($copyrightRegistrations as $copyrightRegistration) {
            if ($copyrightRegistration->status === CopyrightRegistration::STATUS_PENDING) {
                $reportCopyrightRegistration['pending'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightRegistration::STATUS_BEING_DEPLOYMENT) {
                $reportCopyrightRegistration['being_development'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightRegistration::STATUS_NEED_FURTHER_IMPROVEMENT) {
                $reportCopyrightRegistration['further_improvement'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightRegistration::STATUS_REJECT) {
                $reportCopyrightRegistration['reject'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightRegistration::STATUS_COMPLETE) {
                $reportCopyrightRegistration['complete'] += 1;
            }
        }

        $reportCopyrightProtection = [
            'pending' => 0,
            'being_development' => 0,
            'further_improvement' => 0,
            'reject' => 0,
            'complete' => 0,
        ];
        $copyrightRegistrations = CopyrightProtection::query()
            ->when(auth()->user()->role === User::ROLE_MEMBER, function($q) {
                $q->where('user_id', auth()->user()->id);
            })->get();
        foreach ($copyrightRegistrations as $copyrightRegistration) {
            if ($copyrightRegistration->status === CopyrightProtection::STATUS_PENDING) {
                $reportCopyrightProtection['pending'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightProtection::STATUS_BEING_DEPLOYMENT) {
                $reportCopyrightProtection['being_development'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightProtection::STATUS_NEED_FURTHER_IMPROVEMENT) {
                $reportCopyrightProtection['further_improvement'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightProtection::STATUS_REJECT) {
                $reportCopyrightProtection['reject'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightProtection::STATUS_COMPLETE) {
                $reportCopyrightProtection['complete'] += 1;
            }
        }

        $reportCopyrightReport = [
            'pending' => 0,
            'being_development' => 0,
            'further_improvement' => 0,
            'reject' => 0,
            'complete' => 0,
        ];
        $copyrightRegistrations = CopyrightReport::query()
            ->when(auth()->user()->role === User::ROLE_MEMBER, function($q) {
                $q->where('user_id', auth()->user()->id);
            })->get();
        foreach ($copyrightRegistrations as $copyrightRegistration) {
            if ($copyrightRegistration->status === CopyrightReport::STATUS_PENDING) {
                $reportCopyrightReport['pending'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightReport::STATUS_BEING_DEPLOYMENT) {
                $reportCopyrightReport['being_development'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightReport::STATUS_NEED_FURTHER_IMPROVEMENT) {
                $reportCopyrightReport['further_improvement'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightReport::STATUS_REJECT) {
                $reportCopyrightReport['reject'] += 1;
            } elseif ($copyrightRegistration->status === CopyrightReport::STATUS_COMPLETE) {
                $reportCopyrightReport['complete'] += 1;
            }
        }

        $totalVideoUploaded = Library::query()->where('type', Library::TYPE_VIDEO)->count();
        $videoUploads = Library::select(
                DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as date"),
                DB::raw('COUNT(*) as "total_video"')
            )
            ->where('type', Library::TYPE_VIDEO)
            ->whereDate('created_at', '>=', Carbon::today()->subDays(30))
            ->whereDate('created_at', '<=', date('Y-m-d'))
            ->groupBy('date')
            ->orderBy('date', 'DESC')
            ->get()
            ->toArray();
        $dates = array_column($videoUploads, 'date');

        $videoUploadedLast7daysStatistics = [];
        for ($i = 6; $i >= 0; $i--) {
            $date = Carbon::today()->subDays($i)->format('d-m-Y');
            $item = [
                'date' => $date,
                'total_video' => 0
            ];
            $searchKey = array_search($date, $dates);

            if ($searchKey !== false && isset($videoUploads[$searchKey])) {
                $item = [
                    'date' => $date,
                    'total_video' => $videoUploads[$searchKey]['total_video']
                ];
            }
            $videoUploadedLast7daysStatistics[] = $item;
        }

        $videoUploadedLast30daysStatistics = [];
        for ($i = 29; $i >= 0; $i--) {
            $date = Carbon::today()->subDays($i)->format('d-m-Y');
            $item = [
                'date' => $date,
                'total_video' => 0
            ];
            $searchKey = array_search($date, $dates);

            if ($searchKey !== false && isset($videoUploads[$searchKey])) {
                $item = [
                    'date' => $date,
                    'total_video' => $videoUploads[$searchKey]['total_video']
                ];
            }
            $videoUploadedLast30daysStatistics[] = $item;
        }

        $totalVideoViolated = AiApprove::query()->where('status_ai_detect', AiApprove::STATUS_AI_DETECT_POSSIBLE_VIOLATION)->count();
        $videoViolates = AiApprove::select(
                DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as date"),
                DB::raw('COUNT(*) as "total_video"')
            )
            ->where('status_ai_detect', AiApprove::STATUS_AI_DETECT_POSSIBLE_VIOLATION)
            ->whereDate('created_at', '>=', Carbon::today()->subDays(30))
            ->whereDate('created_at', '<=', date('Y-m-d'))
            ->groupBy('date')
            ->orderBy('date', 'DESC')
            ->get()
            ->toArray();
        $dates = array_column($videoViolates, 'date');

        $videoViolateLast7daysStatistics = [];
        for ($i = 6; $i >= 0; $i--) {
            $date = Carbon::today()->subDays($i)->format('d-m-Y');
            $item = [
                'date' => $date,
                'total_video' => 0
            ];
            $searchKey = array_search($date, $dates);

            if ($searchKey !== false && isset($videoViolates[$searchKey])) {
                $item = [
                    'date' => $date,
                    'total_video' => $videoViolates[$searchKey]['total_video']
                ];
            }
            $videoViolateLast7daysStatistics[] = $item;
        }

        $videoViolateLast30daysStatistics = [];
        for ($i = 29; $i >= 0; $i--) {
            $date = Carbon::today()->subDays($i)->format('d-m-Y');
            $item = [
                'date' => $date,
                'total_video' => 0
            ];
            $searchKey = array_search($date, $dates);

            if ($searchKey !== false && isset($videoViolates[$searchKey])) {
                $item = [
                    'date' => $date,
                    'total_video' => $videoViolates[$searchKey]['total_video']
                ];
            }
            $videoViolateLast30daysStatistics[] = $item;
        }

        return view('dashboard', compact(
            'reportCopyrightRegistration',
            'reportCopyrightProtection',
            'reportCopyrightReport',
            'totalVideoUploaded',
            'videoUploadedLast7daysStatistics',
            'videoUploadedLast30daysStatistics',
            'totalVideoViolated',
            'videoViolateLast7daysStatistics',
            'videoViolateLast30daysStatistics'
        ));
    }
}
