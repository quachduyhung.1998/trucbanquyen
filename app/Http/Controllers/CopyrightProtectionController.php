<?php

namespace App\Http\Controllers;

use App\Http\Requests\CopyrightProtection\ChangeStatusCopyrightProtectionRequest;
use App\Http\Requests\CopyrightProtection\CreateCopyrightProtectionRequest;
use App\Http\Requests\CopyrightProtection\UpdateCopyrightProtectionRequest;
use App\Models\Category;
use App\Models\CopyrightProtection;
use App\Models\Notification;
use App\Models\User;
use App\Services\Notification\NotificationService;
use Illuminate\Http\Request;

class CopyrightProtectionController extends Controller
{
    public function __construct(protected NotificationService $notificationService) {}

    public function index(Request $request)
    {
        $copyrightProtections = CopyrightProtection::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%")
                    ->orWhere('author_name', 'like', "%$request->keyword%")
                    ->orWhere('owner_name', 'like', "%$request->keyword%");
            })
            ->when($request->type_id, function ($q) use ($request) {
                $q->where('type_id', $request->type_id);
            })
            ->when($request->field_id, function ($q) use ($request) {
                $q->where('field_id', $request->field_id);
            })
            ->when($request->status_ai_detect, function ($q) use ($request) {
                $q->where('status_ai_detect', $request->status_ai_detect);
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            });

        if (auth()->user()->role === User::ROLE_MEMBER) {
            $copyrightProtections->where('user_id', auth()->user()->id);
        }

        $copyrightProtections = $copyrightProtections->latest('id')->paginate(10);
        return view('copyright_protection.index', compact('copyrightProtections'));
    }

    public function create()
    {
        $types = Category::query()
            ->where('type', Category::LOAI_HINH)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        return view('copyright_protection.create', compact('types', 'fields'));
    }

    public function store(CreateCopyrightProtectionRequest $request)
    {
        $copyrightProtection = CopyrightProtection::create($request->except('_token'));
        if ($copyrightProtection) {
            return redirect()->route('copyright_protection.index')->with('success', __('Thêm đối tượng thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function edit($id)
    {
        $copyrightProtection = CopyrightProtection::find($id);
        if (!$copyrightProtection) abort(404);
        $types = Category::query()
            ->where('type', Category::LOAI_HINH)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $notifications = Notification::with('user')
            ->where('model_type', Notification::MODEL_TYPE_COPYRIGHT_PROTECTION)
            ->where('model_id', $id)
            ->latest('id')
            ->get();
        return view('copyright_protection.edit', compact('copyrightProtection', 'types', 'fields', 'notifications'));
    }

    public function update($id, UpdateCopyrightProtectionRequest $request)
    {
        $copyrightProtection = CopyrightProtection::find($id);
        if ($copyrightProtection) {
            $copyrightProtection->update($request->except('_token'));
            return redirect()->route('copyright_protection.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function changeStatus(ChangeStatusCopyrightProtectionRequest $request)
    {
        $copyrightProtection = CopyrightProtection::find($request->id);
        if ($copyrightProtection) {
            $copyrightProtection->status = $request->status;
            $copyrightProtection->save();

            // notification
            $this->notificationService->create(
                auth()->user()->id,
                Notification::MODEL_TYPE_COPYRIGHT_PROTECTION,
                $copyrightProtection->id,
                $request->notification,
                $request->status
            );

            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
