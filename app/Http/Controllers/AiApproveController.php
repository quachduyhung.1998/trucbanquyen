<?php

namespace App\Http\Controllers;

use App\Http\Requests\AiApprove\CreateAiApproveRequest;
use App\Models\AiApprove;
use App\Models\Category;
use App\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AiApproveController extends Controller
{
    public function index(Request $request)
    {
        $aiApproves = AiApprove::with(['objectCopyright.library', 'objectFace.library', 'library'])
            ->where(function ($q) use ($request) {
                $q->whereHas('objectCopyright', function($q1) use ($request) {
                    $q1->where('ai_approves.model_type', AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                        ->when($request->keyword, function ($q) use ($request) {
                            $q->where('name', 'like', "%$request->keyword%");
                        })->whereHas('library', function($q2) use ($request) {
                            $q2->when($request->type, function ($q3) use ($request) {
                                $q3->where('type', $request->type);
                            })
                            ->when($request->field_id, function ($q3) use ($request) {
                                $q3->where('field_id', $request->field_id);
                            });
                        });
                })
//                ->orWhereHas('objectFace', function($q1) use ($request) {
//                    $q1->where('ai_approves.model_type', AiApprove::MODEL_TYPE_OBJECT_FACE)
//                        ->when($request->keyword, function ($q) use ($request) {
//                            $q->where('name', 'like', "%$request->keyword%");
//                        })->whereHas('library', function($q2) use ($request) {
//                            $q2->when($request->type, function ($q3) use ($request) {
//                                $q3->where('type', $request->type);
//                            });
//                            $q2->when($request->field_id, function ($q3) use ($request) {
//                                $q3->where('field_id', $request->field_id);
//                            });
//                        });
//                })
                ->orWhereHas('library', function($q1) use ($request) {
                    $q1->where('ai_approves.model_type', AiApprove::MODEL_TYPE_LIBRARY)
                        ->when($request->keyword, function ($q) use ($request) {
                            $q->where('title', 'like', "%$request->keyword%");
                        })
                        ->when($request->type, function ($q2) use ($request) {
                            $q2->where('type', $request->type);
                        })
                        ->when($request->field_id, function ($q2) use ($request) {
                            $q2->where('field_id', $request->field_id);
                        });
                });
            })
            ->when($request->status_ai_detect, function ($q) use ($request) {
                $q->where('status_ai_detect', $request->status_ai_detect);
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            })
            ->latest('id')
            ->paginate(10);
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        return view('ai_approve.index', compact('aiApproves', 'fields'));
    }

    public function store(CreateAiApproveRequest $request)
    {
        $name = explode('.', $request->name)[0];
        $type = str_contains($request->type, 'image') ? AiApprove::TYPE_IMAGE : AiApprove::TYPE_VIDEO;
        $aiApprove = AiApprove::create([
            'name' => $name,
            'url' => $request->url,
            'type' => $type,
        ]);
        if ($aiApprove) {
            return response()->json(['status' => true, 'aiApprove' => $aiApprove]);
        }
        return response()->json(['status' => false]);
    }

    public function edit($id)
    {
        $aiApprove = AiApprove::find($id);
        if (!$aiApprove) abort(404);
        return view('ai_approve.edit', compact('aiApprove'));
    }

    public function update($id, Request $request)
    {
        $aiApprove = AiApprove::find($id);
        if ($aiApprove) {
            $aiApprove->update($request->except('_token'));
            return redirect()->route('ai_approve.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));

    }

    public function delete(Request $request)
    {
        $aiApprove = AiApprove::find($request->id);
        if ($aiApprove) {
            $libraryId = null;
            if ($aiApprove->model_type === AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT) {
                $libraryId = $aiApprove->objectCopyright?->library?->file_id;
                if ($aiApprove->objectCopyright?->library) $aiApprove->objectCopyright?->library->delete();
                if ($aiApprove->objectCopyright) $aiApprove->objectCopyright->delete();
            }
//            elseif ($aiApprove->model_type === AiApprove::MODEL_TYPE_OBJECT_FACE)
//                $libraryId = $aiApprove->objectFace?->library?->file_id;
            elseif ($aiApprove->model_type === AiApprove::MODEL_TYPE_LIBRARY) {
                $libraryId = $aiApprove->library?->file_id;
                if ($aiApprove->library) $aiApprove->library->delete();
            }

            $aiApprove->delete();
            if ($libraryId) {
                Http::asForm()->post(config('default.base_url_server_upload') . '/api/v1/video/delete', [
                    'video_id' => $libraryId
                ]);
            }
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function changeStatus(Request $request)
    {
        $aiApprove = AiApprove::find($request->id);
        if ($aiApprove) {
            $aiApprove->status = $request->status;
            $aiApprove->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function requestAiDetect(Request $request)
    {
        if (!$request->file_id) return response()->json(['status' => false]);

        $response = Http::asForm()->post(config('default.base_url_server_upload') . '/api/v1/video/search', [
            'video_id' => $request->file_id
        ]);
        if ($response->successful()) {
            $res = $response->json();
            if ($res['data']) {
                if (!empty($res['data']['id'])) {
                    $library = Library::query()->where('file_id', $res['data']['id'])->first();
                    if ($library) {
                        $library->title = $res['data']['title'];
                        $library->url = $res['data']['link'];
                        $library->thumb = $res['data']['thumbnail'];
                        $library->video_duration = $res['data']['duration'];
                        if (!empty($res['data']['similary'])) {
                            $library->data_similar = $res['data']['similary'];
                        }
                        $library->save();

                        $status = AiApprove::STATUS_AI_DETECT_NOT_FOUND_VIOLATION;
                        if (!empty($res['data']['similary'])) {
                            $status = AiApprove::STATUS_AI_DETECT_POSSIBLE_VIOLATION;
                        }
                        AiApprove::query()
                            ->where('model_type', AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                            ->where('model_id', $library->objectCopyright?->id)
                            ->update(['status_ai_detect' => $status]);
                    }
                    return response()->json(['status' => true]);
                }
            }
        }
        return response()->json(['status' => false]);
    }
}
