<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        if (!$request->file) return response()->json(['status' => false]);

        if (str_contains($request->file->getMimeType(), 'video')) {
            $response = Http::attach('file', file_get_contents($request->file), $request->file->getClientOriginalName())
                ->post(config('default.base_url_server_upload') . '/api/v1/video/add', [
                    'title' => $request->title,
                    'author' => auth()->user()->full_name,
                    'callback' => route('api_callback.update_video'),
                ]);
            if ($response->successful()) {
                $res = $response->json();
                return response()->json($res);
            }
        } else {
            $filePath = null;
            if (!empty($request->file)) {
                $filePath = FileHelper::upload($request->file);
            }
            return response()->json(['url' => $filePath]);
        }
    }
}
