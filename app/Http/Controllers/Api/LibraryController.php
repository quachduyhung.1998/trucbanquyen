<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AiApprove;
use App\Models\Library;
use App\Models\ObjectCopyright;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LibraryController extends Controller
{
    public function update(Request $request)
    {
        Log::info(json_encode($request->all()));
        if ($request->id) {
            $library = Library::query()->where('file_id', $request->id)->first();
            if ($library) {
                if (!empty($request->similary)) {
                    $library->data_similar = $request->similary;
                    $library->save();
                }

                $status = AiApprove::STATUS_AI_DETECT_NOT_FOUND_VIOLATION;
                if (!empty($request->similary) && is_array($request->similary) && $request->similary['similary'] >= config('default.violation_threshold', 0.85)) {
                    $status = AiApprove::STATUS_AI_DETECT_POSSIBLE_VIOLATION;
                }
                AiApprove::query()
                    ->where('model_type', AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT)
                    ->where('model_id', $library->objectCopyright?->id)
                    ->update(['status_ai_detect' => $status]);
            }
        }
    }
}
