<?php

namespace App\Http\Controllers;

use App\Http\Requests\ObjectFace\CreateObjectFaceRequest;
use App\Http\Requests\ObjectFace\UpdateObjectFaceRequest;
use App\Models\ObjectFace;
use Illuminate\Http\Request;

class ObjectFaceController extends Controller
{
    public function index(Request $request)
    {
        $objectFaces = ObjectFace::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('position', 'like', "%$request->keyword%")
                    ->orWhere('note', 'like', "%$request->keyword%");
            })
            ->when($request->tag_alerts, function ($q) use ($request) {
                $q->whereJsonContains('tag_alerts', array_map(fn ($item) => (int) $item, $request->tag_alerts));
            })
            ->latest('id')
            ->paginate(10);
        return view('object_face.index', compact('objectFaces'));
    }

    public function create()
    {
        return view('object_face.create');
    }

    public function store(CreateObjectFaceRequest $request)
    {
        $objectFace = ObjectFace::create($request->except('_token'));
        if ($objectFace) {
            return redirect()->route('object_face.index')->with('success', __('Thêm đối tượng thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function edit($id)
    {
        $objectFace = ObjectFace::find($id);
        if (!$objectFace) abort(404);
        return view('object_face.edit', compact('objectFace'));
    }

    public function update($id, UpdateObjectFaceRequest $request)
    {
        $objectFace = ObjectFace::find($id);
        if ($objectFace) {
            $objectFace->update($request->except('_token'));
            return redirect()->route('object_face.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));

    }

    public function delete(Request $request)
    {
        $objectFace = ObjectFace::find($request->id);
        if ($objectFace) {
            $objectFace->delete();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
