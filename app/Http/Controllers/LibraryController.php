<?php

namespace App\Http\Controllers;

use App\Http\Requests\Library\StoreLibraryRequest;
use App\Models\AiApprove;
use App\Models\Library;
use App\Models\ObjectCopyright;

class LibraryController extends Controller
{
    public function storeObjectCopyright(StoreLibraryRequest $request)
    {
        $titleExplode = explode('.', $request->title);
        $lastKey = array_key_last($titleExplode);
        $ext = end($titleExplode);
        array_splice($titleExplode, $lastKey, 1);
        $title = implode(' ', $titleExplode);
        $type = str_contains($request->type, 'video') ? Library::TYPE_VIDEO : Library::TYPE_IMAGE;

        $library = Library::create([
            'title' => $title,
            'type' => $type,
            'file_id' => $request->file_id,
            'url' => $request->url,
            'thumb' => $request->thumb,
            'size' => $request->size,
            'file_extension' => $ext,
            'video_duration' => $request->duration,
            'author' => $request->author,
            'field_id' => $request->field_id,
        ]);

        if ($library) {
            $objectCopyright = ObjectCopyright::create([
                "user_id" => auth()->user()->id,
                "name" => $title,
                "author" => $request->author,
                'library_id' => $library->id
            ]);
            if ($objectCopyright) {
                AiApprove::create([
                    'model_type' => AiApprove::MODEL_TYPE_OBJECT_COPYRIGHT,
                    'model_id' => $objectCopyright->id,
                ]);
            }
        }

        return response()->json(['status' => true]);
    }

    public function store(StoreLibraryRequest $request)
    {
        $titleExplode = explode('.', $request->title);
        $lastKey = array_key_last($titleExplode);
        $ext = end($titleExplode);
        array_splice($titleExplode, $lastKey, 1);
        $title = implode(' ', $titleExplode);
        $type = str_contains($request->type, 'video') ? Library::TYPE_VIDEO : Library::TYPE_IMAGE;

        $library = Library::create([
            'title' => $title,
            'type' => $type,
            'file_id' => $request->file_id,
            'url' => $request->url,
            'thumb' => $request->thumb,
            'size' => $request->size,
            'file_extension' => $ext,
            'video_duration' => $request->duration,
            'author' => $request->author,
            'field_id' => $request->field_id,
        ]);

        if ($library) {
            AiApprove::create([
                'model_type' => AiApprove::MODEL_TYPE_LIBRARY,
                'model_id' => $library->id,
            ]);
        }

        return response()->json(['status' => true]);
    }
}
