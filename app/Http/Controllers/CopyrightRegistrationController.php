<?php

namespace App\Http\Controllers;

use App\Http\Requests\CopyrightRegistration\ChangeStatusCopyrightRegistrationRequest;
use App\Http\Requests\CopyrightRegistration\CreateCopyrightRegistrationRequest;
use App\Http\Requests\CopyrightRegistration\UpdateCopyrightRegistrationRequest;
use App\Models\Category;
use App\Models\CopyrightRegistration;
use App\Models\Notification;
use App\Models\User;
use App\Services\Notification\NotificationService;
use Illuminate\Http\Request;

class CopyrightRegistrationController extends Controller
{
    public function __construct(protected NotificationService $notificationService) {}

    public function index(Request $request)
    {
        $copyrightRegistrations = CopyrightRegistration::query()
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'like', "%$request->keyword%")
                    ->orWhere('description', 'like', "%$request->keyword%");
            })
            ->when($request->type_id, function ($q) use ($request) {
                $q->where('type_id', $request->type_id);
            })
            ->when($request->field_id, function ($q) use ($request) {
                $q->where('field_id', $request->field_id);
            })
            ->when($request->status_ai_detect, function ($q) use ($request) {
                $q->where('status_ai_detect', $request->status_ai_detect);
            })
            ->when($request->status, function ($q) use ($request) {
                $q->where('status', $request->status);
            });

        if (auth()->user()->role === User::ROLE_MEMBER) {
            $copyrightRegistrations->where('user_id', auth()->user()->id);
        }

        $copyrightRegistrations = $copyrightRegistrations->latest('id')->paginate(10);
        return view('copyright_registration.index', compact('copyrightRegistrations'));
    }

    public function create()
    {
        $types = Category::query()
            ->where('type', Category::LOAI_HINH)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        return view('copyright_registration.create', compact('types', 'fields'));
    }

    public function store(CreateCopyrightRegistrationRequest $request)
    {
        $copyrightRegistration = CopyrightRegistration::create($request->except('_token'));
        if ($copyrightRegistration) {
            return redirect()->route('copyright_registration.index')->with('success', __('Thêm đối tượng thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function edit($id)
    {
        $copyrightRegistration = CopyrightRegistration::find($id);
        if (!$copyrightRegistration) abort(404);
        $types = Category::query()
            ->where('type', Category::LOAI_HINH)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $fields = Category::query()
            ->where('type', Category::LINH_VUC)
            ->latest('id')
            ->pluck('name', 'id')
            ->toArray();
        $notifications = Notification::with('user')
            ->where('model_type', Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION)
            ->where('model_id', $id)
            ->latest('id')
            ->get();
        return view('copyright_registration.edit', compact('copyrightRegistration', 'types', 'fields', 'notifications'));
    }

    public function update($id, UpdateCopyrightRegistrationRequest $request)
    {
        $copyrightRegistration = CopyrightRegistration::find($id);
        if ($copyrightRegistration) {
            $copyrightRegistration->update($request->except('_token'));
            return redirect()->route('copyright_registration.index')->with('success', __('Cập nhật thành công'));
        }
        return back()->with('error', __('Đã có lỗi xảy ra, vui lòng thử lại sau'));
    }

    public function changeStatus(ChangeStatusCopyrightRegistrationRequest $request)
    {
        $copyrightRegistration = CopyrightRegistration::find($request->id);
        if ($copyrightRegistration) {
            $copyrightRegistration->status = $request->status;
            $copyrightRegistration->save();

            // notification
            $this->notificationService->create(
                auth()->user()->id,
                Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION,
                $copyrightRegistration->id,
                $request->notification,
                $request->status
            );

            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }
}
