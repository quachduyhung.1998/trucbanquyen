<?php

namespace App\Http\Requests\Package;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'price' => 'required|numeric',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'price' => str_replace(',', '', str_replace('.', '', $this->price)),
        ]);
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên gói',
            'price.required' => 'Bạn chưa nhập giá của gói',
            'price.numeric' => 'Giá gói phải là số',
        ];
    }
}
