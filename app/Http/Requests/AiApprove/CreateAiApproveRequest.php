<?php

namespace App\Http\Requests\AiApprove;

use Illuminate\Foundation\Http\FormRequest;

class CreateAiApproveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'url' => 'nullable',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Thiếu tên file',
            'url.required' => 'Thiếu url',
            'type.required' => 'Thiếu kiểu file',
        ];
    }
}
