<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|string',
            'email' => [
                'nullable',
                'email',
                Rule::unique('users')->ignore(auth()->user()->id)
            ],
            'phone' => [
                'nullable',
                'regex:/^(03|05|07|08|09)[0-9]{8}+$/'
            ],
        ];
    }

    public function messages()
    {
        return [
            'full_name.required' => 'Bạn chưa nhập họ tên',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã được sử dụng',
            'phone.regex' => 'Số điện thoại không đúng định dạng',
        ];
    }
}
