<?php

namespace App\Http\Requests\Library;

use Illuminate\Foundation\Http\FormRequest;

class StoreLibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'file_id' => 'required',
            'type' => 'nullable',
            'size' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Thiếu title',
            'file_id.required' => 'Thiếu file_id',
            'type.required' => 'Thiếu type',
            'size.required' => 'Thiếu size',
        ];
    }
}
