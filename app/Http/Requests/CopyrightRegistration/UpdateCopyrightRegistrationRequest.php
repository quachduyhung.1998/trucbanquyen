<?php

namespace App\Http\Requests\CopyrightRegistration;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCopyrightRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'url_content_protection' => 'required',
            'field_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên sản phẩm',
            'url_content_protection.required' => 'Bạn chưa thêm nội dung bản quyền',
            'field_id.required' => 'Bạn chưa chọn lĩnh vực',
        ];
    }
}
