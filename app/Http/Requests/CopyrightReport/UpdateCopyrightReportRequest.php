<?php

namespace App\Http\Requests\CopyrightReport;

use App\Models\CopyrightReport;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCopyrightReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'object_in' => 'required_if:type_object,'.CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED,
            'object_out' => 'required_if:type_object,'.CopyrightReport::TYPE_OBJECT_OUT_COPYRIGHT_PROTECTED,
            'url_contents' => 'required',
            'subject_email' => 'nullable|email',
            'subject_phone' => [
                'nullable',
                'regex:/^(03|05|07|08|09)[0-9]{8}+$/'
            ],
        ];
    }

    public function messages()
    {
        return [
            'object_in.required_if' => 'Bạn chưa chọn sản phẩm',
            'object_out.required_if' => 'Bạn chưa nhập tên sản phẩm',
            'url_contents.required' => 'Bạn chưa thêm nội dung, hình ảnh bằng chứng',
            'subject_email.email' => 'Email không đúng định dạng',
            'subject_phone.regex' => 'Số điện thoại không đúng định dạng',
        ];
    }
}
