<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class CreateNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'model_type' => 'required',
            'model_id' => 'required',
            'notification_content' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'model_type.required' => 'Thiếu model_type',
            'model_id.required' => 'Thiếu model_id',
            'notification_content.required' => 'Bạn chưa nội dung tin nhắn',
        ];
    }
}
