<?php

namespace App\Http\Requests\ObjectFace;

use Illuminate\Foundation\Http\FormRequest;

class CreateObjectFaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'position' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên nhân vật',
            'position.required' => 'Bạn chưa nhập chức vụ',
        ];
    }
}
