<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class FileHelper
{
    public static function upload($file): string
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = date('Y_m_d_His_');
        $fileName .= Str::slug(str_replace("." . $extension, "", $file->getClientOriginalName()));
        $fileName .= "_" . md5(time()) . "." . $extension;
        $save = $file->storeAs('uploads', $fileName, 'public');
        return "/storage/$save";
    }

    public static function generateLink($url): string
    {
        $url = str_replace('139.99.93.129:9000', '', $url);
        if (!str_contains($url, 'http')) {
            if (str_contains($url, '/storage/')) {
                $url = asset($url);
            } else {
                $url = config('default.url_server_file') . $url;
            }
        }
        if (str_contains($url, '.doc') || str_contains($url, '.docx') || str_contains($url, '.pdf')) {
            $url = route('pdftron_viewer', ['file' => $url]);
        }
        return $url;
    }
}
