<?php

namespace App\View\Composers;

use App\Services\Notification\NotificationService;
use Illuminate\View\View;

class NotificationComposer
{
    /**
     * Create a new profile composer.
     */
    public function __construct(protected NotificationService $notificationService) {}

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        $data = $this->notificationService->getAllByUserId(auth()->user()->id);

        $view->with('TOTAL_NOTIFICATION', $data['totalNotification']);
        $view->with('HAS_NEW_NOTIFICATION', $data['hasNewNotification']);
        $view->with('TOTAL_NOTIFICATION_UNREAD', $data['totalNotificationUnread']);
        $view->with('NOTIFICATIONS', $data['notifications']);
    }
}
