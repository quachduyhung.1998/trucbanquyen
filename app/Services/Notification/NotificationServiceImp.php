<?php

namespace App\Services\Notification;

use App\Helpers\NotificationFirebaseHelper;
use App\Mail\ForgotPassword;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\User;
use App\Models\UserFirebaseToken;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class NotificationServiceImp implements NotificationService
{
    public function create($userId, $modelType, $modelId, $notificationContent, $statusContent = null): bool
    {
        $model = App::make($modelType)->find($modelId);
        if ($model) {
            $notification = Notification::create([
                'user_id' => $userId,
                'model_type' => $modelType,
                'model_id' => $modelId,
                'content' => $notificationContent,
                'status_content' => $statusContent
            ]);
            if ($notification) {
                $userIds = Notification::query()
                    ->where('model_type', $modelType)
                    ->where('model_id', $modelId)
                    ->pluck('user_id')
                    ->toArray();
                $ownerId = $model->user_id;
                $sendToUserIds = array_unique(array_diff($userIds, [$ownerId, $userId]));

                $tokens = [];

                // gửi tới người tạo bản quyền
                if ($ownerId !== $userId) {
                    NotificationUser::create([
                        'notification_id' => $notification->id,
                        'user_id' => $ownerId,
                    ]);
                    $tokens = array_merge($tokens, UserFirebaseToken::query()->where('user_id', $ownerId)->pluck('token')->toArray());
                }

                // gửi tới những người cũng đã comment
                $dataInsert = [];
                foreach ($sendToUserIds as $sendToUserId) {
                    $dataInsert[] = [
                        'notification_id' => $notification->id,
                        'user_id' => $sendToUserId,
                        'to_owner' => NotificationUser::IS_NOT_TO_OWNER,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }

                if (!empty($dataInsert)) {
                    NotificationUser::insert($dataInsert);
                    $tokens = array_merge($tokens, UserFirebaseToken::query()->whereIn('user_id', $sendToUserIds)->pluck('token')->toArray());
                }

                if (!empty($tokens)) {
                    $contentName = '';
                    $contentStatusChange = '';
                    $htmlContentName = '';
                    $htmlContentStatusChange = '';
                    if ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REGISTRATION) {
                        $contentName = $notification->copyrightRegistration?->name;
                        $htmlContentName = $notification->copyrightRegistration?->name;
                        if ($notification->status_content) {
                            $contentStatusChange = ' "' . (\App\Models\CopyrightRegistration::$statusLabel[$notification->status_content] ?? '') . '" ';
                            $htmlContentStatusChange = ' <span class="badge badge-' . (\App\Models\CopyrightRegistration::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightRegistration::$statusLabel[$notification->status_content] ?? '') . '</span>';
                        }
                    } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_PROTECTION) {
                        $contentName = $notification->copyrightProtection?->name;
                        $htmlContentName = $notification->copyrightProtection?->name;
                        if ($notification->status_content) {
                            $contentStatusChange = ' "' .(\App\Models\CopyrightProtection::$statusLabel[$notification->status_content] ?? '') . '" ';
                            $htmlContentStatusChange = ' <span class="badge badge-' . (\App\Models\CopyrightProtection::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightProtection::$statusLabel[$notification->status_content] ?? '') . '</span>';
                        }
                    } elseif ($notification->model_type === \App\Models\Notification::MODEL_TYPE_COPYRIGHT_REPORT) {
                        $contentName = $notification->copyrightReport?->object;
                        $htmlContentName = $notification->copyrightReport?->object;
                        if ($notification->copyrightReport?->type_object === \App\Models\CopyrightReport::TYPE_OBJECT_IN_COPYRIGHT_PROTECTED) {
                            $contentName = $notification->copyrightReport?->copyrightProtected?->name;
                            $htmlContentName = $notification->copyrightReport?->copyrightProtected?->name;
                        }
                        if ($notification->status_content) {
                            $contentStatusChange = ' "' . (\App\Models\CopyrightReport::$statusLabel[$notification->status_content] ?? '') . '" ';
                            $htmlContentStatusChange = ' <span class="badge badge-' . (\App\Models\CopyrightReport::$statusColor[$notification->status_content] ?? '') . '">' . (\App\Models\CopyrightReport::$statusLabel[$notification->status_content] ?? '') . '</span>';
                        }
                    }
                    $message = '';
                    if ($notification->content) {
                        if ($notification->status_content) {
                            $message .= ' ' . __('với lời nhắn');
                        }
                        $message .= ': ' . str_replace(['<p>', '</p>'], ['', ''], $notification->content);
                    }

                    $body = $notification->user?->full_name;
                    $body .= ' ' .($notification->status_content ? __('đã cập nhật trạng thái sang') : __('đã thêm tin nhắn mới vào'));
                    $body .= $contentStatusChange;
                    $body .= $notification->status_content ? __('cho') : '';
                    $body .= ' ' . (\App\Models\Notification::$modelTypeLabel[$notification->model_type] ?? '');
                    $body .= " ($contentName)$message";

                    $html = "<div class='fs-6 text-gray-800 notification-item-content'>
                            <b>{$notification->user?->full_name}</b>
                        <span>";
                    $html .= ' ' .($notification->status_content ? __('đã cập nhật trạng thái sang') : __('đã thêm tin nhắn mới vào'));
                    $html .= $htmlContentStatusChange;
                    $html .= $notification->status_content ? __('cho') : '';
                    $html .= ' <b>' . (\App\Models\Notification::$modelTypeLabel[$notification->model_type] ?? '');
                    $html .= " ($htmlContentName)</b>$message
                            </span>
                        </div>
                    ";

                    NotificationFirebaseHelper::send(
                        tokens: $tokens,
                        body: $body,
                        click_action: route('notification.click', $notification->id),
                        data: [
                            'notification_id' => $notification->id,
                            'html' => $html
                        ]
                    );
                }

                return true;
            }
        }
        return false;
    }

    public function getAllByUserId($userId): array
    {
        $totalNotification = NotificationUser::query()->where('user_id', $userId)->count();

        $hasNewNotification = false;
        $countNewNotification = NotificationUser::query()
            ->where('user_id', $userId)
            ->where('is_new', NotificationUser::IS_NEW)
            ->count();
        if ($countNewNotification) $hasNewNotification = true;

        $totalNotificationUnread = NotificationUser::query()
            ->where('user_id', $userId)
            ->where('status', NotificationUser::STATUS_UNREAD)
            ->count();

        $notificationIds = NotificationUser::query()->where('user_id', $userId)->pluck('notification_id')->toArray();
        $notifications = Notification::with([
                'user',
                'copyrightRegistration',
                'copyrightProtection',
                'copyrightReport',
                'notificationUsers' => function ($q) use ($userId) {
                    $q->where('user_id', $userId);
                }
            ])
            ->whereIn('id', $notificationIds)
            ->latest('id')
            ->paginate(10);

        return [
            'totalNotification' => $totalNotification,
            'hasNewNotification' => $hasNewNotification,
            'totalNotificationUnread' => $totalNotificationUnread,
            'notifications' => $notifications,
        ];
    }
}
