<?php

namespace App\Services\Notification;

interface NotificationService
{
    public function create($userId, $modelType, $modelId, $notificationContent, $statusContent = null);

    public function getAllByUserId($userId);
}
